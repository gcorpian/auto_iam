# This script contains multi-function processes
#--------- IMPORTS ----------
from ldap3.utils.ciDict import CaseInsensitiveDict 
import json

#-------CUSTOM IMPORTS -------
import CONFIG
import DB
import GROUPTABLES
import IIQ
import LDAP
import UTILS
import TEST_DATA


#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent

def MappingTable(record):
    available_mappings = {
        "CONNECTORS": Connectors,
        "HU2HLDAP": HU2HLDAP,
        "COMPARE LDAP SCHEMAS": CompareLDAPSchemas,
        "GROUP COUNTS": GrouperCounts
        #"FIND ALUMNI RECORD": FindAlumniRecord
        } 
    MappedObjects = available_mappings.get(record["screen"], lambda x: None)(record)
    UTILS.StatusCheck(record)
    
    
def Connectors(record):
    CONFIG.verboseReporting = False
    LDAP_CHECK = []
    if CONFIG.env == "P-1" or CONFIG.env == "P-0":
        LDAPS_Control = [["AUTHLDAP", None, None],
                         ["FASAD", None, None],
                         ["HLDAP", None, None],
                         ["UNIVAD", None, None],
                         ["FASLDAP", None, None],
                         ["FASMAIL", None, None]]
    else:
        LDAPS_Control = [["AUTHLDAP", None, None],
                         ["FASAD", None, None],
                         ["HLDAP", None, None],
                         ["UNIVAD", None, None]]
                     
    
                 
    faculty_codes = ["AAD","ADG","ART","DEF","DIN","DIV","FCL","GSD","GSE","HAM",
                     "HAS","HCU","HGR","HIO","HMC","HRE","HUL","HUP","HUS","HVN",
                     "LAS","LHF","MAG","MAR","MEM","OGB","OGC","OHR","OPR","PAI",
                     "POL","RAD","SAO","TBD","UHS","UOS","UPO","VIT","VPA","VPF","VPG"]
    
    for c in range(0, len(record["fields"])):
        fieldName = record["fields"][c].upper()
        expectedValue = record["values"][c]
        if fieldName == "HUID":
            searchValue = expectedValue
            
            #1)  SQL Query to get communities for a user record
            result = str(DB.DatabaseOperations({"row":record["row"], "fields":["User Name", "Query"], "values":["Sailpoint", "SELECT NVL(Communities,'N/A') as Communities, NVL(Faculty_cd,'N/A') as Faculty_cd, NVL(Fasusername,'N/A') as Fasusername FROM v_sailpoint_person WHERE HUID =" + searchValue]})).replace(";', '", ";").replace("[", "").replace("]", "").replace("'", "").replace(", ", ",").split(",")#[3:]
            CONFIG.logger1.info(indent + "Communities found for HUID " + searchValue)
            for c in range(0, int(len(result)/3)*3, 3):
                if result[c] == "N/A": continue
                else: 
                    comm = result[c].split(";")
                    fac = result[c+1]
                   
                #2)  Split Communities and eliminate redundancies
                    CONFIG.logger1.info((indent)*2 + "Faculty Code: %s" %(fac))
                    for ldap in comm:
                        if ldap in GROUPTABLES.communities.keys():
                            comment = "%s has entries in %s" %(ldap, GROUPTABLES.communities[ldap])
                            if ldap.find("CAPLUS") > -1 and (fac not in faculty_codes): 
                                GROUPTABLES.communities[ldap] = None
                                comment = "%s skipped due to faculty code." %(ldap)
                            elif GROUPTABLES.communities[ldap] == None: comment = "%s generates no LDAP entries" %(ldap)
                            CONFIG.logger1.info((indent)*3 + comment)
                            if GROUPTABLES.communities[ldap] != None: LDAP_CHECK.extend(GROUPTABLES.communities[ldap])
                        #else: print(ldap + " not found")
            for c in range (0, len(LDAP_CHECK)):
                LDAP_CHECK[c] = LDAP_CHECK[c].replace("*", "").replace(" (disabled)", "").strip()
            LDAP_CHECK = sorted(list(set(LDAP_CHECK)))

            if LDAP_CHECK == []:
                CONFIG.logger1.info((indent)*3 + "None.  Ending Test.\n")
                return
            else: CONFIG.logger1.info(indent + "LDAP Servers that will be queried: %s\n" % (str(LDAP_CHECK)))
            #4)  Get before information for each LDAP
            for search in LDAP_CHECK:
                if (CONFIG.env == "P-1" or CONFIG.env == "P-0") and (search == "FASMAIL"):
                    hldap = LDAP.LDAPOperations({"row":record["row"],"description":"Search " + search + " for FASUsername " + CONFIG.FASUserName, "screen":"SEARCH RECORD", "fields":["SERVER NAME", "FILTER"], "values":[search, "(mailNickName=" + CONFIG.FASUserName + ")"]})
                else:
                    hldap = LDAP.LDAPOperations({"row":record["row"],"description":"Search " + search + " for HUID " + expectedValue, "screen":"SEARCH RECORD", "fields":["SERVER NAME", "HUID"], "values":[search, expectedValue]})
                if search == "AUTHLDAP": LDAPS_Control[0][1] = hldap
                elif search == "FASAD": 
                    LDAPS_Control[1][1] = hldap
                    
                elif search == "HLDAP": LDAPS_Control[2][1] = hldap
                elif search == "UNIVAD": LDAPS_Control[3][1] = hldap
                elif CONFIG.env == "P-1" or CONFIG.env == "P-0":
                    if search == "FASLDAP": LDAPS_Control[4][1] = hldap
                    elif search == "FASMAIL": 
                        if CONFIG.FASUserName == "": UTILS.ReportErrors("Missing FASUsername in v_sailpoint_person -- Unable to query FASMAIL", {"row":record["row"],"description":"Search " + search + " for HUID " + expectedValue})
                        else: LDAPS_Control[5][1] = hldap
            if CONFIG.refreshIIQ == False:
                for c1 in range(0, len(LDAPS_Control)):
                    if LDAPS_Control[c1][0] in LDAP_CHECK:
                        CONFIG.logger1.info("\n" + ("-")*60 + "\n" + LDAPS_Control[c1][0] + " Results:")
                        if type(LDAPS_Control[c1][1]) != CaseInsensitiveDict: 
                            CONFIG.logger1.info(indent + "No attributes found")
                        elif LDAPS_Control[c1][1] != None or LDAPS_Control[c1][1]!= "":
                            for key in sorted(LDAPS_Control[c1][1].keys()):
                                CONFIG.logger1.info(indent + key + ": " + LDAPS_Control[c1][1][key])
            else:    
                #5)  Refresh LDAPs using IIQ
                if CONFIG.driver == "":
                    CONFIG.logger1.info("\n" + indent + "Logging into IIQ")
                    result = IIQ.Login({"row":record["row"], "description":"IIQ Login ", "action": "INPUT", "screen":"LOGIN", "fields":["USERNAME", "PASSWORD", "ACTION"], "values":["spadmin", "YWRtaW4=", "Login"]})
                CONFIG.logger1.info(indent + "Refreshing HUID " + expectedValue + "\n")
                result = IIQ.RefreshIdentity({"row":record["row"], "description":"Refresh Identity for " + expectedValue, "action": "INPUT", "screen":"Refresh Identity", "fields":["SEARCH", "ACTION", "SELECT", "ACTION"], "values":[expectedValue, "Find", expectedValue, "Submit"]})

                #6)  Get after information for each LDAP
                for search in LDAP_CHECK:
                    if (CONFIG.env == "P-1" or CONFIG.env == "P-0") and (search == "FASMAIL"):
                        hldap = LDAP.LDAPOperations({"row":record["row"],"description":"Search " + search + " for FASUsername " + CONFIG.FASUserName, "screen":"SEARCH RECORD", "fields":["SERVER NAME", "FILTER"], "values":[search, "(mailNickName=" + CONFIG.FASUserName + ")"]})
                    else:
                        hldap = LDAP.LDAPOperations({"row":record["row"],"description":"Search " + search + " for HUID " + expectedValue, "screen":"SEARCH RECORD", "fields":["SERVER NAME", "HUID"], "values":[search, expectedValue]})
                    if search == "AUTHLDAP": LDAPS_Control[0][2] = hldap
                    elif search == "FASAD": LDAPS_Control[1][2] = hldap
                    elif search == "HLDAP": LDAPS_Control[2][2] = hldap
                    elif search == "UNIVAD": LDAPS_Control[3][2] = hldap
                    elif CONFIG.env == "P-1" or CONFIG.env == "P-0":
                        if search == "FASLDAP": LDAPS_Control[4][2] = hldap
                        elif search == "FASMAIL": 
                          
                            LDAPS_Control[5][2] = hldap
                #7)  Compare before and after  LDAP records
                for c1 in range(0, len(LDAPS_Control)):
                    if LDAPS_Control[c1][0] in LDAP_CHECK:
                        CONFIG.logger1.info("\n" + ("-")*60 + "\n" + LDAPS_Control[c1][0] + " Comparison Results:")
                        if type(LDAPS_Control[c1][1]) != CaseInsensitiveDict: 
                            CONFIG.logger1.info(indent + "No attributes found before IIQ refresh")
                        elif LDAPS_Control[c1][1] != None or LDAPS_Control[c1][1]!= "": 
                            for key in sorted(LDAPS_Control[c1][1].keys()):
                                if key in LDAPS_Control[c1][2].keys(): 
                                    if LDAPS_Control[c1][1][key] == LDAPS_Control[c1][2][key]:
                                        CONFIG.logger1.info(indent + key + ": " + LDAPS_Control[c1][1][key])
                                    else: CONFIG.logger1.info(indent + key + " VALUE CHANGE. BEFORE REFRESH: " + LDAPS_Control[c1][1][key] + " >>  AFTER REFRESH: " + LDAPS_Control[c1][2][key])
                                else: CONFIG.logger1.info(indent+ "ERROR: " + key + " not found")
                        if type(LDAPS_Control[c1][2]) != CaseInsensitiveDict: 
                            CONFIG.logger1.info(indent + "No attributes found after IIQ refresh")    
                        elif LDAPS_Control[c1][1] == None and LDAPS_Control[c1][2] != None: 
                                for key in sorted(LDAPS_Control[c1][2].keys()):
                                    CONFIG.logger1.info(indent + key + " VALUE CHANGE. BEFORE REFRESH: " + key + " NOT FOUND >>  AFTER REFRESH: " + LDAPS_Control[c1][2][key])

    CONFIG.verboseReporting = True



def HU2HLDAP(record):
    CONFIG.verboseReporting = False
    sortedKeys = []
    if CONFIG.env == "P-1":
        for c in range(0, len(record["fields"])):
            fieldName = record["fields"][c].upper()
            expectedValue = record["values"][c]
            
            huldap = LDAP.LDAPOperations({"row":record["row"],"description":"Search HULDAP for HUID " + expectedValue, "screen":"SEARCH RECORD", "fields":["SERVER NAME", "HUID"], "values":["HULDAP", expectedValue]})
            #print(huldap[0]["attributes"])
 
            for key in huldap[0]["attributes"]:
                sortedKeys.append(key)
            
            hldap = LDAP.LDAPOperations({"row":record["row"],"description":"Search HLDAP for HUID " + expectedValue, "screen":"SEARCH RECORD", "fields":["SERVER NAME", "HUID"], "values":["HLDAP", expectedValue]})
            #print(hldap[0]["attributes"])
            
            for key in hldap[0]["attributes"]:
                if key not in sortedKeys:
                    sortedKeys.append(key)
            sortedKeys.sort()
            
            
            CONFIG.logger1.info("Attribute".ljust(40) + "Match".ljust(7) + "HU-LDAP".ljust(50) + "HLDAP".ljust(50))
            CONFIG.logger1.info("---------".ljust(40) + "-----".ljust(7) + "-------".ljust(50) + "-----".ljust(50))
            
            for key in sortedKeys:
                if key in huldap[0]["attributes"]:# and not (huldap[0]["attributes"][key] is None):
                    huldapValue = huldap[0]["attributes"][key].strip()
                    if len(huldapValue) >= 50:
                        huldapValue = "'" + huldapValue + "';  "
                    elif len(huldapValue) == 0:
                        huldapValue = "''"
                else:
                    huldapValue = "''"
                
                if key in hldap[0]["attributes"]:# and not (hldap[0]["attributes"][key] is None):
                    hldapValue = hldap[0]["attributes"][key].strip()
                    if len(huldapValue) >= 50:
                        hldapValue = "'" + hldapValue + "'"
                    elif len(hldapValue) == 0:
                        hldapValue = "''"
                else:
                    hldapValue = "''"
                
                if huldapValue == hldapValue:
                    if huldapValue == "''" and hldapValue == "''": matchValue = ""
                    else: matchValue = "Yes"
                else:
                    matchValue = " No"
                
                CONFIG.logger1.info(key.ljust(40) + matchValue.ljust(7) + huldapValue.ljust(50) + hldapValue.ljust(50))
    else:
        CONFIG.logger1.info(indent + "This process only runs in the Stage environment.")
    
    CONFIG.verboseReporting = True
   
def CompareLDAPSchemas(record):
    CONFIG.verboseReporting = False
    sortedKeys = []
    
    if CONFIG.env == "P-1":
        for c in range(0, len(record["fields"])):
            fieldName = record["fields"][c].upper()
            expectedValue = record["values"][c]
            
            huldap = LDAP.LDAPOperations({"row":record["row"],"description":"Search HULDAP for HUID " + expectedValue, "screen":"SEARCH RECORD", "fields":["SERVER NAME", "HUID"], "values":["HULDAP", expectedValue]})
            #print(huldap[0]["attributes"])
 
            for key in huldap[0]["attributes"]:
                sortedKeys.append(key)
            
            hldap = LDAP.LDAPOperations({"row":record["row"],"description":"Search HLDAP for HUID " + expectedValue, "screen":"SEARCH RECORD", "fields":["SERVER NAME", "HUID"], "values":["UNIFIED LDAP", expectedValue]})
            #print(hldap[0]["attributes"])
            
            for key in hldap[0]["attributes"]:
                if key not in sortedKeys:
                    sortedKeys.append(key)
            sortedKeys.sort()
            
            
            CONFIG.logger1.info("Attribute".ljust(40) + "Match".ljust(7) + "HU-LDAP".ljust(50) + "UNIFIED LDAP".ljust(50))
            CONFIG.logger1.info("---------".ljust(40) + "-----".ljust(7) + "-------".ljust(50) + "-----".ljust(50))
            
            for key in sortedKeys:
                if key in huldap[0]["attributes"]:# and not (huldap[0]["attributes"][key] is None):
                    huldapValue = huldap[0]["attributes"][key].strip()
                    if len(huldapValue) >= 50:
                        huldapValue = "'" + huldapValue + "';  "
                    elif len(huldapValue) == 0:
                        huldapValue = "''"
                else:
                    huldapValue = "''"
                
                if key in hldap[0]["attributes"]:# and not (hldap[0]["attributes"][key] is None):
                    hldapValue = hldap[0]["attributes"][key].strip()
                    if len(huldapValue) >= 50:
                        hldapValue = "'" + hldapValue + "'"
                    elif len(hldapValue) == 0:
                        hldapValue = "''"
                else:
                    hldapValue = "''"
                
                if huldapValue == hldapValue:
                    if huldapValue == "''" and hldapValue == "''": matchValue = ""
                    else: matchValue = "Yes"
                else:
                    matchValue = " No"
                
                CONFIG.logger1.info(key.ljust(40) + matchValue.ljust(7) + huldapValue.ljust(50) + hldapValue.ljust(50))
    else:
        CONFIG.logger1.info(indent + "This process only runs in the Stage environment.")
    
    CONFIG.verboseReporting = True
    
def GrouperCounts(record):
    CONFIG.verboseReporting = False
    dictObject = ""
    
    # get groupname from spredsheet
    for c in range(0, len(record["fields"])):
        fieldName = record["fields"][c].upper()
        expectedValue = record["values"][c]
        if fieldName == "GROUP NAME":
            groupName = expectedValue

    #determine groupname type
        if groupName.upper().find("POI") > 0:
            viewName = "IDMRW.V_PERSON_ROLES_POI_CUR"
            dictObject = GROUPTABLES.poi
        elif groupName.upper().find("STUDENTS") > 0:
            viewName = "IDMRW.V_PERSON_ROLES_STU_CUR"
            dictObject = GROUPTABLES.student
        elif groupName.upper().find("LIBRARY-BORROWER") > 0:
            viewName = "IDMRW.V_PERSON_ROLES_LIB_BORR_CUR"
            dictObject = GROUPTABLES.libBorrower
        elif groupName.upper().find("EMPLOYEES") > 0:
            viewName = "IDMRW.V_PERSON_ROLES_EMP_CUR"
            dictObject = GROUPTABLES.employee
        else:
            return
        
    # get criteria from groupname dict
        if dictObject[groupName]:
            searchCriteria = dictObject.get(groupName)
            #print(searchCriteria)
        else:
            searchCriteria = ""
        #    CONFIG.logger1.info(indent + "ERROR: " + groupName + ": Not Found in " + viewName)
        #    return

    # query idmrw

        if searchCriteria != "":
            if viewName == "IDMRW.V_PERSON_ROLES_EMP_CUR":
                if groupName.upper().find("ON-QUAD") > 0:
                    IDMRWQuery = "SELECT COUNT(DISTINCT pr.internal_id) FROM " + viewName + " pr JOIN IDMRW.V_HDW_DEPT_path_from_root hdw ON pr.DEPT_ID = hdw.dept_id JOIN idmrw.cd_contact_ps_location ps ON pr.addr_ps_location_cd = ps.addr_ps_location_cd WHERE (pr.role_end_dt is null OR pr.role_end_dt >= SYSDATE) AND pr.role_start_dt <= SYSDATE AND " + searchCriteria
                elif groupName.upper().find("PAID") > 0:
                    IDMRWQuery = "SELECT COUNT(DISTINCT pr.internal_id) FROM " + viewName + " pr JOIN IDMRW.V_HDW_DEPT_path_from_root hdw ON pr.DEPT_ID = hdw.dept_id JOIN idmrw.cd_role_emp_pay_group pg ON pr.paygroup = pg.paygroup WHERE (pr.role_end_dt is null or pr.role_end_dt >= SYSDATE) AND pr.role_start_dt <= SYSDATE AND " + searchCriteria
                else:
                    IDMRWQuery = "SELECT COUNT(DISTINCT pr.internal_id) FROM " + viewName + " pr JOIN IDMRW.V_HDW_DEPT_path_from_root hdw ON pr.DEPT_ID = hdw.dept_id WHERE (pr.role_end_dt is null OR pr.role_end_dt >= SYSDATE) AND pr.role_start_dt <= SYSDATE AND " + searchCriteria
            else:
                IDMRWQuery = "SELECT COUNT(DISTINCT internal_id) FROM " + viewName + " WHERE (role_end_dt is null or role_end_dt > SYSDATE) AND " + searchCriteria
        else:
            IDMRWQuery = "SELECT COUNT(DISTINCT internal_id) FROM " + viewName + " WHERE (role_end_dt is null or role_end_dt > SYSDATE)"
        
        IDMRWResult = str(DB.DatabaseOperations({"row":record["row"], "fields":["User Name", "Query"], "values":["iamautoqa", IDMRWQuery], "screen": "ORACLE"})).replace(";', '", ";").replace("[", "").replace("]", "").replace("'", "").replace(", ", "").replace(":", "")
    
    # query intdb
        INTDBQuery = "SELECT COUNT(DISTINCT membership_id) FROM group_memberships GROUP BY group_name HAVING group_name = '" + groupName + "'"
        
        try:
            INTDBResult = str(DB.DatabaseOperations({"row":record["row"], "fields":["User Name", "Query"], "values":["grouper", INTDBQuery], "screen": "MYSQL"})[0].get("COUNT(DISTINCT membership_id)"))
        except:
            INTDBResult = str(0)

    # compare results and report
        if IDMRWResult == INTDBResult:
            CONFIG.logger1.info(indent + "Record count from IDMRW matches record count in INTDB: " + INTDBResult)
        else:
            UTILS.ReportErrors("Record count from IDMRW (%s) does not match record count in INTDB (%s)" %(IDMRWResult, INTDBResult), record)
            CONFIG.logger1.info(indent*2 + "IDMRW Query: " + IDMRWQuery)
            CONFIG.logger1.info(indent*2 + "INTDB Query: " + INTDBQuery)
            
            #tempQuery = IDMRWQuery.replace("COUNT(DISTINCT internal_id)", "internal_id")
            IDMRWQuery = "SELECT distinct external_id FROM IDMRW2.ext_id_mappings where external_id_type = 'UUID' and internal_id in (" + IDMRWQuery.replace("COUNT(DISTINCT pr.internal_id)", "pr.internal_id").replace("COUNT(DISTINCT internal_id)", "internal_id") + ") order by external_id"
            IDMRWResult = str(DB.DatabaseOperations({"row":record["row"], "fields":["User Name", "Query"], "values":["iamautoqa", IDMRWQuery], "screen": "ORACLE"})).replace("[", "").replace("]", "").replace("'", "").replace(":","").replace(" ","").split(",")
            
            INTDBQuery = "SELECT subject_id FROM group_memberships where group_name = '" + groupName + "' order by subject_id"
            
            try:
                INTDBResult = str(DB.DatabaseOperations({"row":record["row"], "fields":["User Name", "Query"], "values":["grouper", INTDBQuery], "screen": "MYSQL"})).replace("{'subject_id': ", "").replace("}", "").replace("'", "").replace("[", "").replace("]", "").replace(" ", "").split(",")
            except:
                INTDBResult = str(0)
            
            for entry in IDMRWResult:
                if entry not in INTDBResult:
                    huidQuery = "select external_id from IDMRW.EXT_ID_MAPPINGS where internal_id = (select internal_id from IDMRW.EXT_ID_MAPPINGS WHERE external_id = '" + entry + "') and external_id_type = 'HUID'"
                    huidResult = DB.DatabaseOperations({"row":record["row"], "fields":["User Name", "Query"], "values":["iamautoqa", huidQuery], "screen": "ORACLE"})[0].replace("[", "").replace("]", "").replace("'", "").replace(":","").replace(" ","").split(",")
                    CONFIG.logger1.info(indent*3 + "UUID " + entry + " (HUID: " + huidResult[0] + ") not found in INTDB")
                #else:
                #    IDMRWResult.remove(record)
                #    INTDBResult.remove(record)
            #print(IDMRWResult)
            #print(len(IDMRWResult))
            #print(INTDBResult)
            #print(len(INTDBResult))
                    
            
    CONFIG.verboseReporting = TEST_DATA.verboseReporting