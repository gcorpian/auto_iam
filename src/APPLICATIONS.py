#-------CUSTOM IMPORTS -------
import UTILS
import API
import AWS
import IIQ
import KEYMGMT
import MIDAS
import LDAP
import DB
import PIN_CAS
import GROUPERUI
import DUO
import PROCESSES
import XID


# This will determine which application script needs to be accessed
def AvailableApplications(record):
    available_applications = {
        "API": API.MappingTable,
        "AWS": AWS.MappingTable,
        "IIQ": IIQ.MappingTable,
        "KEYMGMT": KEYMGMT.MappingTable,
        "LDAP": LDAP.MappingTable,
        "MIDAS": MIDAS.MappingTable,
        "SQL": DB.DatabaseOperations,
        "PIN CAS": PIN_CAS.MappingTable,
        "GROUPERUI":GROUPERUI.MappingTable,
        "DUO": DUO.MappingTable,
        "PROCESSES": PROCESSES.MappingTable,
        "XID": XID.MappingTable
    }
    if available_applications[record["application"]]:
        available_applications[record["application"]](record)
    else:
        UTILS.ReportErrors("Unrecognized Application: " + record["application"], record)
    #try:
    #    available_applications[record["application"]](record)
    #except KeyError:
    #    UTILS.ReportErrors("Unrecognized Application: " + record["application"], record)
