#---------- IMPORTS ----------
from datetime import datetime
from getpass import getuser
import os
import re
import sys
import time
import xlrd
import traceback
from subprocess import Popen, PIPE, STDOUT, check_output
from tkinter import *
from tkinter import messagebox

#-------CUSTOM IMPORTS -------
import CONFIG
import UTILS
import APPLICATIONS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
counter0, APIResponse, reqStatus = ("",) * 3
record, SpreadsheetData = ([],) * 2

# look for command line arguments being passed in for spreadsheet path and worksheet name
try:
    if sys.argv[1] and sys.argv[2]:
        CONFIG.filename = sys.argv[1]
        CONFIG.worksheet = sys.argv[2]
except:
    pass
    
#optional argument for browser     
try:
    if sys.argv[3]:
        CONFIG.browser = sys.argv[3].upper()
except:
    pass
 
#optional argument for environment
try:
    if sys.argv[4]:
        CONFIG.env = sys.argv[4].upper()
except:
    pass

         
# This will connect to the Excel spreadsheet and parse the data into a list of dictionaries
def GetData(filename, worksheet):
    global SpreadsheetData
    #check filename exists
    if os.path.isfile(filename): 
        workbook = xlrd.open_workbook(filename)
        sheet = workbook.sheet_by_name(worksheet)

        curr_row = 0
        num_rows = sheet.nrows - 1
        while curr_row < num_rows:
            curr_row += 1
            #Assign values to test parameters if the row is to be run
            strAuto = sheet.cell_value(curr_row, 0)
            if strAuto.upper() == "":
                break
            strTestCaseID = sheet.cell_value(curr_row, 1)         
            strDescription = sheet.cell_value(curr_row, 2)
            strApplication = sheet.cell_value(curr_row, 3).upper()
            strAction = sheet.cell_value(curr_row, 4).upper()
            strScreen = sheet.cell_value(curr_row, 5).upper()
            arrFields = sheet.cell_value(curr_row, 6).split("|")
            arrInputValues = str(sheet.cell_value(curr_row, 7)).split("|")
            strExpectedResults = sheet.cell_value(curr_row, 8).upper()[:1]        
            strComments = sheet.cell_value(curr_row, 9) 

            if re.match('[FN]', strExpectedResults): strExpectedResults = "F"
            else: strExpectedResults = "T"

            SpreadsheetData.append({"row":curr_row+1, "auto":strAuto, "id":strTestCaseID, "description":strDescription, "application":strApplication, "action":strAction, "screen":strScreen, "fields":arrFields, "values":arrInputValues, "results":strExpectedResults, "comments":strComments})
        return SpreadsheetData
    else:
        UTILS.ReportErrors("The Test Spreadsheet does not exist in location: " + filename, "")
        sys.exit()

def CreateMessageBox(format, title, message):
    top = Tk()
    if format == "askyesno":
        result = messagebox.askyesno(title, message)
    #root.destroy()
    #root.mainloop()
    button = Button(top, text="Close Me", command=top.destroy)
    button.pack()

    return result

def main():
    start = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")

    CONFIG.logger1.info("Worksheet: " + CONFIG.worksheet + " in " + CONFIG.filename[CONFIG.filename.rfind("/")+1:] + " on " + CONFIG.env)
    CONFIG.logger1.info("System Host: " + check_output("hostname").decode("ascii").strip())
    CONFIG.logger1.info("Test User: " + getuser())
    CONFIG.logger1.info("Start time: " + str(datetime.now().time()) + "\n")

    # check VPN connectivity
    req = Popen("nc -vz -G 1 itis-autoprovprod.cadm.harvard.edu 22",  shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    if str(req.stdout.read().decode("utf-8")).find("Operation timed out") > -1:
        CONFIG.logger1.info("This process is being run while disconnected from the VPN.")
    else:
        CONFIG.logger1.info("This process is being run while connected to the VPN.\n")
    
    try:
        SpreadsheetData = GetData(CONFIG.filename, CONFIG.worksheet)
        should_restart = True
        while should_restart:
            CONFIG.perfCounter += 1
            if CONFIG.performanceMode == True:
                CONFIG.logger1.info("\nPerformance Test Iteration %s" % (str(CONFIG.perfCounter)))
            should_restart = CONFIG.performanceMode
            for record in SpreadsheetData:
                if record["auto"] == "B":
                    CONFIG.logger1.info("\nA breakpoint was encountered in the spreadsheet at row " + str(record["row"]))
                    #result = CreateMessageBox("askyesno", "Breakpoint Found", "A breakpoint has been encountered in your test at row " + str(record["row"]) + ".  Do you wish to Continue?")
                    result = messagebox.askquestion('Breakpoint Found', "A breakpoint has been encountered in your test at row " + str(record["row"]) + ".  Do you wish to Continue?")
                    if result == False or result == "no":
                        CONFIG.logger1.info("\nEnding Test")
                        sys.exit()
                    else:
                        CONFIG.logger1.info("\nContinuing Test")
                        record["auto"] = "Y"
                        
                if (len(record["fields"]) != len(record["values"])) and record["auto"] == "Y":
                    UTILS.ReportErrors("Row " + str(record["row"]) + "- Record count mismatch between Fields ("+ str(len(record["fields"])) + ") and Expected Values ("+ str(len(record["values"])) + ").", record)
                    sys.exit()

                if record["auto"] == "Y":
                    CONFIG.testCount += 1
                    if record["row"] > 1: CONFIG.logger1.info("")
                    if record["id"] == "":        
                        CONFIG.logger1.info("Worksheet: " + CONFIG.worksheet + "   Row: " + str(record["row"]) + "   Description: " + record["description"])
                    else:
                        CONFIG.logger1.info("Worksheet: " + CONFIG.worksheet + "   Row: " + str(record["row"]) + "   Test Case ID: " + str(record["id"]) + "   Description: " + str(record["description"]))
                    stepStart = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
                    APPLICATIONS.AvailableApplications(record)
                    stepStop = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
                    CONFIG.logger1.info("Step duration for Row " + str(record["row"]) + ": " + str(stepStop - stepStart))
                    time.sleep(1)
                    continue
                if record["auto"] == "":                    
                    CONFIG.logger1.info("End of test iteration.")
                    break
            if CONFIG.totalTests > 0 and CONFIG.perfCounter >= CONFIG.totalTests:
                should_restart = False   
        UTILS.ErrorSummary()

        CONFIG.logger1.info("Stop time: " + str(datetime.now().time()))
        stop = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
        #CONFIG.logger1.info("Completion Time: %s" % str(stop))
        tDelta = stop - start
        CONFIG.testDuration = "Test Duration: %s" % str(tDelta)
        CONFIG.logger1.info(CONFIG.testDuration)
        
    except SystemExit as e:
        sys.exit(e)
    #except UnicodeDecodeError as e:
    #    CONFIG.logger1.info("Unable to locate object")
    #    sys.exit(e)
    except:
        if str(sys.exc_info()[1]) == "invalid server address":
            #print("EXCEPTION: " + str(sys.exc_info()[1]))
            print("Ending Test")
        else:
            #print("EXCEPTION: " + str(sys.exc_info()[1]))
            UTILS.ReportErrors(str(sys.exc_info()), record)
            UTILS.ReportErrors(str(traceback.format_exc()), record) 
            CONFIG.logger1.info("Stop time: " + str(datetime.now().time()))
            stop = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
            tDelta = stop - start
            CONFIG.testDuration = "Test Duration: %s\n" % str(tDelta)
            CONFIG.CloseLogFile()
            time.sleep(5)
            if CONFIG.driver != "":
                CONFIG.driver.maximize_window()
                os.system("screencapture " + CONFIG.LogFolder + CONFIG.imagefile)
            UTILS.SendMail(CONFIG.testCount)
            return 1
    
    CONFIG.CloseLogFile()
    time.sleep(5)
    UTILS.SendMail(CONFIG.testCount) 
    if CONFIG.dbRecordResults == True:
        Query = "SELECT COUNT(DISTINCT pr.internal_id) FROM " + viewName + " pr JOIN IDMRW.V_HDW_DEPT_path_from_root hdw ON pr.DEPT_ID = hdw.dept_id JOIN idmrw.cd_contact_ps_location ps ON pr.addr_ps_location_cd = ps.addr_ps_location_cd WHERE (pr.role_end_dt is null OR pr.role_end_dt >= SYSDATE) AND pr.role_start_dt <= SYSDATE AND " + searchCriteria
        pass
    
    #if CONFIG.driver != "":
    #    CONFIG.driver.close()
     
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    main()