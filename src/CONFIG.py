import sys
import os
import logging
from logging import FileHandler
from datetime import datetime

#-------CUSTOM IMPORTS -------
import TEST_DATA

#---------- TEST DATA SETUP ----------
clientConfig = TEST_DATA.clientConfig

try: env
except NameError: env = None
if env is None:
    env = TEST_DATA.env
filename = TEST_DATA.filename
worksheet = TEST_DATA.worksheet
browser, browserProfile = TEST_DATA.browser, TEST_DATA.browserProfile
performanceMode = TEST_DATA.performanceMode
perfCounter = TEST_DATA.perfCounter
dbRecordResults = TEST_DATA.dbRecordResults
totalTests = TEST_DATA.totalTests
refreshIIQ = TEST_DATA.refreshIIQ
#useAPICerts = TEST_DATA.useAPICerts     # Set this to True if Certs are used in the API testing
verboseReporting = TEST_DATA.verboseReporting

if env == "ANDROID" or env == "IOS":
    # pip3.4 install Appium-Python-Client -- will be part of Selenium 3.0 when released
    from appium import webdriver
    from appium.webdriver.common.mobileby import MobileBy as By
    if env == "ANDROID":
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '4.2'
        desired_caps['deviceName'] = 'Android Emulator'
        desired_caps['app'] = PATH('../../../apps/selendroid-test-app.apk')
    elif env == "IOS":
        desired_caps['platformName'] = 'iOS'
        desired_caps['platformVersion'] = '7.1'
        desired_caps['deviceName'] = 'iPhone Simulator'
        desired_caps['app'] = PATH('../../apps/UICatalog.app.zip')
    driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
else:
    from selenium import webdriver
    from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.firefox.webdriver import FirefoxProfile

#---------- VARIABLE DECLARATIONS ----------
driver, activeTab, displayValue = ("",) * 3
errorList, attributeValues = ([],)*2
boolValue = False
testSummary = ""
testDuration = ""

testCount = 0
errorCount = 0
counter0 = ""
counter1 = ""
indent = "   "
tableAlias = ""
tableActionAlias = ""
FASUserName = ""
startTime = ""
stopTime = ""


#---------- LOG FILE SETUP ----------
resultsfile = worksheet + datetime.now().strftime("_%m%d_%H%M")
logfile = resultsfile + "_" + env + '.log'
imagefile = resultsfile + '.png' #move and change to include row number

if sys.platform == "darwin":
    LogFolder = "../Logs/"
    os.system("touch " + LogFolder + logfile)
elif sys.platform == "windows" or sys.platform == "win32":
    LogFolder = "..\\Logs\\"
    # On Windows, must ensure Logs folder exists.
    if not os.path.isdir(LogFolder):
        os.mkdir(LogFolder)

#--------- AWS SETUP ---------
awsAccessKey = "test_access_key"
awsSecretAccessKey = "test_secret_key"
awsRegion = "us-east-1"
awsOutput = "json"

#--------- EMAIL SETUP ---------
sendmail_host = "host.harvard.edu"
mail_username = "harv.test@gmail.com"
mail_password = "mail_password"
# set to Y if test log file should be included in the email
add_attachment = "Y"

#------- APPLICATION SETUP BY ENV -------    
if env == "P-3":
    DBServer = 'iamdev-rds.dev' ## 11g database
    DBPermissions = {"name": "password"}
    
    HULDAPServer = ["huldap-direct2master", 636, True]
    HULDAPPermissions = ["cn=Directory Manager", "password"]
    
    HLDAPServer = ["ldap-devops-master", 636, True]
    HLDAPPermissions = ["CN=Directory Manager", 'password']
    FASLDAPServer = ["ldap-dev-1.unix",389, False]
    FASLDAPPermissions = ["uid=name,ou=services,o=hascs,dc=fas,dc=harvard,dc=edu", "password"]  #
    
    UnifiedLDAPServer = ["ldap-master.dev.iam.harvard.edu", 636, True]
    UnifiedLDAPPermissions = ["uid=qa-tester,ou=ldap,dc=harvard,dc=edu", "password"]     
    
    MidasURL = "https://midas.dev"
    MidasURLBypass = "https://midas.dev"
    iiqURL = "https://iiq.dev"
    #iiqPermissions = {"iamautoqa": "admin", "spadmin": "YWRtaW4="}  #482N4CJnB9dL
    harvardKeyURL = "https://key.dev"
    grouperURL = "https://grouper.dev"
    userLists = {"username": 'password'}
    #IdentityAPIURL = "http://localhost:8080"
    IdentityAPIURL = "https://idservice.dev"
    XIDURL = "https://xid.dev"
    XIDAdminURL = "https://xid.dev"
    
elif env == "P-2":
    DBServer = 'iamdev-rds.qa' ## 11g database
    DBPermissions = {"name": "password"}
    
    HULDAPServer = ["huldap-direct2master", 636, True]
    HULDAPPermissions = ["cn=Directory Manager", "password"]
    
    HLDAPServer = ["ldap-devops-master", 636, True]
    HLDAPPermissions = ["CN=Directory Manager", 'password']
    FASLDAPServer = ["ldap-qa-1.unix",389, False]
    FASLDAPPermissions = ["uid=name,ou=services,o=hascs,dc=fas,dc=harvard,dc=edu", "password"]  #
    
    UnifiedLDAPServer = ["ldap-master.qa.iam.harvard.edu", 636, True]
    UnifiedLDAPPermissions = ["uid=qa-tester,ou=ldap,dc=harvard,dc=edu", "password"]     
    
    MidasURL = "https://midas.qa"
    MidasURLBypass = "https://midas.qa"
    iiqURL = "https://iiq.qa"
    #iiqPermissions = {"iamautoqa": "admin", "spadmin": "YWRtaW4="}  #482N4CJnB9dL
    harvardKeyURL = "https://key.qa"
    grouperURL = "https://grouper.qa"
    userLists = {"username": 'password'}
    #IdentityAPIURL = "http://localhost:8080"
    IdentityAPIURL = "https://idservice.qa"
    XIDURL = "https://xid.qa"
    XIDAdminURL = "https://xid.qa"
    
elif env == "P-1":
    DBServer = 'iamdev-rds.stg' ## 11g database
    DBPermissions = {"name": "password"}
    
    HULDAPServer = ["huldap-stg", 636, True]
    HULDAPPermissions = ["cn=Directory Manager", "password"]
    
    HLDAPServer = ["ldap-stg-master", 636, True]
    HLDAPPermissions = ["CN=Directory Manager", 'password']
    FASLDAPServer = ["ldap-stg-1.unix",389, False]
    FASLDAPPermissions = ["uid=name,ou=services,o=hascs,dc=fas,dc=harvard,dc=edu", "password"]  #
    
    UnifiedLDAPServer = ["ldap-master.stg.iam.harvard.edu", 636, True]
    UnifiedLDAPPermissions = ["uid=tester,ou=ldap,dc=harvard,dc=edu", "password"]     
    
    MidasURL = "https://midas.stg"
    MidasURLBypass = "https://midas.stg"
    iiqURL = "https://iiq.stg"
    #iiqPermissions = {"iamautoqa": "admin", "spadmin": "YWRtaW4="}  #482N4CJnB9dL
    harvardKeyURL = "https://key.stg"
    grouperURL = "https://grouper.stg"
    userLists = {"username": 'password'}
    #IdentityAPIURL = "http://localhost:8080"
    IdentityAPIURL = "https://idservice.stg"
    XIDURL = "https://xid.stg"
    XIDAdminURL = "https://xid.stg"
    
elif env == "P-0":   #Pespu6Ex
    DBServer = 'iam-rds' ## 11g database
    DBPermissions = {"name": "password"}
    
    HULDAPServer = ["huldap-direct2master", 636, True]
    HULDAPPermissions = ["cn=Directory Manager", "password"]
    
    HLDAPServer = ["ldap-master", 636, True]
    HLDAPPermissions = ["CN=Directory Manager", 'password']
    FASLDAPServer = ["ldap.unix",389, False]
    FASLDAPPermissions = ["uid=name,ou=services,o=hascs,dc=fas,dc=harvard,dc=edu", "password"]  #
    
    UnifiedLDAPServer = ["ldap-master", 636, True]
    UnifiedLDAPPermissions = ["uid=tester,ou=ldap,dc=harvard,dc=edu", "password"]     
    
    MidasURL = "https://midas"
    MidasURLBypass = "https://midas"
    iiqURL = "https://iiq"
    #iiqPermissions = {"iamautoqa": "admin", "spadmin": "YWRtaW4="}  #482N4CJnB9dL
    harvardKeyURL = "https://key"
    grouperURL = "https://grouper"
    userLists = {"username": 'password'}
    #IdentityAPIURL = "http://localhost:8080"
    IdentityAPIURL = "https://idservice"
    XIDURL = "https://xid"
    XIDAdminURL = "https://xid"
    

#------- LOGGING SETUP -------
if not os.path.exists(LogFolder):
    os.makedirs(LogFolder)
        
logging.basicConfig(level=logging.INFO,
                    format='%(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=LogFolder + logfile,
                    filemode='w')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)
logger1 = logging.getLogger('Info')
logger2 = logging.getLogger('Error')

def CloseLogFile():
    logging.shutdown()
    return

#---------- CLIENT CERT SELECTION ----------
if clientConfig[:clientConfig.find(" ")] == "FAS_AURORA":
    clientName = "fasa"
else:
    clientName = clientConfig[:clientConfig.find(" ")].lower()

clientPermissions = clientConfig[clientConfig.find(" ")+ 1:].lower()
    
if env == "P-0":
    cert_file_path = "../Certs/PROD/prodclient_iam_harvard_edu_cert.cer.pem"
    key_file_path = "../Certs/PROD/prodclient.iam.harvard.edu.key.pem"
    combined_path = "../Certs/PROD/prodclient_combined.pem"     # combined cert + key
    CA_file_path = "../Certs/QA/AddTrust_External_CA_Root.pem"
else:
    cert_file_path = "../Certs/TEST/" + clientName + "client-" + clientPermissions + "-qa_iam_harvard_edu_cert.cer.pem"
    key_file_path = "../Certs/TEST/" + clientName + "client-" + clientPermissions + "-qa.iam.harvard.edu.key.pem"
    combined_path = "../Certs/TEST/"  + clientName + "client-" + clientPermissions + "-qa_combined.pem"      # combined cert + key
    CA_file_path = "../Certs/TEST/AddTrust_External_CA_Root.pem"

    #cert_file_path = "../Certs/QA/stgclient3_iam_harvard_edu_cert.cer.pem"
    #key_file_path = "../Certs/QA/stgclient3_iam_harvard_edu_key.pem"
    #combined_path = "../Certs/QA/test.pem"       # combined cert + key
    #CA_file_path = "../Certs/QA/AddTrust_External_CA_Root.pem"

#qa:w5fkXlR3nRJK

