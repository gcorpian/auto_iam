#---------- IMPORTS ----------
import sys

#-------CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
tableActionAlias = ""


def MappingTable(record):
    #converts the screen value from the spreadsheet into a function that controls how interactions take place.
    
    available_mappings = {
        "LOGIN": Login,
        "LOGOUT": Logout,
        "CREATE GROUP": CreateGroup,
        "CREATE ATTRIBUTE": CreateAttribute,
        "CREATE FOLDER": CreateFolder,
        "ADD MEMBERS": AddMembers,
        "QUICK LINKS": QuickLinks,
        "TREEVIEW": TreeView,
        "BROWSE FOLDERS": BrowseFolders,
        "MY GROUPS": MyGroups,
        "GROUP DETAIL": GroupDetail,
        "COPY GROUP": CopyGroup,
        "GROUP PRIVILEGES": GroupPrivileges,
        "PERSON DETAIL": PersonDetail,
        "MY FOLDERS": MyFolders,
        "NEW FOLDER": NewFolder,
        "DELETE FOLDER": DeleteFolder,
        "FOLDER DETAIL": FolderDetail,
        "MY FAVORITES": MyFavorites,
        "MY SERVICES": MyServices,
        "MY ACTIVITY": MyActivity,
        "MISCELLANEOUS": Miscellaneous,
        "ADMIN UI": AdminUI,
        "LITE UI": LiteUI,
        "STATUS CHECK": StatusCheck
        } 
    MappedObjects = available_mappings.get(record["screen"], lambda x: None)(record)

    UTILS.UseScreenObjects(MappedObjects, record)

    UTILS.StatusCheck(record)
    
def CreateGroup(record):
    UTILS.WaitForObject("XPATH", "html/body/div[2]/div[2]/footer/p", record, 10)
    
    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div[2]/div[2]/div[2]/div[1]/a[2]").click()
    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div[2]/div[2]/div[2]/div[1]/ul/li[3]/a").click()
    
    return {"CREATE IN THIS FOLDER": ".//*[@id='parentFolderComboId']",
            "GROUP NAME": ".//*[@id='groupName']",
            "GROUP ID": ".//*[@id='groupId']",
            "DESCRIPTION": ".//*[@id='groupDescription']",
            "ASSIGN PRIVILEGES TO EVERYONE": {"READ": ".//*[@id='addGroupForm']/div[5]/div[1]/div/label[1]/input",
                                              "VIEW": ".//*[@id='addGroupForm']/div[5]/div[1]/div/label[2]/input",
                                              "OPTIN": ".//*[@id='addGroupForm']/div[5]/div[1]/div/label[3]/input",
                                              "OPTOUT": ".//*[@id='addGroupForm']/div[5]/div[1]/div/label[4]/input",
                                              "ATTRIBUTE READ": ".//*[@id='addGroupForm']/div[5]/div[1]/div/label[5]/input"},
            "TYPE": {"GROUP": "",
                     "ROLE": ""},
            "ACTION": {"SEARCH FOR A FOLDER": ".//*[@id='submitLogin']",
                       "SHOW ADVANCED PROPERTIES": ".//*[@id='addGroupForm']/p[1]/a",
                       "HIDE ADVANCED PROPERTIES": ".//*[@id='addGroupForm']/p[2]/a",
                       "SAVE": ".//*[@id='addGroupForm']/div[6]/a[1]",
                       "EDIT THE ID": ".//*[@id='nameDifferentThanIdId']",
                       "CANCEL": ".//*[@id='addGroupForm']/div[6]/a[2]"}}
    
    
def CreateAttribute(record):
    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div[2]/div[2]/div[2]/div[1]/a[2]").click()
    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div[2]/div[2]/div[2]/div[1]/ul/li[1]/a").click()
    
    return {"CREATE IN THIS FOLDER": ".//*[@id='parentFolderComboId']",
            "SEARCH FOR FOLDER": ".//*[@id='addAttributeDefForm']/div[1]/div/span/a",
            "ATTRIBUTE DEFINITION ID": ".//*[@id='attributeDefId']",
            "DESCRIPTION": ".//*[@id='attributeDefDescription']",
            "TYPE": ".//*[@id='attributeDefTypeId']",
            "MULTI-ASSIGNABLE": ".//*[@id='addAttributeDefForm']/div[6]/div/input",
            "VALUE TYPE": ".//*[@id='attributeDefValueTypeId']",
            "MULTI-VALUED": ".//*[@id='addAttributeDefForm']/div[8]/div/input",
            "SHOW ADVANCED PROPERTIES": ".//*[@id='addAttributeDefForm']/p[1]/a",
            "HIDE ADVANCED PROPERTIES": ".//*[@id='addAttributeDefForm']/p[2]/a",
            "HUID": "//tr[2]/td/input",
            "ASSIGN PRIVIEGES TO EVERYONE":{"ADMIN": ".//*[@id='addAttributeDefForm']/div[9]/div/div/label[1]/input",
                                            "UPDATE": ".//*[@id='addAttributeDefForm']/div[9]/div/div/label[2]/input",
                                            "READ": ".//*[@id='addAttributeDefForm']/div[9]/div/div/label[3]/input",
                                            "VIEW": ".//*[@id='addAttributeDefForm']/div[9]/div/div/label[4]/input",
                                            "OPTIN": ".//*[@id='addAttributeDefForm']/div[9]/div/div/label[5]/input",
                                            "OPTOUT": ".//*[@id='addAttributeDefForm']/div[9]/div/div/label[6]/input",
                                            "ATTRIBUTE READ": ".//*[@id='addAttributeDefForm']/div[9]/div/div/label[7]/input",
                                            "ATTRIBUTE UPDATE": ".//*[@id='addAttributeDefForm']/div[9]/div/div/label[8]/input"},
            "ASSIGN TO": {"ATTRIBUTE DEFINITION": ".//*[@id='attributeDefToEditAssignToAttributeDefId']",
                          "ATTRIBUTE DEFINITION ATTRIBUTE ASSIGNMENT": ".//*[@id='attributeDefToEditAssignToAttributeDefAssignId']",
                          "FOLDER": ".//*[@id='attributeDefToEditAssignToStemId']",
                          "FOLDER ATTRIBUTE ASSIGNMENT": ".//*[@id='attributeDefToEditAssignToStemAssignId']",
                          "GROUP/ROLE/LOCAL ENTITY": ".//*[@id='attributeDefToEditAssignToGroupId']",
                          "GROUP/ROLE/LOCAL ENTITY ATTRIBUTE ASSIGNMENT": ".//*[@id='attributeDefToEditAssignToGroupAssignId']",
                          "MEMBER": ".//*[@id='attributeDefToEditAssignToMemberId']",
                          "MEMBER ATTRIBUTE ASSIGNMENT": ".//*[@id='attributeDefToEditAssignToMemberAssignId']",
                          "MEMBERSHIP": ".//*[@id='attributeDefToEditAssignToMembershipId']",
                          "MEMBERSHIP ATTRIBUTE ASSIGNMENT": ".//*[@id='attributeDefToEditAssignToMembershipAssignId']",
                          "MEMBERSHIP - IMMEDIATE ONLY": ".//*[@id='attributeDefToEditAssignToImmediateMembershipId']",
                          "MEMBERSHIP - IMMEDIATE ONLY - ATTRIBUTE ASSIGNMENT": ".//*[@id='attributeDefToEditAssignToImmediateMembershipAssignId']"},
            "ACTION": {"SAVE": ".//*[@id='addAttributeDefForm']/div[10]/a[1]",
                       "CANCEL": ".//*[@id='addAttributeDefForm']/div[10]/a[2]"}}
    
    
def CreateFolder(record):
    UTILS.WaitForObject("XPATH", "html/body/div[2]/div[2]/footer/p", record, 10)
    
    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div[2]/div[2]/div[2]/div[1]/a[2]").click()
    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div[2]/div[2]/div[2]/div[1]/ul/li[2]/a").click()
    
    return {"CREATE IN THIS FOLDER": ".//*[@id='parentFolderComboId']",
            "SEARCH FOR FOLDER": ".//*[@id='addStemForm']/div[1]/div/span/a",
            "FOLDER NAME": ".//*[@id='stemName']",
            "FOLDER ID": ".//*[@id='stemId']",
            "DESCRIPTION": ".//*[@id='stemDescription']",
            "ACTION": {"EDIT THE ID": "",
                       "SAVE": ".//*[@id='addStemForm']/div[5]/a[1]",
                       "CANCEL": ".//*[@id='addStemForm']/div[5]/a[2 ]"}}
    
    
def AddMembers(record):
    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div[2]/div[2]/div[2]/div[1]/a[2]").click()
    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div[2]/div[2]/div[2]/div[1]/ul/li[5]/a").click()
    
    return {"ADD MEMBERS TO GROUP": ".//*[@id='groupImportGroupComboId']",
            "SEARCH FOR A GROUP": ".//*[@id='importGroupFormId']/div[1]/div/div[1]/a",
            "HOW TO ADD MEMBERS":{"SEARCH FOR MEMBERS TO ADD": ".//*[@id='importGroupFormId']/div[2]/div/label[1]/input",
                                  "COPY/PASTE A LIST OF MEMBER IDS": ".//*[@id='importGroupFormId']/div[2]/div/label[2]/input",
                                  "IMPORT A FILE": ".//*[@id='importGroupFormId']/div[2]/div/label[3]/input"},
            "MEMBER NAME OR ID": ".//*[@id='groupAddMemberComboId']",
            "ENTER MEMBER IDS":".//*[@id='entityListId']",
            "SEARCH FOR AN ENTITY": ".//*[@id='importGroupFormId']/div[3]/div/div[1]/a",
            "REPLACE EXISTING MEMBERS": ".//*[@id='importGroupFormId']/div[6]/div/label/input",
            "ACTION": {"ADD ANOTHER MEMBER": ".//*[@id='importGroupFormId']/div[1]/div/div[2]/div/a",
                       "ADD MEMBERS": ".//*[@id='importGroupFormId']/div[7]/a[1]",
                       "BROWSE": ".//*[@id='importCsvFileId']",
                       "CANCEL": ".//*[@id='importGroupFormId']/div[7]/a[2]"}}
    

def Login(record):
    if CONFIG.driver == "":
        CONFIG.driver = UTILS.OpenBrowser(CONFIG.browser)
    
    if CONFIG.driver.current_url != CONFIG.grouperURL:
        CONFIG.driver.get(CONFIG.grouperURL)
    
    UTILS.CheckValueCount(record)

    return {"LOGIN ID": ".//*[@id='username']",
            "LOGIN NAME": ".//*[@id='username']",
            "PASSWORD": ".//*[@id='password']",
            "HUID": "//tr[2]/td/input",
            "ACTION": {"LOGIN": ".//*[@id='submitLogin']",
                       "NEW USER": "link_text:	",
                       "SUBMIT": "//tr[3]/td/input"}}    
    
        
def QuickLinks(record):
    UTILS.WaitForObject("XPATH", ".//*[@id='liteiu-link']", record, 10)
    
    return {"ACTION": {"MY GROUPS": ".//*[@id='demo2']/div/ul/li[1]/a",
                       "MY FOLDERS": ".//*[@id='demo2']/div/ul/li[2]/a",
                       "MY FAVORITES": ".//*[@id='demo2']/div/ul/li[3]/a",
                       "MY SERVICES": ".//*[@id='demo2']/div/ul/li[4]/a",
                       "MY ACTIVITY": ".//*[@id='demo2']/div/ul/li[5]/a",
                       "MISCELLANEOUS": ".//*[@id='demo2']/div/ul/li[6]/a",
                       "ADMIN UI": ".//*[@id='adminiu-link']",
                       "LITE UI": ".//*[@id='liteiu-link']"}}
                       
def Logout(record):
    UTILS.WaitForObject("XPATH", "//a[@class='navbar-link' and @href='../../logout.do']", record, 10)
    
    return {"ACTION": {"LOGOUT": "//a[@class='navbar-link' and @href='../../logout.do']"}}

def TreeView(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"ACTION": ".//span[text()='" + expectedValue.lower() + "']"}
                       
                       
def BrowseFolders(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}
                       
                       
def MyGroups(record):
    return {"FILTER FOR": ".//*[@id='myGroupsFilterId']",
            "FOLDER": "table:row:.//*[@id='myGroupsResultsId']/table/tbody/tr,/td[1]/a,1;/td[1]/a",
            "GROUP NAME": "tableAlias/td[2]/a",
            "SHOW": ".//*[@id='show-entries']",
            "TABS": {"GROUPS I MANAGE": ".//*[@class='nav nav-tabs']/li[1]/a",
                     "MY MEMBERSHIPS": ".//*[@class='nav nav-tabs']/li[2]/a",
                     "GROUPS I CAN JOIN": ".//*[@class='nav nav-tabs']/li[3]/a"},
            "ACTION": {"APPLY FILTER": ".//*[@id='myGroupsForm']/div/div[3]/a[1]",
                       "RESET": ".//*[@id='myGroupsForm']/div/div[3]/a[2]",
                       "JOIN GROUP": "tableAlias/td[3]/a",
                       "OK": "//input[@value='OK']"}}


def GroupDetail(record):
    UTILS.WaitForObject("XPATH", "html/body/div[2]/div[2]/footer/p", record, 10)
    return {"FILTER FOR": ".//*[@id='people-filter']",
            "MEMBER NAME": ".//*[@id='table-filter']",
            "ENTITY NAME": "table:row:.//*[@id='membersToDeleteFormId']/table/tbody/tr,/td[2]/a,1;/td[2]/a",
            "MEMBERSHIP": "tableAlias/td[3]/a",
            "SHOW": ".//*[@id='show-entries']",
            "NAME": ".//*[@id='groupDetailsId']/table/tbody/tr[1]/td[2]",
            "PATH": ".//*[@id='groupDetailsId']/table/tbody/tr[2]/td[2]",
            "ID PATH": ".//*[@id='groupDetailsId']/table/tbody/tr[3]/td[2]",
            "ALTERNATE ID PATH": ".//*[@id='groupDetailsId']/table/tbody/tr[4]/td[2]",
            "ID": ".//*[@id='groupDetailsId']/table/tbody/tr[5]/td[2]",
            "CREATED": ".//*[@id='groupDetailsId']/table/tbody/tr[6]/td[2]",
            "CREATOR": ".//*[@id='groupDetailsId']/table/tbody/tr[7]/td[2]",
            "LAST EDITED": ".//*[@id='groupDetailsId']/table/tbody/tr[8]/td[2]",
            "LAST EDITED BY": ".//*[@id='groupDetailsId']/table/tbody/tr[9]/td[2]",
            "TYPE": ".//*[@id='groupDetailsId']/table/tbody/tr[10]/td[2]",
            "PRIVILEGES ASSIGNED TO EVERYONE": ".//*[@id='groupDetailsId']/table/tbody/tr[11]/td[2]",
            "COMPOSITE OWNER": ".//*[@id='groupDetailsId']/table/tbody/tr[12]/td[2]",
            "COMPOSITE FACTOR OF OTHER GROUPS": ".//*[@id='groupDetailsId']/table/tbody/tr[13]/td[2]",
            "ID INDEX": ".//*[@id='groupDetailsId']/table/tbody/tr[14]/td[2]",
            "UUID": ".//*[@id='groupDetailsId']/table/tbody/tr[15]/td[2]",
            "GROUP SHOWING": ".//*[@id='groupFilterResultsId']/div/div",
            "TABS": {"MEMBERS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[1]/a",
                     "PRIVILEGES": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[2]/a",
                     "MORE": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/a",
                     "THIS GROUP'S MEMBERSHIPS IN OTHER GROUPS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/ul/li[1]/a",
                     "THIS GROUP'S PRIVILEGES IN OTHER GROUPS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/ul/li[2]/a",
                     "THIS GROUP'S PRIVILEGES IN FOLDERS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/ul/li[3]/a",
                     "THIS GROUP'S PRIVILEGES IN ATTRIBUTE DEFINITIONS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/ul/li[4]/a",
                     "THIS GROUP'S PRIVILEGES INHERITED FROM FOLDERS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/ul/li[5]/a",
                     "INHERITED PRIVILEGES ASSIGNED TO MEMBERS OF THIS GROUP": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/ul/li[6]/a"},
            "ACTION": {"ADD MEMBERS": ".//*[@id='show-add-block']",
                       "MORE": ".//*[@id='stemDetailsMoreId']/a",
                       "LESS": ".//*[@id='stemDetailsLessId']/a",
                       "MORE ACTIONS": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/a",
                       "ADD TO MY FAVORITES": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[1]/a",
                       "REMOVE FROM MY FAVORITES": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[1]/a",
                       "JOIN GROUP": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[2]/a",
                       "LEAVE GROUP": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[2]/a",
                       "COPY GROUP": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[4]/a",
                       "DELETE GROUP": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[5]/a",
                       "EDIT GROUP": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[6]/a",
                       "EDIT COMPOSITE": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[7]/a",
                       "MOVE GROUP": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[8]/a",
                       "EXPORT MEMBERS": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[10]/a",
                       "IMPORT MEMBERS": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[11]/a",
                       "REMOVE ALL MEMBERS": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[12]/a",
                       "VIEW AUDIT LOG": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[14]/a",
                       "ADMIN UI": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[16]/a",
                       "REMOVE SELECTED MEMBERS": "",
                       "APPLY FILTER": ".//*[@id='myGroupsForm']/div/div[3]/a[1]",
                       "RESET": ".//*[@id='myGroupsForm']/div/div[3]/a[2]",
                       "ACTIONS": "tableAlias/td[4]/div/a",
                       "EDIT MEMBERSHIP AND PRIVILEGES": "tableAlias/td[4]/div/ul/li[1]/a",
                       "REVOKE MEMBERSHIP": "tableAlias/td[4]/div/ul/li[2]/a",
                       "OK": "//input[@value='OK']",
                       "SELECT ENTITY": "tableAlias/td[1]/label/input"}}

def CopyGroup(record): 
    return {"NEW GROUP NAME": ".//*[@id='people-filter']",
            "NEW GROUP ID": ".//*[@id='people-filter']",
            "COPY INTO THIS FOLDER": ".//*[@id='people-filter']",
            "OPTIONS": {"COPY ATTRIBUTES": ".//*[@id='myGroupsForm']/div/div[3]/a[1]",
                        "COPY LIST MEMBERSHIPS OF THE GROUP": ".//*[@id='myGroupsForm']/div/div[3]/a[2]",
                        "COPY PRIVILEGES OF THE GROUP": "tableAlias/td[3]/a",
                        "COPY LIST MEMBERSHI:PS WHERE THE GROUP IS A MEMBER OF OTHER GROUPS": ".//*[@id='myGroupsForm']/div/div[3]/a[2]",
                        "JOIN GROUP": "tableAlias/td[3]/a",
                        "OK": "//input[@value='OK']"},
            "ACTION": {"APPLY FILTER": ".//*[@id='myGroupsForm']/div/div[3]/a[1]",
                       "RESET": ".//*[@id='myGroupsForm']/div/div[3]/a[2]",
                       "JOIN GROUP": "tableAlias/td[3]/a",
                       "OK": "//input[@value='OK']"}}

def GroupPrivileges(record):
    return {"FILTER FOR": ".//*[@id='people-filter']",
            "FILTER FOR": ".//*[@id='people-filter']"
            }
    


def PersonDetail(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"FILTER": ".//*[@id='myGroupsFilterId']",
            "GROUP NAME": "name:emailAddress.email",
            "FOLDER": "name:emailAddress.email",
            "GROUP": "name:emailAddress.email",
            "MEMBERSHIP": "name:emailAddress.email",
            "UPDATE TYPE": "",
            "GROUP": "",
            "ADMIN": "",
            "READ": "",
            "UPDATE": "",
            "OPTIN": "",
            "OPTOUT": "",
            "ATTRIBUTE READ": "",
            "ATTRIBUTE UPDATE": "",
            "VIEW": "",
            
            "TABS": {"MEMBERSHIPS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[1]/a",
                     "GROUP PRIVILEGES": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[2]/a]",
                     "FOLDER PRIVILEGES": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[1]/a",
                     "ATTRIBUTE PRIVILEGES": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[2]/a]",
                     "MORE": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/a"},
            "ACTION": {"APPLY FILTER": ".//*[@id='myGroupsForm']/div/div[3]/a[1]",
                       "RESET": ".//*[@id='myGroupsForm']/div/div[3]/a[2]",
                       "REMOVE SELECTED GROUPS": "//input[@value='OK']",
                       "EDIT MEMBERSHIP AND PRIVILEGES": "",
                       "REVOKE MEMBERSHIP": "",
                       "UPDATE SELECTED": "",
                       "TRACE PRIVILEGES": "",
                       "VIEW GROUP": ""}}

def MyFolders(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}
                       
                       
def MyFavorites(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}
                       
                       
def MyServices(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}
                       

def MyActivity(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}
                       
                      
def Miscellaneous(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}
                       
                       
def AdminUI(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}


def LiteUI(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}

def FindRow(tableAlias, appendPath, expectedValue, objectType):
    try:
        table = CONFIG.driver.find_elements_by_xpath(tableAlias)
        for counter in range(1, len(table)+1, 1):
                tempAlias = tableAlias + "[" + str(counter) + "]" + appendPath
                #print("tempAlias: ")
                #print(tempAlias)
                try:
                    if CONFIG.driver.find_element_by_xpath(tempAlias).text.upper() == expectedValue.upper(): 
                        CONFIG.tableAlias = tableAlias + "[" + str(counter) + "]/"   
                        print(CONFIG.tempAlias)
                        return CONFIG.tableAlias
                except: pass   
    except: pass


def FolderDetail(record):
    UTILS.WaitForObject("XPATH", "html/body/div[2]/div[2]/footer/p", record, 10)
    return {"FILTER FOR": ".//*[@id='table-filter']",
            "NAME": "table:row:.//*[@id='stemFilterResultsId']/table/tbody/tr,/td/a,1;/td/a",
            "PATH": ".//*[@id='stemDetailsId']/table/tbody/tr[1]/td[2]",
            "ID PATH": ".//*[@id='stemDetailsId']/table/tbody/tr[2]/td[2]",
            "ALTERNATE ID PATH": ".//*[@id='stemDetailsId']/table/tbody/tr[3]/td[2]",
            "ID": ".//*[@id='stemDetailsId']/table/tbody/tr[3]/td[2]",
            "CREATED": ".//*[@id='stemDetailsId']/table/tbody/tr[3]/td[2]",
            "CREATOR": ".//*[@id='stemDetailsId']/table/tbody/tr[3]/td[2]",
            "LAST EDITED": ".//*[@id='stemDetailsId']/table/tbody/tr[3]/td[2]",
            "LAST EDITOR": ".//*[@id='stemDetailsId']/table/tbody/tr[3]/td[2]",
            "ID INDEX": ".//*[@id='stemDetailsId']/table/tbody/tr[3]/td[2]",
            "UUID": ".//*[@id='stemDetailsId']/table/tbody/tr[3]/td[2]",
            "TABS": {"FOLDER CONTENTS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[1]/a",
                     "PRIVILEGES": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[2]/a",
                     "MORE": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/a",
                     "PRIVILEGES INHERITED TO OBJECTS IN FOLDER": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/ul/li[1]/a",
                     "THIS FOLDERS PRIVILEGES INHERITED FROM ANCESTOR FOLDERS": ".//*[@id='grouperMainContentDivId']/div[2]/div/ul/li[3]/ul/li[2]/a"},
            "ACTION": {"EDIT FOLDER": ".//*[@id='stemMoreActionsButtonContentsDivId']/a",
                       "MORE": ".//*[@id='stemDetailsMoreId']/a",
                       "LESS": ".//*[@id='stemDetailsLessId']/a",
                       "MORE ACTIONS": ".//*[@id='stemMoreActionsButtonContentsDivId']/div/a",
                       "ADD TO MY FAVORITES": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[1]/a",
                       "REMOVE FROM MY FAVORITES": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[1]/a",
                       "CREATE NEW FOLDER": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[2]/a",
                       "CREATE NEW GROUP": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[2]/a",
                       "COPY FOLDER": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[4]/a",
                       "DELETE FOLDER": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[5]/a",
                       "EDIT FOLDER": ".//*[@id='groupMoreActionsButtonContentsDivId']/div/ul/li[6]/a",
                       "APPLY FILTER": ".//*[@id='myGroupsForm']/div/div[3]/a[1]",
                       "RESET": ".//*[@id='myGroupsForm']/div/div[3]/a[2]",
                       "ACTIONS": "tableAlias/td[4]/div/a",
                       "EDIT MEMBERSHIP AND PRIVILEGES": "tableAlias/td[4]/div/ul/li[1]/a",
                       "REVOKE MEMBERSHIP": "tableAlias/td[4]/div/ul/li[2]/a",
                       "OK": "//input[@value='OK']",
                       "SELECT ENTITY": "tableAlias/td[1]/label/input"}}

def NewFolder(record):
    UTILS.WaitForObject("XPATH", "html/body/div[2]/div[2]/footer/p", record, 10)
    return {"CREATE IN THIS FOLDER": ".//*[@id='parentFolderComboId']",
            "FOLDER NAME": ".//*[@id='stemName']",
            "FOLDER ID": ".//*[@id='stemId']",
            "DESCRIPTION": ".//*[@id='stemDescription']",
            "ACTION": {"EDIT THE ID": ".//*[@id='nameDifferentThanIdId']",
                       "SAVE": ".//*[@id='addStemForm']/div[5]/a[1]",
                       "CANCEL": ".//*[@id='addStemForm']/div[5]/a[2]"}}


def DeleteFolder(record):
    UTILS.WaitForObject("XPATH", "html/body/div[2]/div[2]/footer/p", record, 10)
    return {"ACTION": {"DELETE": ".//*[@id='addStemForm']/div[5]/a[1]",
                       "CANCEL": ".//*[@id='addStemForm']/div[5]/a[2]"}}

def StatusCheck(record):
    UTILS.WaitForObject("XPATH", "html/body/div[2]/div[2]/footer/p", record, 10)
    return {"STATUS": [".//*[@class='alert alert-error']", ".//*[@class='alert alert-success']"],
            "ACTION": {"CLOSE": ".//*[@id='messaging']/div/button"}}


    #Admin UI
    #New UI
    #Groups / roles / local entities
    #Membership update
    #Manage attributes and permissions




