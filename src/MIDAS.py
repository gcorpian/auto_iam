#---------- IMPORTS ----------
import poplib
import time
import sys

#------- CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
tableActionAlias = ""


#---------- FUNCTIONS ----------
def MappingTable(record):
    #converts the screen value from the spreadsheet into a function that controls how interactions take place.
    available_mappings = {
        "DETAILS": Details,
        "ROLE SELECT": SelectRole,
        "LOGIN": Login,
        "LOGIN BYPASS": LoginBypass,
        "IMPERSONATE": Login,
        "SEARCH": Search,
        "SEARCH RESULTS": SearchResults,
        "VERIFY PERSON DETAILS": VerifyPersonDetails,
        "CREATE PERSON": CreatePerson,
        "CREATE PERSON DETAILS": CreatePersonDetails,
        "CREATE PERSON SEARCH RESULTS": CreatePersonSearchResults,
        "HELP": Help,
        # "CREATE USER": CreateUser,
        "REPORTS": Reports,
        "TABS": Tabs,
        "ADD EMAIL": UpdateEmail,
        "ADD ADDRESS": UpdateAddress,
        "ADD DIRECTORY LISTING": AddDirectoryListing,
        "ADD NAME": AddName,
        "ADD ONBOARD EMAIL": UpdateEmail,
        "ADD LOGIN NAME": AddLoginName,
        "ADD LIBRARY BORROWER ROLE": AddLBRole,
        "ADD STUDENT ROLE": AddStudentRole,
        "ADD POI ROLE": AddPOIRole,
        "ADD ROLE": AddPOIRole,
        "EDIT NAME": AddName,
        "EDIT PERSON DETAILS": EditPersonDetails,
        "EDIT ADDRESS": UpdateAddress,
        "EDIT OFFICIAL EMAIL": UpdateEmail,
        "EDIT ONBOARD EMAIL": UpdateEmail,
        "EDIT DIRECTORY LISTING": AddDirectoryListing,
        "SELECT OFFICIAL EMAIL": SelectOfficialEmail,
        "UPDATE IMAGE": UpdateImage,
        "IDENTIFIERS": Identifiers,
        "IDENTITY RESOLUTION": IdentityResolution,
        "ROLE DETAILS": RoleDetails,
        "PROCESS EMAIL": ProcessEmail
        } 
    MappedObjects = available_mappings.get(record["screen"], lambda x: None)(record)
    if record["screen"] != "PROCESS EMAIL" or record["screen"] != "LOGIN BYPASS":
        UTILS.UseScreenObjects(MappedObjects, record)

    try:
        enum = CONFIG.driver.find_element_by_class_name("alertbox")
        if enum.is_displayed and enum.is_enabled:
            if record["results"] in ["N", "F"]:
                CONFIG.logger1.info (indent + "Alertbox error '%s' appeared as expected." % (enum.text))
            else:
                UTILS.ReportErrors(enum.text, record)
    except:
        pass
    

def UpdateAddress(record):
    UTILS.WaitForObject("NAME", "address.categoryCode", record, 10)
    
    return {"CATEGORY": "name:address.categoryCode",
            "ADDRESS MAIL REALM": "name:address.addressMailRealmCode",
            "ADDRESS 1": "name:address.address1",
            "ADDRESS 2": "name:address.address2",
            "ADDRESS 3": "name:address.address3",
            "CITY": "name:address.city",
            "STATE": "name:address.state",
            "ZIP": "name:address.postal",
            "COUNTRY": "name:address.countryCode",
            "LOCATION": "name:address.location",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']"}}


def UpdateEmail(record):
    UTILS.WaitForObject("NAME", "emailAddress.email", record, 10)
    
    return {"EMAIL ADDRESS": "name:emailAddress.email",
            "ONBOARD EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "OK": "//input[@value='OK']"}}
    

def UpdateOnboardEmail(record):
    return UpdateEmail(record)
    
    
def AddDirectoryListing(record):
    UTILS.WaitForObject("NAME", "employeeDirListing.officePhone", record, 10)
    
    return {"OFFICE PHONE": "name:employeeDirListing.officePhone",
            "EXT": "name:employeeDirListing.extension",
            "LOCATION": ["//td[2]/table/tbody/tr[5]/td[2]", "name:locationSearchParam"],
            "LOCATION ID": "name:employeeDirListing.locationIdentifier",
            "OFFICE FAX": "name:employeeDirListing.officeFax",
            "LISTING TITLE": "name:employeeDirListing.listingTitle",
            "LISTING STATUS": "name:employeeDirListing.listingStatus",
            "ACTION": {"SET": ".//input[@value='Set']",
                       "SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']"}}
    

def AddName(record):
    UTILS.WaitForObject("XPATH", "//table/tbody/tr[2]/td/table/tbody/tr[5]/td[2]", record, 10)

    return {"NAME TYPE": "//table/tbody/tr[4]/td[1]/select", 
            "PREFIX": "//table/tbody/tr[4]/td[2]/select",
            "LAST NAME": "//table/tbody/tr[4]/td[3]/input",
            "FIRST NAME": "//table/tbody/tr[4]/td[4]/input",
            "MIDDLE NAME": "//table/tbody/tr[4]/td[5]/input",
            "SUFFIX": "//table/tbody/tr[4]/td[6]/select",
            "ACTION": {"ADD": ".//input[@value='Add']",
                       "CLEAR": ".//input[@value='Clear']",
                       "SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']"}}


def AddLoginName(record):
    UTILS.WaitForObject("XPATH", "//table/tbody/tr[2]/td/table/tbody/tr[5]/td[2]", record, 10)

    return {"NAME": "//table/tbody/tr[5]/td/div/b",
            "LOGIN NAME": "//table/tbody/tr[8]/td[2]/input", 
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']"}}

def AddPOIRole(record):
    return {"ROLE TYPE": "name:role",
            "SCHOOL - UNIT": "name:poiRole.facultyCode",
            "SCHOOL CODE": "name:schoolCode",
            "DEPARTMENT":"name:poiRole.deptId",
            "COMPANY":"name:poiRole.company",
            "AUTHORIZER'S HUID": "name:poiRole.authorizingId",
            "AUTHORIZER ADMIN'S HUID": "name:poiRole.adminId",
            "ID LINE 1": "name:poiRole.description1",
            "ID LINE 2": "name:poiRole.description2",
            "START DATE": "splitDate://input[contains(@name,'poiRole.roleStartMonth')]+,//input[contains(@name,'poiRole.roleStartDay')]+,//input[contains(@name,'poiRole.roleStartYear')]",
            "END DATE": "splitDate://input[contains(@name,'poiRole.roleEndMonth')]+,//input[contains(@name,'poiRole.roleEndDay')]+,//input[contains(@name,'poiRole.roleEndYear')]",
            "COMMENTS": "name:poiRole.comments",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "BACK": ".//input[@value='< Back']",
                       "SELECT": ".//input[@value='Select']",                    
                       "NEXT": ".//input[@value='Next >']"}}


def AddLBRole(record):
    return {"LIBRARY BORROWER CODE": "name:libraryBorrower.libBorrCode",
            "START DATE": "splitDate://input[contains(@name,'poiRole.roleStartMonth')],//input[contains(@name,'poiRole.roleStartDay')],//input[contains(@name,'poiRole.roleStartYear')]",
            "END DATE": "splitDate://input[contains(@name,'poiRole.roleEndMonth')],//input[contains(@name,'poiRole.roleEndDay')],//input[contains(@name,'poiRole.roleEndYear')]",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "BACK": ".//input[@value='< Back']"}}


def AddStudentRole(record):
    return {"ROLE TYPE": "name:role",
            "SCHOOL": "name:student.schoolCode",
            "STATUS": "name:student.studentStatus",
            "DEPARTMENT": "name:student.studentDepartmentDescription",
            "DEGREE": "name:student.degree",
            "PROGRAM": "name:student.program",
            "END DATE": "splitDate://input[contains(@name,'student.endDateMonth')]+,//input[contains(@name,'student.endDateDay')]+,//input[contains(@name,'student.endDateYear')]",
            "TIME STATUS": "name:student.timeStatus",
            "RECORD HOUSE": "name:student.recordHouse",
            "RESIDENCE HOUSE": "name:student.residenceHouseDescription",
            "BOARD HOUSE": "name:student.boardHouseDescription",
            "BOARDING STATUS": {"ON BOARD": "//input[@name='student.boardingStatus' and @value='B']",
                                "NOT ON BOARD": "//input[@name='student.boardingStatus' and @value='N']"},
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']",
                       "BACK": ".//input[@value='< Back']",
                       "SELECT": ".//input[@value='Select']",                    
                       "NEXT": ".//input[@value='Next >']"}}


def Login(record):
    if CONFIG.driver == "":
        CONFIG.driver = UTILS.OpenBrowser(CONFIG.browser)
    
    if CONFIG.driver.current_url != CONFIG.MidasURL:
        CONFIG.driver.get(CONFIG.MidasURL)
    
    UTILS.CheckValueCount(record)
    #CONFIG.driver.get(CONFIG.MidasURL)

    return {"LOGIN ID": ".//*[@id='username']",
            "PASSWORD": ".//*[@id='password']",
            "HUID": "//tr[2]/td/input",
            "ACTION": {"LOGIN": ".//*[@id='submitLogin']",
                       "NEW USER": "link_text:	New user? Forgot your PIN / Password?",
                       "SUBMIT": "//tr[3]/td/input"}}
    
def LoginBypass(record):
    if CONFIG.driver == "":
        CONFIG.driver = UTILS.OpenBrowser(CONFIG.browser)
    
    CONFIG.driver.get(CONFIG.MidasURLBypass)
    
    return {"":""}

        
def SelectRole(record):      
    UTILS.WaitForObject("XPATH", "html/body/div[1]/table/tbody/tr[3]/td/table/tbody/tr[5]/td[2]/p", record, 10)
    
    return {"ROLE": ".//select[@name='role']",
            "ACTION": {"SELECT": " .//input[@value='Select']",
                       "NEXT": ".//input[@value='Next >']",
                       "CANCEL": ".//input[@value='Cancel']"}}

    
def VerifyPersonDetails(record):
    return {"HUID": "//td[2]/table/tbody/tr[3]/td[2]",
            "NAME TYPE": "//tr[4]/td[2]",
            "FIRST NAME": "//tr[5]/td[2]",
            "MIDDLE NAME": "//tr[6]/td[2]",
            "LAST NAME": "//tr[7]/td[2]",
            "BIRTH DATE": "//tr[8]/td[2]",
            "LAST FOUR SSN": "//tr[9]/td[2]",
            "PERSONA NON GRATA": "//tr[10]/td[2]",
            "LAST UPDATE DATE": "//tr[11]/td[2]",
            "ACTION": {"BACK": ".//input[@value='< Back']",
                       "CREATE": ".//input[@value='Create']"}}


def CreatePerson(record):
    return {"ROLE TYPE":"name:role",
            "SCHOOL - UNIT": "name:facultyCode",
            "SCHOOL CODE": "name:schoolCode",
            "DEPARTMENT": "name:deptId",
            "HUID": "name:universityId",
            "FIRST NAME": "name:firstName",
            "MIDDLE NAME": "name:middleName",
            "LAST NAME": "name:lastName",
            "PREFIX": "name:prefix",
            "SUFFIX": "name:suffix",
            "BIRTH MONTH": "name:birthMonth",
            "BIRTH DAY": "name:birthDate",
            "BIRTH YEAR": "name:birthYear",
            "LAST FOUR SSN": "name:fourDigitSsn",
            "GENDER": {"M": "//input[@name='person.gender' and @value='M']",
                       "F": "//input[@name='person.gender' and @value='F']",
                       "U": "//input[@name='person.gender' and @value='U']"},
            "EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"NEXT": ".//input[@value='Next >']",
                       "BACK": ".//input[@value='< Back']",
                       "CANCEL:": ".//input[@value='Cancel']",
                       "CREATE": ".//input[@value='Create']",
                       "SEARCH": ".//input[@value='Search']",
                       "REFINE SEARCH": ".//tr[4]/td/table/tbody/tr[2]/td[6]/input[@value='REFINE_SEARCH']",
                       "NEW SEARCH": ".//tr[4]/td/table/tbody/tr[2]/td[7]/input[@value='NEW_SEARCH']"}}

                
def CreatePersonDetails(record):
    return {"HUID": "name:person.universityId",
            "FIRST NAME": "name:name.firstName",
            "MIDDLE NAME": "name:name.middleName",
            "LAST NAME": "name:name.lastName",
            "PREFIX": "name:name.prefix",
            "SUFFIX": "name:name.suffix",
            "BIRTH DATE": "splitDate://input[contains(@name,'birthMonth')],//input[contains(@name,'birthDate')],//input[contains(@name,'birthYear')]",
            "LAST FOUR SSN": "name:nationalId.fourDiigitNationalId]",
            "GENDER": {"M": "//input[@name='person.gender' and @value='M']",
                       "F": "//input[@name='person.gender' and @value='F']",
                       "U": "//input[@name='person.gender' and @value='U']"},
            "EMAIL ADDRESS": "name:emailAddress.email",
            "ACTION": {"NEXT": ".//input[@value='Next >']",
                       "CREATE": ".//input[@value='Create']"}}



def CreatePersonSearchResults(record):
    return {"HUID": "table:row://table/tbody/tr[3]/td/form/table/tbody/tr/td/table/tbody/tr,/td/table/tbody/tr[1],1;/td[3]/div",
            "MATCH SCORE":"tableAlias/td[7]/div",
            "ACTION": {"REFINE SEARCH": "name:refineSearchButton",
                       "NEW SEARCH": "name:newSearchButton"}}
            

def EditPersonDetails(record):
    return {"BIRTH DATE": "splitDate://input[contains(@name,'birthYear')],//input[contains(@name,'birthMonth')],//input[contains(@name,'birthDay')]",
            "LAST FOUR SSN": "name:nationalId.nationalId]",
            "GENDER": {"M": "//input[@name='person.gender' and @value='F']",
                       "F": "//input[@name='person.gender' and @value='M']",
                       "U": "//input[@name='person.gender' and @value='U']"},
            "PERSONA NON GRATA STATUS": {"YES":"//input[@name='person.PNGStatus' and @value='Y']",
                                         "NO": "//input[@name='person.PNGStatus' and @value='N']"},
            "STUDENT SPECIAL STATUS": "name:stuGenRole.studentSpecialStatus",
            "ACTION": {"SAVE": ".//input[@value='Save']",
                       "CANCEL": ".//input[@value='Cancel']"}}


def EditPrivacyDetails(record):
    return {"PERSON": {"RESTRICTED": "//input[@name='viewablePrivacy.personPrivacy' and @value='Restricted']",
                       "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.personPrivacy' and @value='Department']",
                       "PRINT ONLY": "//input[@name='viewablePrivacy.personPrivacy' and @value='Print']",
                       "HARVARD": "//input[@name='viewablePrivacy.personPrivacy' and @value='Harvard']",
                       "PUBLIC": "//input[@name='viewablePrivacy.personPrivacy' and @value='Public']"},
             "EMAIL ADDRESS": {"RESTRICTED": "//input[@name='viewablePrivacy.emailPrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.emailPrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.emailPrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.emailPrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.emailPrivacy' and @value='Public']"},
             "IMAGE": {"RESTRICTED": "//input[@name='viewablePrivacy.imagePrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.imagePrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.imagePrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.imagePrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.imagePrivacy' and @value='Public']"},
             "EMPLOYEE ROLE": {"RESTRICTED": "//input[@name='viewablePrivacy.emplRolePrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.emplRolePrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.emplRolePrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.emplRolePrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.emplRolePrivacy' and @value='Public']"},
             "STUDENT ROLE": {"RESTRICTED": "//input[@name='viewablePrivacy.studRolePrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.studRolePrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.studRolePrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.studRolePrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.studRolePrivacy' and @value='Public']"},
             "SPECIAL ROLE": {"RESTRICTED": "//input[@name='viewablePrivacy.specRolePrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.specRolePrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.specRolePrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.specRolePrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.specRolePrivacy' and @value='Public']"},
             "OFFICE ADDRESS": {"RESTRICTED": "//input[@name='viewablePrivacy.offAddressPrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.offAddressPrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.offAddressPrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.offAddressPrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.offAddressPrivacy' and @value='Public']"},
             "OFFICE PHONE": {"RESTRICTED": "//input[@name='viewablePrivacy.offPhonePrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.offPhonePrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.offPhonePrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.offPhonePrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.offPhonePrivacy' and @value='Public']"},
             "OFFICE FAX": {"RESTRICTED": "//input[@name='viewablePrivacy.offFaxPrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.offFaxPrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.offFaxPrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.offFaxPrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.offFaxPrivacy' and @value='Public']"},
             "RESIDENCE ADDRESS": {"RESTRICTED": "//input[@name='viewablePrivacy.resAddressPrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.resAddressPrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.resAddressPrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.resAddressPrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.resAddressPrivacy' and @value='Public']"},
             "DORM ADDRESS": {"RESTRICTED": "//input[@name='viewablePrivacy.dormAddressPrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.dormAddressPrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.dormAddressPrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.dormAddressPrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.dormAddressPrivacy' and @value='Public']"},
             "HOME PHONE": {"RESTRICTED": "//input[@name='viewablePrivacy.homePhonePrivacy' and @value='Restricted']",
                        "DEPARTMENT/SCHOOL": "//input[@name='viewablePrivacy.homePhonePrivacy' and @value='Department']",
                        "PRINT ONLY": "//input[@name='viewablePrivacy.homePhonePrivacy' and @value='Print']",
                        "HARVARD": "//input[@name='viewablePrivacy.homePhonePrivacy' and @value='Harvard']",
                        "PUBLIC": "//input[@name='viewablePrivacy.homePhonePrivacy' and @value='Public']"},
             "ACTION": {"SAVE": ".//input[@value='Save']",
                        "CANCEL": ".//input[@value='Cancel']"}}
    

def Help(record):
    pass

def Search(record):
    #UTILS.WaitForObject("NAME", "button", record, 30)
    try:
        CONFIG.driver.find_element_by_xpath("//td[1]/table/tbody/tr[2]/td[1]/span/a[1]").click()
        time.sleep(1)
    except:
        pass

    return {"SEARCH TYPE": {"SIMPLE SEARCH": ".//tr[3]/td/table/tbody/tr/td[2]/span/a",
                            "ADVANCED SEARCH": ".//tr[3]/td/table/tbody/tr/td[2]/span/a"},
            "HUID": ".//input[@name='huid']",
            "UNIVERSITY ADID": "//input[contains(@name,'adid')]",
            "ALUMNI ID": "//input[contains(@name,'aluid')]",
            "SSN/ITIN": "//input[contains(@name,'ssn')]",
            "PHONE NUMBER": "//input[contains(@name,'phoneNumber')]",
            "LOGIN NAME": "//input[contains(@name,'loginName')]",
            "AUTHORIZER HUID": "//input[contains(@name,'authId')]",
            "AUTH ADMIN HUID": "//input[contains(@name,'authAdminId')]",
            "EMAIL ADDRESS": "//input[contains(@name,'emailAddress')]",
            "FIRST NAME": "//input[contains(@name,'firstName')]",
            "FIRST NAME SEARCH TYPE": {"EXACT": "//input[@name='firstNameType' and @value='Exact']",
                                       "STARTS WITH": "//input[@name='firstNameType' and @value='Starts with']",
                                       "CONTAINS": "//input[@name='firstNameType' and @value='Contains']",
                                       "SOUNDS LIKE": "//input[@name='firstNameType' and @value='Sounds like']"},
            "MIDDLE NAME": "//input[contains(@name,'middleName')]",
            "MIDDLE NAME SEARCH TYPE": {"EXACT": "//input[@name='middleNameType' and @value='Exact']",
                                       "STARTS WITH": "//input[@name='middleNameType' and @value='Starts with']",
                                       "CONTAINS": "//input[@name='middleNameType' and @value='Contains']",
                                       "SOUNDS LIKE": "//input[@name='middleNameType' and @value='Sounds like']"},
            "LAST NAME": "//input[contains(@name,'lastName')]",
            "LAST NAME SEARCH TYPE": {"EXACT": "//input[@name='lastNameType' and @value='Exact']",
                                       "STARTS WITH": "//input[@name='lastNameType' and @value='Starts with']",
                                       "CONTAINS": "//input[@name='lastNameType' and @value='Contains']",
                                       "SOUNDS LIKE": "//input[@name='lastNameType' and @value='Sounds like']"},
            "LISTING LOCATION": "//input[contains(@name,'location')]",
            "LISTING LOCATION SEARCH TYPE": {"EXACT": "//input[@name='locationType' and @value='Exact']",
                                             "STARTS WITH": "//input[@name='locationType' and @value='Starts with']",
                                             "CONTAINS": "//input[@name='locationType' and @value='Contains']",
                                             "SOUNDS LIKE": "//input[@name='locationType' and @value='Sounds like']"},
            "STATUS": {"EMPLOYEE": "//input[@name='employeeRoleFlag' and @type='checkbox']",
                       "STUDENT": "//input[@name='studentRoleFlag' and @type='checkbox']",
                       "LIBRARY BORROWER": "//input[@name='libBorrowerRoleFlag' and @type='checkbox']",
                       "PERSON OF INTEREST": "//input[@name='poiRoleFlag' and @type='checkbox']",
                       "ALUMNI": "//input[@name='aluRoleFlag' and @type='checkbox']",
                       "SCHOOL": "//select[@name='school']",
                       "SCHOOL - UNIT": "//select[@name='department']",
                       "CURRENT & NON-CURRENT": "//input[@value='BOTH' and @type='radio']",
                       "CURRENT ONLY": "//input[@value='CURRENT' and @type='radio']",
                       "NON-CURRENT ONLY": "//input[@value='NON-CURRENT' and @type='radio']"},
            "GEOCODES": "//select[@name='codes']",
            "ACTION": {"SEARCH": ["//input[@tabIndex='6' and @value='Search']",
                                  ".//tr[5]/td/form/table/tbody/tr/td[2]/table/tbody/tr[5]/td[2]/input",
                                  ".//tr[5]/td/form/table/tbody/tr/td[2]/table/tbody/tr[10]/td[2]/input",
                                  ".//tr[5]/td/form/table/tbody/tr/td[2]/table/tbody/tr[11]/td[2]/input"],
                       "EXACT SEARCH": ["//input[@tabIndex='7' and @value='Search']",
                                        ".//tr[11]/td[1]/input"],
                       "CUSTOM SEARCH": ["//input[@tabIndex='16' and @value='Search']", 
                                         ".//tr[15]/td[1]/input"],
                       "SIMPLE SEARCH": ".//tr[3]/td/table/tbody/tr/td[2]/span/a",
                       "ADVANCED SEARCH": ".//tr[3]/td/table/tbody/tr/td[2]/span/a",
                       "RESET": "//input[@name='button' and @value='Reset']",
                       "LOGOFF": "//input[@name='button' and @value='Logoff']"}}
     

def Reports(record):
    pass

def RoleDetails(record):
    return {"STATUS": "name:student.studentStatus",
            "DEPARTMENT": "name:student.studentStatus",
            "DEGREE": "name:student.degree",
            "PROGRAM": "name:student.program",
            "START DATE": "splitDate://input[contains(@name,'student.startDateMonth')],//input[contains(@name,'student.startDateDay')],//input[contains(@name,'student.startDateYear')]",
            "END DATE": "splitDate://input[contains(@name,'student.startDateMonth')],//input[contains(@name,'student.startDateDay')],//input[contains(@name,'student.startDateYear')]",
            "TIME STATUS": "name:student.timeStatus",
            "RECORD HOUSE": "name:student.recordHouse",
            "RESIDENCE HOUSE": "name:student.residenceHouse",
            "BOARD HOUSE": "name:student.boardHouse",
            "BOARDING STATUS": {"ON BOARD": "//input[@name='student.boardingStatus' and @value='B']",
                                "NOT ON BOARD": "//input[@name='student.boardingStatus' and @value='N']"},
            "ACTION": {"FINISH": "//input[@value='Finish']",
                       "ADD CONTACT DATA": "//input[@value='Add Contact Data']",
                       "CANCEL": "//input[@value='Cancel']",
                       "BACK": "//input[@name='button' and @value='Reset']"}}
    

def Details(record):
    UTILS.WaitForObject("XPATH", "//div[1]/table/tbody/tr[4]/td/table/tbody/tr/td/table/tbody/tr[5]/td[2]/p", record, 20)
    
    return {"ERROR": "//table[1]/tbody/tr/td[2]/li/span",
            "YOU ARE LOGGED IN AS": "//table/tbody/tr/td[1]/table/tbody/tr[2]/td[2]/span[2]",
            "ROLE": "//table/tbody/tr/td[1]/table/tbody/tr[3]/td[2]/span[2]",
            
            "IMAGE NUMBER": "",
            "PURGE INDICATOR": "",
            "IMAGE UPDATE DATE": "",

            "NAME TYPE": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr,Name,/td[1]/div,0;/td[1]/div",
            "NAME LAST": "tableAlias/td[2]/div",
            "NAME FIRST": "tableAlias/td[3]/div",
            "NAME MIDDLE": "tableAlias/td[4]/div",
            "NAME PREFIX": "tableAlias/td[5]/div",
            "NAME SUFFIX": "tableAlias/td[6]/div",
            "NAME UPDATE DATE": "tableAlias/td[7]/div",

            "NATIONAL ID": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr,Person Detail,/td[2]/a,0;/td[2]/a",
            "NATIONAL ID TYPE": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr,Person Detail,/td[4],0;/td[4]",
            "NATIONAL ID UPDATE DATE": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr,Person Detail,/td[6],0;/td[6]",
            "GENDER": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[2],Person Detail,/td[2],0;/td[2]",
            "BIRTH DATE": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[2],Person Detail,/td[4],0;/td[4]",
            "GENDER/DOB UPDATE DATE": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[2],Person Detail,/td[6],0;/td[6]",
            "ETHNICITY": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[3],Person Detail,/td[2],0;/td[2]",
            "PERSONA NON GRATA": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[3][,Person Detail,/td[4],0;/td[4]",
            "STUDENT SPECIAL STATUS": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[3],Person Detail,/td[6],0;/td[6]",
            "EPE STATUS": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[4],Person Detail,/td[2],0;/td[2]",
            "LONGER SERVICE": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[4],Person Detail,/td[4],0;/td[4]",
            #"UNIVERSITY NETID": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[4],Person Detail,/td[6],0;/td[6]",
            "UNIVERSITY NETID": "//tr[6]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[4]/td[6]",
            "DECEASED": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[5],Person Detail,/td[4],0;/td[4]",
            "ALUMNI ID": "table://tr[3]/td/table/tbody/tr,/td[1],/td[2]/table/tbody/tr[5],Person Detail,/td[6],0;/td[6]",

            "CLAIM OVERRIDE": "//tr[8]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[2] ",
            "CLAIM STATUS": "//tr[8]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[4]",
            "CLAIM STATUS DATE": "//tr[8]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[6]",
            "LOGIN NAME": "//tr[8]/td[2]/table/tbody/tr[4]/td/table/tbody/tr[2]/td[1]/div",
            "LOGIN NAME UPDATE SOURCE": "//tr[8]/td[2]/table/tbody/tr[4]/td/table/tbody/tr[2]/td[2]/div",
            "LOGIN NAME UPDATE DATE": "//tr[8]/td[2]/table/tbody/tr[4]/td/table/tbody/tr[2]/td[3]/div",
            
            "RECOVERY EMAIL ADDRESS": "table:row://tr[8]/td[2]/table/tbody/tr[6]/td/table/tbody/tr,[1]/div/a,1;[1]/div/a",
            "RECOVERY EMAIL ADDRESS TYPE": "tableAlias[2]/div",
            "RECOVERY EMAIL ADDRESS UPDATE SOURCE": "tableAlias[3]/div",
            "RECOVERY EMAIL ADDRESS UPDATE DATE": "tableAlias[4]/div",

            "ADDRESS CATEGORY": "table:row://table/tbody/tr[5]/td/table[1]/tbody/tr[2]/td[2]/table/tbody/tr,[1]/td[2],1;[1]/td[2]",
            "ADDRESS 1": "tableAlias[2]/td[2]",
            "ADDRESS 2": "tableAlias[3]/td[2]",
            "ADDRESS 3": "tableAlias[4]/td[2]",
            "CITY": "tableAlias[1]/td[4]",
            "STATE": "tableAlias[2]/td[4]",
            "POSTAL CODE": "tableAlias[3]/td[4]",
            "COUNTRY": "tableAlias[4]/td[4]",
            "MAIL REALM": "tableAlias[1]/td[6]",
            "ADDRESS LOCATION": "tableAlias[2]/td[6]",
            "ADDRESS SOURCE": "tableAlias[3]/td[6]",
            "ADDRESS UPDATE DATE": "tableAlias[4]/td[6]",

            "ALUMNI ADDRESS CATEGORY": "table:row://table[2]/tbody/tr[2]/td[2]/table/tbody/tr,[1]/td[2],1;[1]/td[2]",
            "ALUMNI ADDRESS 1": "tableAlias[2]/td[2]",
            "ALUMNI ADDRESS 2": "tableAlias[3]/td[2]",
            "ALUMNI ADDRESS 3": "tableAlias[4]/td[2]",
            "ALUMNI CITY": "tableAlias[1]/td[4]",
            "ALUMNI STATE": "tableAlias[2]/td[4]",
            "ALUMNI POSTAL CODE": "tableAlias[3]/td[4]",
            "ALUMNI COUNTRY": "tableAlias[4]/td[4]",
            "ALUMNI MAIL REALM": "tableAlias[1]/td[6]",
            "ALUMNI LOCATION": "tableAlias[2]/td[6]",
            "ALUMNI ADDRESS SOURCE": "tableAlias[3]/td[6]",
            "ALUMNI ADDRESS UPDATE DATE": "tableAlias[4]/td[6]","": "",

            "EMAIL ADDRESS": "table:row://table[3]/tbody/tr[2]/td[2]/table/tbody/tr,/td[2]/a,EMAIL;/td[2]/a",
            "EMAIL OFFICIAL": "tableAlias/td[3]/img[@src='image/check.gif']",
            "EMAIL UPDATE DATE": "tableAlias/td[4]",

            "ONBOARD EMAIL ADDRESS": "//table[4]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]",
            "ONBOARD EMAIL UPDATE DATE": "//table[4]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[4]",

            "ALUMNI EMAIL ADDRESS": "//table[5]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]",
            "ALUMNI EMAIL PREFERRED": "//table[5]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[3]/img",
            "ALUMNI EMAIL UPDATE DATE": "//table[5]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[4]",

            "MAIL CENTER": "//table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[1]/td[2]",
            "MAILBOX NUMBER": "//table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[1]/td[4]",
            "DORM LOCATION": "//table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[1]/td[6]",
            "LOCATION IDENTIFIER": "//table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[2]/td[2]",
            "PHONE NUMBER": "//table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[2]/td[4]",
            "MOBILE PHONE": "//table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[2]/td[6]",
            "CITY/STATE": "//table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[3]/td[2]",

            "LISTING TITLE": "//table[6]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]",
            "OFFICE PHONE": "//table[6]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]",
            "LOCATION": "//table[6]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]",
            "LISTING STATUS": "//table[6]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[4]",
            "PRINT ORDER": "//table[6]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[6]",
            "OFFICE FAX": "//table[6]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[6]",
            "DIRECTORY LISTING UPDATE DATE":"//table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td/table/tbody/tr[3]/td[6]",
            
            "SCHOOL": "table:row://tr[7]/td/table[1]/tbody/tr[2]/td[2]/table/tbody/tr,/td[2],1;/td[2]",
            "STUDENT STATUS": "tableAlias[2]/td[2]",
            "DEPARTMENT": "tableAlias[3]/td[2]",
            "DEGREE": "tableAlias[4]/td[2]",
            "PROGRAM": "tableAlias[5]/td[2]",
            "YEAR": "tableAlias[6]/td[2]",
            "GRADUATION": "tableAlias[7]/td[2]",
            "LAST ATTENDANCE": "tableAlias[8]/td[2]",
            "ROLE TERM": "tableAlias[9]/td[2]",
            "STUDENT TYPE": "tableAlias[2]/td[4]",
            "TIME STATUS": "tableAlias[3]/td[4]",
            "RECORD HOUSE": "tableAlias[4]/td[4]",
            "RESIDENCE HOUSE": "tableAlias[5]/td[4]",
            "BOARD HOUSE": "tableAlias[6]/td[4]",
            "BOARDING STATUS": "tableAlias[7]/td[4]",
            "STUDENT SOURCE": "tableAlias[8]/td[4]",
            "STUDENT UPDATE DATE": "tableAlias[9]/td[4]",
            #reference data by Employee Record Id first
            "JOB TITLE": "tableAlias[1]/td[2]",
            "JOB CODE": "tableAlias[2]/td[2]",
            "JOB TERM": "tableAlias[2]/td[4]",
            "JOB CLASS": "tableAlias[3]/td[2]",
            "APPT END DATE": "tableAlias[3]/td[4]",
            "EMPLOYEE SCHOOL - UNIT": "tableAlias[4]/td[2]",
            "EMPLOYMENT STATUS": "tableAlias[4]/td[4]",
            "EMPLOYEE DEPARTMENT": "tableAlias[5]/td[2]",
            "EMPL RECORD ID": "tableAlias[5]/td[4]",
            "PAID": "tableAlias[6]/td[2]",
            "EMPLOYEE RECORD ID": "table:row://table/tbody/tr[7]/td/table[2]/tbody/tr[2]/td[2]/table/tbody/tr,/td[2],1;[6]/td[2]",
            "EMPLOYEE UPDATE DATE": "tableAlias[6]/td[4]",

            "LIBRARY BORROWER TITLE": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]",
            "LIBRARY BORROWER CODE": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]",
            "LIBRARY BORROWER TYPE": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]",
            "LIBRARY BORROWER AUTHORIZER NAME": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]",
            "LIBRARY BORROWER AUTHORIZER ID": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[4]/td[2]",
            "LIBRARY BORROWER TYPE": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[4]",
            "LIBRARY BORROWER TERM": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[4]",
            "LIBRARY BORROWER SOURCE": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[4]/td[4]", 
            "LIBRARY BORROWER UPDATE DATE": "//tr[7]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[5]/td[4]",

            "POI TITLE": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]",
            "POI SCHOOL - UNIT": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]",
            "POI DEPARTMENT": "tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]",
            "POI COMPANY": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[5]/td[2]",
            "POI DESCRIPTION LINE 1": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[6]/td[2]",
            "POI DESCRIPTION LINE 2": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[7]/td[2]",
            "POI COMMENTS": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[9]/td[2]",
            "POI TYPE":  "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[4]",
            "POI TERM": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[4]", 
            "POI AUTHORIZER NAME": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[4]/td[4]",
            "POI AUTHORIZER ID": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[5]/td[4]",
            "POI AUTHORIZER ADMIN NAME": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[7]/td[4]",
            "POI AUTHORIZER ADMIN ID": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[8]/td[4]",
            "POI SOURCE": "//tr[7]/td/table[4]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[9]/td[4]",
            "POI UPDATE DATE": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[10]/td[4]",

            "ALUMNI TITLE": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]",
            "ALUMNI DEGREE": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]",
            "ALUMNI DEGREE YEAR": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]",
            "ALUMNI SCHOOL": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[4]/td[2]",
            "ALUMNI RECORD ID": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[5]/td[2]",
            "ALUMNI TYPE": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[4]",
            "ALUMNI ROLE START DATE": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[4]",
            "ALUMNI ROLE END DATE": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[4]",
            "ALUMNI PREFERRED YEAR": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[4]/td[4]",
            "ALUMNI AFFILIATE YEAR": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[5]/td[4]",
            "ALUMNI ROLE UPDATE DATE": "//tr[7]/td/table[5]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[6]/td[4]",
            
            "PRIVACY TYPE": {"PERSON": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]",
                             "EMAIL ADDRESS": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[3]",
                             "IMAGE": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[4]",
                             "EMPLOYEE ROLE": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[5]",
                             "STUDENT ROLE": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[6]",
                             "SPECIAL ROLE": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[7]",
                             "OFFICE ADDRESS": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[8]",
                             "OFFICE PHONE": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[9]",
                             "OFFICE FAX": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[10]",
                             "RESIDENCE ADDRESS": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[11]",
                             "DORM ADDRESS": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[12]",
                             "HOME PHONE": "setTableAlias://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[13]"},
            
            
            
            #"PRIVACY TYPE": "table:row://tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr,/td[1]/div,PRIVACY;/td[1]/div",
            "PRIVACY VALUE": "tableAlias/td[2]/div",
            "PRIVACY UPDATE BY": "tableAlias/td[3]/div",
            "PRIVACY UPDATE DATE": "tableAlias/td[4]/div",

            "FERPA BLOCK": "//tr[11]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]",
            "ELECTED RELEASE OF NON_STUDENT DATA": "//tr[11]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[4]",
            "FERPA UPDATE BY": "//tr[11]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]",
            "FERPA UPDATE DATE": "//tr[11]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[4]",

            "CARD DISABLED INDICATOR": "//tr[13]/td/table[1]/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]",
            "CARD GENERAL REISSUE DIGIT": "//tr[13]/td/table[1]/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[4]",
            "CARD UPDATE DATE": "//tr[13]/td/table[1]/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td[6]",
            "CARD CLASS": "//tr[13]/td/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]",
            "CARD TYPE": "//tr[13]/td/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[4]",
            "CARD ISSUE DATE": "//tr[13]/td/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]",
            "CARD VALID THROUGH DATE": "//tr[13]/td/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[4]",
            "CARD DETAILS REISSUE DIGIT": "//tr[13]/td/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]",
            # Add contact objects
            "ACTION": {"LOG OFF": "//input[@value='Log Off']",
                       "IMAGE": "//a[@class='menuTxt' and @href='/psc/displayImageEdit.do']",
                       "IDENTIFIERS": "//a[@class='menuTxt' and @href='/psc/updateIdentifer.do']",
                       "IDENTITY RESOLUTION": "//a[@class='menuTxt' and @href='/psc/target_id_input_form.jsp']",
                       "DIRECTORY DISPLAY ORDER": "//a[@class='menuTxt' and @href='/psc/printOrderHandler.do']",
                       "SELECT OFFICIAL EMAIL": "//a[@class='menuTxt' and @href='/psc/officialEmailSelectHandler.do']",
                       "EMAIL": "//a[@class='menuTxt' and @href='/psc/viewAddEmailAddress.do']",
                       "ONBOARD EMAIL": "//a[@class='menuTxt' and @href='/psc/viewAddOnboardEmailAddress.do']",
                       "2ND RECOVERY EMAIL": "//a[@class='menuTxt' and @href='/psc/addNameMenu.do?']",
                       "LOGIN NAME": "//a[@class='menuTxt' and @href='/psc/addLoginName.do']",
                       "DIRECTORY LISTING": "//a[@class='menuTxt' and @href='/psc/createListingForm.do?']",
                       "NAME": "//a[@class='menuTxt' and @href='/psc/addNameMenu.do?']",
                       "ADDRESS": "//a[@class='menuTxt' and @href='/psc/viewAddAddress.do']",
                       "ROLE": "//a[@class='menuTxt' and @href='/psc/viewAddRoles.do']",
                       "ADD CONTACT DATA": "//table/tbody/tr[12]/td/a",
                       "SEARCH": "//td[1]/table/tbody/tr[2]/td[1]/span/a[1]",
                       "CHANGE ROLE": "//td[1]/table/tbody/tr[2]/td[1]/span/a[2]",
                       "CREATE PERSON": "//td[1]/table/tbody/tr[2]/td[1]/span/a[3]",
                       "REPORTS": "//td[1]/table/tbody/tr[2]/td[1]/span/a[4]",
                       "HELP":"//td[1]/table/tbody/tr[2]/td[1]/span/a[5]",
                       "OK": "//input[@value='OK']",
                       "EDIT": "tableActionAlias/td[1]/a",
                       "DELETE": "tableActionAlias/td[3]/a"}}

            
def SearchResults(record):
    #while CONFIG.driver.current_url.endswith("SearchInProgress.do"):
    #    time.sleep(1)
    
    try:
        if CONFIG.driver.find_element_by_class_name("currentProfielName"):
           CONFIG.logger1.info (indent + "Only one record found - HUID: " + CONFIG.driver.find_element_by_xpath("html/body/div[1]/table/tbody/tr[2]/td/table/tbody/tr/td[1]").text) 
           return
    except:
        pass
    
    return {"HUID": ["link_text:expectedValue"],
            "MATCH SCORE": "",
            "ACTION": {"REFINE SEARCH": "//input[@value='REFINE_SEARCH']",
                       "NEW SEARCH": "//input[@value='NEW_SEARCH']"}}

        
def Tabs(record):
    UTILS.WaitForObject("XPATH", "//div[1]/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[1]/table/tbody/tr[2]/td[1]", record, 10)
    return {"SEARCH": "link_text:Search",
            "CHANGE ROLE": "link_text:Change Role",
            "CREATE PERSON": "link_text:Create Person",
            "REPORTS": "link_text:Reports",
            "HELP": "link_text:Help"}


def UpdateImage(record):
    return {"IDENTITY": "",
            "IMAGE NUMBER": "]",
            "PURGE INDICATOR": "",
            "MOVE IMAGE TO": "",
            "COPY IMAGE TO": "",
            "ACTION": {"SUBMIT": ".//input[@value='Submit']",
                       "CANCEL": ".//input[@value='Cancel']"}}


def Identifiers(record):
    
    return {"PERSON NAME": "//tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td/div/b",
            "HUID": "//tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td/div/b",
            "TARGET HUID": "//input[@name='targetHuid']",
            "NETID": "//tr[4]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td[2]",
            "UUID": "//tr[4]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td[2]",
            "EPPN": "//tr[4]/td[2]/table/tbody/tr/td/table/tbody/tr[3]/td[2]",
            "NEW NETID": "//input[@name='netid.identifier']",
            "NEW UUID": "//input[@name='uuid.identifier']",
            "NEW EPPN": "//input[@name='eppn.identifier']",
            "REPLACE NETID": "//input[@name='replaceNetid']",
            "REPLACE UUID": "//input[@name='replaceUuid']",
            "REPLACE EPPN": "//input[@name='replaceEppn']",
            "ACTION": {"MODIFY": "//input[@value='Modify']",
                       "MOVE": "//input[@value='Move']",
                       "MOVE PRIOR": "//input[@value='Move Prior']",
                       "SAVE": "//input[@value='Save']",
                       "OK": "//input[@value='OK']",
                       "UPDATE NETID": "//input[@name='updateNetIDFlag']",
                       "UPDATE UUID": "//input[@name='updateUUIDFlag']",
                       "UPDATE EPPN": "//input[@name='updateEPPNFlag']",
                       "CANCEL": "//input[@value='Cancel']",
                       "CLOSE": "//input[@value='Close']"}}


def IdentityResolution(record):
    return {"PERSON NAME": [".//table/tbody/tr[3]/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td[3]", ".//tr[2]/td/form/table/tbody/tr[3]/td[3]"],
            "BIRTH DATE": ".//table/tbody/tr[3]/td[2]/table/tbody/tr/td/table/tbody/tr[]/td[3]",
            "LAST FOUR": ".//table/tbody/tr[3]/td[2]/table/tbody/tr/td/table/tbody/tr[3]/td[3]",
            "HUID": [".//table/tbody/tr[3]/td[2]/table/tbody/tr/td/table/tbody/tr[4]/td[3]", ".//tr[2]/td/form/table/tbody/tr[5]/td[3]"],
            "VALID RECORD HUID": ".//tr[2]/td/form/table/tbody/tr[7]/td[3]/input",
            "IDENTIFIER STATUS": ".//table/tbody/tr[3]/td[2]/table/tbody/tr/td/table/tbody/tr[5]/td[3]",
            "ACTION": {"FIND MATCH": "//input[@value='Find Match']",
                       "MARK AS NORMAL": "//input[@value='Mark as Normal']",
                       "MARK AS DUPLICATE": "//input[@value='Mark as Duplicate']",
                       "CANCEL": "//input[@value='Cancel']",
                       "LOGOFF": "//input[@value='Logoff']"}}
    

def SelectOfficialEmail(record):
    return {"SELECT OFFICIAL EMAIL": "table:row:",
            "ACTION": {"SAVE": "//input[@value='Save']",
                       "CANCEL": "//input[@value='Cancel']"}}
     
        
def FindTable(tableAlias, searchPath, appendPath, searchString):
    # set initial table area location
    table = CONFIG.driver.find_elements_by_xpath(tableAlias)
    #parse through each row in the table
    for counter in range(1, len(table)+1, 1):
        #print(tableAlias + "[" + str(counter) + "]" + searchPath)
        try:
            #compare values to confirm a match
            if CONFIG.driver.find_element_by_xpath(tableAlias + "[" + str(counter) + "]" + searchPath).text.upper() == searchString.upper():
                counter += 1
                break
        except: pass
    #set the table alias for validation of other objects within that grouping.
    CONFIG.tableAlias = tableAlias + "[" + str(counter) + "]" + appendPath
    return CONFIG.tableAlias

def FindRow(tableAlias, appendPath, expectedValue, objectType):
    try:
        #print("table: " + str(tableAlias))
        table = CONFIG.driver.find_elements_by_xpath(tableAlias)
        for counter in range(1, len(table)+1, 1):
            if objectType == "EMAIL":
                tempAlias = tableAlias + "[" + str(counter) + "]"
                CONFIG.tableActionAlias = tableAlias + "[" + str(counter-1) + "]" + "/td/table/tbody/tr/td[2]/table/tbody/tr"
                try:
                    if CONFIG.driver.find_element_by_xpath(tempAlias + appendPath).text.upper() == expectedValue.upper():
                        CONFIG.tableAlias = tempAlias
                        return CONFIG.tableAlias
                except: pass
            elif objectType == "PRIVACY":
                tempAlias = tableAlias + "[" + str(counter + 1) + "]"
                try:
                    if CONFIG.driver.find_element_by_xpath(tempAlias + appendPath):
                        if CONFIG.driver.find_element_by_xpath(tempAlias + appendPath).text.upper() == expectedValue.upper() + ":":
                            CONFIG.tableAlias = tempAlias
                            #print(indent + CONFIG.tableAlias)
                            return CONFIG.tableAlias  
                except: pass
            else:
                tempAlias = tableAlias + "[" + str(counter) + "]" + "/td/table/tbody/tr"
                #print("tempAlias: " + tempAlias)
                CONFIG.tableActionAlias = tableAlias + "[" + str(counter-1) + "]" + "/td[2]/table/tbody/tr"
                table2 = CONFIG.driver.find_elements_by_xpath(tempAlias)
                for counter2 in range(1, len(table2)+1, 1):
                    tempAlias2 = tempAlias + "[" + str(counter2) + "]"
                    #print("tempAlias2: " + tempAlias2)
                    #print(tempAlias2 + appendPath)
                    try:
                        #print(CONFIG.driver.find_element_by_xpath(tempAlias2 + appendPath + "/img[@src='image/harvard_shield_ico.gif']").text.upper())
                        if CONFIG.driver.find_element_by_xpath(tempAlias2 + appendPath).text.upper() == expectedValue.upper():
                            #print(str(CONFIG.driver.find_element_by_xpath(tempAlias2 + appendPath).text.upper()))
                            if objectType == "0": 
                                CONFIG.tableAlias = tempAlias2
                            else: 
                                CONFIG.tableAlias = tempAlias
                            return CONFIG.tableAlias
                    except: pass   
    except: pass
    

            
def ProcessEmail(record):
    print("checking email")
    time.sleep(20)
    # Set up the connection to the POP server 
    pop_conn = poplib.POP3_SSL('pop.gmail.com')
    pop_conn.user(CONFIG.mail_username)
    pop_conn.pass_(CONFIG.mail_password)
    
    poplist = pop_conn.list()
    #print(poplist)
    
    # Iterate through the messages 
    for i in range(len(pop_conn.list()[1])): 
        message = ""
        for j in pop_conn.retr(i+1)[1]: 
            message += str(j).replace("b'", "").replace("'", ""). replace("  ", "")
            #print(message)
        if record["fields"][0].upper() == "LOGIN NAME":
            index = message.find("service desk recently updated the login name for your HarvardKey account")
            if index > -1:
                CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
            pop_conn.dele(i+1)
        elif record["fields"][0].upper() == "REGISTRATION COMPLETE":
            searchString = "Your HarvardKey has been activated and is now ready to use."
            index = message.find(searchString)
            if index > -1:
                CONFIG.logger1.info(indent + record["fields"][0] + " email message received")
                searchValues = ["Login Name", "Primary Recovery Email", "Secondary Recovery Email"]
                message = message.replace(": <", ":<")
                for value in searchValues:
                    message = message[message.find("<b>" + value + ":</b><span>") + len("<b>" + value + ":</b><span>"):]
                    CONFIG.logger1.info(indent + value.upper() + ":   " + message[:message.find("</span></p>")])
                pop_conn.dele(i+1)
            
        elif record["fields"][0].upper() == "PASSWORD CHANGE":
            index = message.find("you recently changed the password for your HarvardKey account.")
            if index > -1:
                CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
            pop_conn.dele(i+1)
        elif record["fields"][0].upper() == "PASSWORD RESET":
            index = message.find("you recently reset the password for your HarvardKey account.")
            if index > -1:
                CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
            pop_conn.dele(i+1)
        else:
            if record["fields"][0].upper() == "REGISTRATION" or record["fields"][0].upper() == "PASSWORD":
                searchString = "copy and paste into your web browser):"
                index = message.find(searchString)
                #print(index)
            if index > -1:
                CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
                #print(message)
                message = message[index + len(searchString):]
                url = message[message.find('href="') + len('href="'):message.find('"><span>https:')]
                passcode = message[message.find('<span id="passcode">') + len('<span id="passcode">'):message.find('</span></p>')]
                pop_conn.dele(i+1)
            else: continue
            CONFIG.driver.get(url)
            enum = CONFIG.driver.find_element_by_name("passcodeToken")
            result = UTILS.PerformAction(enum, "Confirmation Code", passcode, record)
            CONFIG.driver.find_element_by_xpath("html/body/div[2]/div/form/div/input").click()

    pop_conn.quit()        
                
                
   