#---------- IMPORTS ----------
import time

#-------CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
tableActionAlias = ""

#---------- FUNCTIONS ----------
def MappingTable(record):
    #converts the screen value from the spreadsheet into a function that controls how interactions take place.
    available_mappings = {
        "LOGIN": Login,
        "TWO-STEP VERIFICATION": TwoStepVerification,
        "MANAGE TWO-STEP VERIFICATION": ManageTwoStepVerification
        } 
    MappedObjects = available_mappings.get(record["screen"], lambda x: None)(record)

    UTILS.UseScreenObjects(MappedObjects, record)
    
    try:
        enum = CONFIG.driver.find_element_by_xpath(".//*[@id='messages-view']/div/div[2]/div/span")
        UTILS.ReportErrors(enum.text, record)
        CONFIG.driver.switch_to.Window(CONFIG.driver.WindowHandles[0])
    except:
        pass



def Login(record):
    UTILS.WaitForObject("ID", "duo_iframe", record, 10)

    try:
        CONFIG.driver.switch_to.frame(CONFIG.driver.find_element_by_id("duo_iframe"))
    except:
        pass
    #CONFIG.driver.switch_to.frame(CONFIG.driver.find_element_by_id("login-form"))
    UTILS.WaitForObject("XPATH", ".//*[@id='login-form']/fieldset[2]", record, 10)
    
    return {"PASSCODE": [".//*[@id='login-form']/fieldset[2]/div[3]/input[2]", ".//*[@id='login-form']/fieldset[2]/div[2]/input[2]"],
            "ACTION": {"ENTER A PASSCODE": [".//*[@id='login-form']/fieldset[2]/div[3]/button", ".//*[@id='login-form']/fieldset[2]/div[2]/button"],
                       "LOGIN": [".//*[@id='login-form']/fieldset[2]/div[3]/button", ".//*[@id='login-form']/fieldset[2]/div[2]/button"],
                       "LOG IN": [".//*[@id='login-form']/fieldset[2]/div[3]/button", ".//*[@id='login-form']/fieldset[2]/div[2]/button"]}}

def TwoStepVerification(record):
    CONFIG.driver.switch_to_frame(CONFIG.driver.find_element_by_id("duo_iframe"));
    UTILS.WaitForObject("XPATH", "html/body/div[1]/div[2]/div/div[2]/div/nav/a[1]/span", record, 10)

    return {"REMEMBER ME FOR 30 DAYS": ".//*[@id='login-form']/fieldset[2]/div[3]/input[2]",
            #"PASSCODE": "//input[contains(@class,'passcode-input')]",
            "PASSCODE": ".//*[@id='login-form']/fieldset[2]/div[2]/input[2]",
            "ACTION": {"START SETUP": "html/body/div[1]/div[1]/div/div/a",
                       "SEND ME A PUSH": ".//*[@id='login-form']/fieldset[2]/div[3]/button",
                       "CALL ME": ".//*[@id='login-form']/fieldset[2]/div[3]/button",
                       "ENTER A PASSCODE": ".//*[@id='login-form']/fieldset[2]/div[2]/button",
                       "ENTER A BYPASS CODE": ".//button[contains(text(),'Enter a Bypass Code')]",
                       #"ENTER A BYPASS CODE": ".//*[@id='login-form']/fieldset[2]/div[2]/button",
                       #"ENTER A BYPASS CODE": "html/body/div[@class='base-wrapper']/div[@class='base-main']/div/form/fieldset/div[2]/button",
                       "LOG IN": ".//button[contains(text(),'Log In')]",
                       "WHAT IS THIS?": "html/body/div[1]/div[2]/div/div[2]/div/nav/a[1]/span",
                       "NEED HELP": "html/body/div[1]/div[2]/div/div[2]/div/nav/a[2]",
                       "": "",
                       "": "",
                       "": "",
                       "": "",}}
                       
 
def ManageTwoStepVerification(record):
    
    CONFIG.driver.switch_to_frame(CONFIG.driver.find_element_by_id("duo_iframe"));
    
    return {"ENROLLMENT STATUS": ".//*[@id='mfaEnrollmentStatus']/span",
            "ACTIVATION STATUS": ".//*[@id='mfaActivationStatus']/span",
            "NAME": "",
            "TYPE/PLATFORM": "",
            "PHONE #": "",
            "ACTIVE": "",
            "ACTION": {"SET UP TWO-STEP VERIFICATION": "html/body/div[2]/div/p/a",
                       "ACTIVATE TWO-STEP VERIFICATION": "html/body/div[2]/div/form",
                       "CANCEL": "html/body/div[2]/div/div[2]/a"}}