#---------- IMPORTS ----------
from subprocess import Popen, PIPE
from collections import OrderedDict
import os
import re
import base64
import pymysql.cursors

#-------CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
server, DbPassword, userName, SQLQuery = ("",) * 4

os.environ['ORACLE_HOME'] = "/opt/oracle/instantclient"
os.environ['DYLD_LIBRARY_PATH'] = "/opt/oracle/instantclient"


def DatabaseOperations(record):
    useHeaders = False
    headerValues = []
    queryType = ""
    
    #spreadsheet parsing
    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1]       
        if fieldName == "DB NAME":
            if expectedValue.upper() == "IAMDEV":
                CONFIG.DBServer = 'iamdev-rds.dev.iam.harvard.edu:1521/iamdev'
            elif expectedValue.upper() == "IAMQA":
                CONFIG.DBServer = 'new-iamdb.qa.iam.harvard.edu:1521/iamqa'
            elif expectedValue.upper() == "IDDBDEV":
                CONFIG.DBServer = 'iddbdev-rds.dev.iam.harvard.edu:1521/iddbdev'    
        elif fieldName == "HEADINGS" and expectedValue.upper() == "ON":
            useHeaders = True    
        elif fieldName == "USER NAME": 
            userName = expectedValue
            DbPassword = base64.b64decode(bytes(CONFIG.DBPermissions[userName.upper()], "utf-8")).decode('ascii') 
        elif fieldName == "PASSWORD": DbPassword = base64.b64decode(bytes(expectedValue, "utf-8")).decode('ascii')
        elif fieldName == "QUERY": 
            SQLQuery = (expectedValue.rstrip('\n') + ";").replace(";;", ";").strip()
            queryType = SQLQuery[:SQLQuery.find(" ")].upper().strip()
        elif fieldName == "FILE":
            SQLQuery = "@" + expectedValue
    #verify connection string can be created        
    if CONFIG.DBServer == "" or userName == "" or DbPassword == "" or SQLQuery == "":
        return
    else:
        
        if record["screen"].upper() == "ORACLE" or record["screen"].upper() == "SQLPLUS":
            connectString = userName + "/" + DbPassword + "@" + CONFIG.DBServer
            
            #get header values    
            if useHeaders == True and queryType == "SELECT":
                if SQLQuery.upper().find("SELECT *") > -1:
                    outputProcessing = "SET HEADING OFF TRIM ON TRIMSPOOL ON PAGESIZE 2000 LINESIZE 130 UND OFF FEED OFF\n "
                    #get table name
                    if SQLQuery.upper().find("WHERE"):
                        tableName = SQLQuery[SQLQuery.upper().find("FROM")+5:SQLQuery.upper().find("WHERE")].upper().strip()
                        if tableName.find(".") > -1:
                            tableName = tableName[tableName.find(".")+1:]
                    else:
                        tableName = SQLQuery[SQLQuery.upper().find("FROM")+5:]
                    headerQuery = "select column_name from user_tab_columns where table_name='%s';" %(tableName)
                    #get table columns from user_tab_columns
                    headerValues = submitOracleQuery(queryType, connectString, outputProcessing, headerQuery, record, ":")
                else:
                    #get columns from SELECT query
                    headerValues = SQLQuery[SQLQuery.upper().find("SELECT")+7:SQLQuery.upper().find("FROM")-1].replace(" ", "").split(",")      

                longest_string = max(headerValues, key = len)
            
            if queryType == "SELECT":
                outputProcessing = "SET HEADING OFF TRIM ON TRIMSPOOL ON PAGESIZE 2000 LINESIZE 130 COLSEP : RECSEPCHAR | RECSEP EACH NULL NULL UND OFF FEED OFF\n "    
            else:
                outputProcessing = "SET HEADING OFF TRIM ON TRIMSPOOL ON PAGESIZE 2000 LINESIZE 130\n "
            
            if CONFIG.verboseReporting == True:
                CONFIG.logger1.info(indent + "Connection String: SQLPlus -S " + connectString)
                CONFIG.logger1.info(indent + "Submitting query: " + SQLQuery)
            #if CONFIG.verboseReporting == True:
           
            queryResult = submitOracleQuery(queryType, connectString, outputProcessing, SQLQuery, record, "|")
        
        elif record["screen"].upper() == "MYSQL":
            connectString = userName + "/" + DbPassword + "@" + CONFIG.MySQLServer
            #print(connectString)
            if CONFIG.verboseReporting == True:
                CONFIG.logger1.info(indent + "Connection String : MySQL: " + connectString)
                CONFIG.logger1.info(indent + "Submitting query: " + SQLQuery + "\n")
            queryResult = submitMySQLQuery(CONFIG.MySQLServer, userName, DbPassword, "intdb", SQLQuery, record)
    
        if CONFIG.verboseReporting == True:
            CONFIG.logger1.info(queryResult)
        return queryResult
        
  
def submitOracleQuery(queryType, connectString, outputProcessing, SQLQuery, record, splitMarker):
    session = Popen(['/opt/oracle/instantclient/sqlplus', '-S', connectString], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    #print(SQLQuery)
    session.stdin.write(bytes(outputProcessing + SQLQuery, 'UTF-8'))
    queryResult, errorMessage = session.communicate()
    if errorMessage.decode("utf-8").strip() != "":
        UTILS.ReportErrors("Database Query failed: " + str(errorMessage.decode("utf-8").strip()), record)
        return errorMessage
    elif queryResult != "":
        #print(queryResult)
        queryResult = queryResult.decode("utf-8").strip()  # .replace("-","").replace("   ", "").replace("  ", "").replace("\t", "\n").replace("\n\n", "").replace("\n", " : ")
        if queryType == "SELECT":
            queryResult = re.sub(r"[|+]+", "|", queryResult).strip()
            queryResult = re.sub(r"(\-\-)+", "", queryResult)
            queryResult = re.sub(r"[\n\t]+", ":", queryResult)
            queryResult = re.sub(r"(\s\s+)+", "", queryResult)
            queryResult = re.sub(r"(\s\t+)+", "", queryResult)
            queryResult = re.sub(r"(::)+", ":", queryResult)
            
            if len(queryResult) > 0 and queryResult[-1] == "|":
                queryResult = queryResult[0:-1]
            queryResult = queryResult.strip()
            if splitMarker == None:
                queryResult = queryResult.splitlines()
            else:
                queryResult = queryResult.split(splitMarker)
    return queryResult

            
def submitMySQLQuery(server, userName, password, db, SQLQuery, record):
    # Connect to the database
    
    connection = pymysql.connect(host=server,
                             user=userName,
                             password=password,
                             db=db,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = SQLQuery
            #sql = "select gm.*, g.*, m.* from intdb.group_memberships gm, intdb.groups g, intdb.members m where gm.group_name = g.group_name and m.uuid = gm.subject_id limit 5"
            cursor.execute(sql)
            queryResult = cursor.fetchall()
        connection.commit()
    finally:
        connection.close()   
    return queryResult
            
            
            
            
            