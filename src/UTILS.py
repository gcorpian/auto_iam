#---------- IMPORTS ----------
import os
import sys
import time
import smtplib
import base64
import subprocess
#import wmi
#import pyscreenshot as ImageGrab# pip install
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders
from getpass import getuser
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.firefox.webdriver import FirefoxProfile


#-------CUSTOM IMPORTS -------
import CONFIG
import MIDAS
import GROUPERUI
import XID

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent

def OpenBrowser(browser):
    if browser == "CHROME":
        # https://sites.google.com/a/chromium.org/chromedriver/downloads
        #System.setProperty("webdriver.chrome.driver", "/opt/chrome/chromedriver");
        if CONFIG.performanceMode != True:
            KillProcess(["Chrome"])
        if sys.platform == "darwin":
            try:
                CONFIG.driver = webdriver.Chrome("/opt/chrome/chromedriver")
            except selenium.common.exceptions.WebDriverException:
                print("Missing Chrome Driver")
        elif sys.platform == "windows" or sys.platform == "win32":
            CONFIG.driver = webdriver.Chrome("C:\\opt\\chrome\\chromedriver")
        time.sleep(5)    
    elif browser == "IE":
        CONFIG.driver = webdriver.Ie()
    elif browser == "FIREFOX":
        if CONFIG.performanceMode != True:
            KillProcess(["firefox"])
        
        firefox_capabilities = DesiredCapabilities.FIREFOX
        firefox_capabilities['marionette'] = True
        #firefox_capabilities['firefox_profile'] = CONFIG.browserProfile
        #firefox_capabilities['profile'] = CONFIG.browserProfile
   
        if os.path.isdir(CONFIG.browserProfile):
            profile = CONFIG.FirefoxProfile(CONFIG.browserProfile)
            #executablePath = CONFIG.FirefoxProfile(CONFIG.executablePath)
            #CONFIG.driver = webdriver.Firefox(capabilities=firefox_capabilities)
            os.environ["PATH"] += ":/opt/gecko"
            CONFIG.driver = webdriver.Firefox(profile)
        else:
            CONFIG.driver = webdriver.Firefox()
    elif browser == "PHANTOMJS":
        phantomjs_args = [
            "--ssl-protocol=any",
            "--load-images=false",
            "--proxy-type=None",
            "--ignore-ssl-errors=true",
        ]
        CONFIG.driver = webdriver.PhantomJS(service_args=phantomjs_args)
    elif browser == "SAFARI":
        CONFIG.driver = webdriver.Safari()
    
    CONFIG.driver.maximize_window()

    return CONFIG.driver  

def KillProcess(applist):
    for application in applist:
        if application == "Chrome":
            os.system('killall -9 "Google Chrome"')
        try:
            if sys.platform == "darwin":
                ps = subprocess.Popen("ps -eaf | grep "+ application, shell=True, stdout=subprocess.PIPE)
                if ps.stdout.read() != "":
                    #print(application.casefold())
                    os.system('pkill -f ' + application.casefold())
            elif sys.platform == "windows":
                for process in wmi.WMI.Win32_Process (name=application):    #["firefox.exe"]
                    process.Terminate ()
        except ProcessLookupError: # errno.ESRCH 
            pass

def goToURL(url):
    if CONFIG.driver.current_url != url:
        CONFIG.driver.get(url)
        return

def stringToDictionary(s, pairSeparator, keyValueSeparator):
    #converts a string value to a dictionary
    items = s.split(pairSeparator)
    data = {}
    for item in items:
        keyvalpair = item.split(keyValueSeparator)
        data[keyvalpair[0].strip()] = str(keyvalpair[1]) # strip() removes unwanted trailing/leading spaces
    return data

def WaitForObject(searchType, searchValue, record, delay):
    if delay == "":
        delay = 10
    try:
        if searchType == "ID":
            WebDriverWait(CONFIG.driver, delay).until(EC.presence_of_element_located((By.ID, searchValue)))
        elif searchType == "XPATH":
            WebDriverWait(CONFIG.driver, delay).until(EC.presence_of_element_located((By.XPATH, searchValue)))
        elif searchType == "CLASS":
            WebDriverWait(CONFIG.driver, delay).until(EC.presence_of_element_located((By.Class, searchValue)))
        elif searchType == "NAME":
            WebDriverWait(CONFIG.driver, delay).until(EC.presence_of_element_located((By.NAME, searchValue)))
    except:
        #ReportErrors("Can't find " + searchValue + " by " + searchType, record)
        return -1
    return 0

    
def CheckValueCount(record):
    if len(record["fields"]) != len(record["values"]):
        ReportErrors("Record count mismatch between Fields and Expected Values.", record)
        if record["results"] == "T":
            sys.exit
        else:
            return

def ConvertURL(value):
    urlCodes = {"%2F":"/", "%252F":"/", "%3A":":", "%253A":":", "%26":"&", "%3D":"=", "%3F":"?"}
    for key in urlCodes:
        value = value.replace(key,urlCodes[key])
    return value

def PerformAction(enum, field, value, record): 
    try:
        objectType = enum.get_attribute("type")
    except: 
        print("no enum object")
        if enum.get_attribute("type") != None: return
    #print(objectType)
    if record["action"] == "INPUT":
        if field == "ACTION" or objectType == "" or objectType.upper() == "RADIO" or objectType.upper() == "CHECKBOX":
            clickStart = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
            enum.click()
            clickStop = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
            
            if CONFIG.verboseReporting == True:
                CONFIG.logger1.info (indent + "%s - '%s' clicked - Processing time is %s" % (field, value, str(clickStop - clickStart)))
            #time.sleep(1)
        elif objectType.upper() == "TEXT" or objectType.upper() == "TEXTAREA" or objectType.upper() == "PASSWORD":
            enum.clear()
            if (value == "" or value == " ") and CONFIG.verboseReporting == True:
                CONFIG.logger1.info (indent + "%s field: value deleted" % (field))
            else:
                enum.send_keys(value)
                if field.upper() == "PASSWORD":
                    #if CONFIG.verboseReporting == True:
                        #print(value)
                        #CONFIG.logger1.info (indent + "%s field: set value to '%s'" % (field, value))
                    value = "*** sensitive value removed ***"
                CONFIG.logger1.info (indent + "%s field: set value to '%s'" % (field, value))
        elif objectType.upper() == "SELECT-ONE":
            match = False
            for option in enum.find_elements_by_tag_name('option'):
                if option.text.upper().strip() == value.upper().strip():
                        option.click()
                        match = True
                        if CONFIG.verboseReporting == True:
                            CONFIG.logger1.info (indent + "%s field: select value changed to '%s'" % (field, value))
                        break
            if match == False:
                for option in enum.find_elements_by_tag_name('option'):
                    if value.upper().strip() in option.text.upper().strip():
                        option.click()
                        match = True
                        if CONFIG.verboseReporting == True:
                            CONFIG.logger1.info (indent + "%s field: select value changed to closest match.  Expected '%s' and changed to '%s'" % (field, value, option.text))
                        break
            
            if match == False:
                if record["results"] == "F": CONFIG.logger1.info (indent + "Select value '" + fieldName + "' was not found as expected.")
                else: ReportErrors("Could not find select value: " + value, record)
        elif objectType.upper() == "SUBMIT":
            print("BUTTON")
            if CONFIG.verboseReporting == True:
                CONFIG.logger1.info (indent + "%s - %s clicked'" % (field, value))
            enum.click()
        elif objectType == None:
            ReportErrors("%s object not modifiable" %(field), record)
        
        return
    elif record["action"] == "VERIFY":
        if objectType == "select-one": 
            if enum.get_attribute("selected").upper().strip() == value.upper().strip():
                CONFIG.logger1.info(indent + "'%s' value (%s) matched expected results in the response." % (field, value)) 
            else: ReportErrors("'%s' value (%s) did not match Expected Value: '%s'" % (field, enum.text, value), record)
        elif objectType == None:
            try:
                if enum.get_attribute("src").find("gif") > 1:
                    if value.upper().strip()[:1] == "Y":
                        #print(enum.get_attribute("src")[enum.get_attribute("src").rfind("/")+1:])
                        CONFIG.logger1.info(indent + "'%s' image found (%s) matched expected results." % (field, enum.get_attribute("src")[enum.get_attribute("src").rfind("/")+1:])) 
                    else:
                        ReportErrors("'%s' image found (%s) but was not expected." % (field, enum.get_attribute("src")[enum.get_attribute("src").rfind("/")+1:]), record)
                    return True
                
            except:
                pass
            if enum.text.upper().strip() == value.upper().strip() or enum.text.upper().replace(":", "").strip() == value.upper().strip() or enum.text.upper().find(value.upper()):
                CONFIG.logger1.info(indent + "'%s' value (%s) matched expected results in the response." % (field, value)) 
            else: ReportErrors("'%s' value (%s) did not match Expected Value: '%s'" % (field, enum.text, value), record)
    return True

    
def SendMail(testCases):
    try:
        textfile = "../EmailList/emails.txt"
        To = ""

        with open(textfile) as f:
            sendTo = f.read().splitlines()
        sendTo = [x for x in sendTo if x != ""]

        msg = MIMEMultipart()
        msg.attach(MIMEText("Test Results from test: %s in %s\nNumber of Tests Run: %d of %d total available.\n%s\n%s" %(CONFIG.worksheet, CONFIG.filename[CONFIG.filename.rfind("/")+1:], testCases, CONFIG.testCount, CONFIG.testSummary, CONFIG.testDuration)))
        f.close()

        for email in sendTo:
            To = To + email + ";"
        if To[-1:] == ";":
            To = To[0:-1]

        msg['Subject'] = "Results from test: " + "Worksheet: " + CONFIG.worksheet + " on " + CONFIG.env + " from " + getuser()
        msg['From'] = sendTo[0]
        msg['To'] = To

        attach = MIMEBase('application', "octet-stream")
        if CONFIG.add_attachment == "Y":
            attach.set_payload(open("../Logs/" + CONFIG.logfile, "r").read())
            if os.path.isfile("../Logs/" + CONFIG.imagefile): 
                attach.set_payload(open("../Logs/" + CONFIG.imagefile, "r").read())
        encoders.encode_base64(attach)

        attach.add_header('Content-Disposition', 'attachment; filename=' + os.path.basename(CONFIG.logfile))

        msg.attach(attach)

        s = smtplib.SMTP(CONFIG.sendmail_host)
        s.sendmail(sendTo[0], sendTo, msg.as_string())
        s.quit()
    except smtplib.SMTPSenderRefused: 
        print("Log file was too large to be sent by email.  Message size exceeds fixed limit.")
    

def SplitDate(listEnum, fieldName, listValue, record):
    #print("ListEnum: " + str(listEnum))
    #print(listValue)
    #print("1:" + listEnum[0])
    #print("2:" + listEnum[1])
    #print("3:" + listEnum[2])
    #This function splits a date value and populates 3 fields 
    PerformAction(CONFIG.driver.find_element(By.XPATH, listEnum[0]), fieldName + " MONTH", listValue[0], record)
    PerformAction(CONFIG.driver.find_element(By.XPATH, listEnum[1]), fieldName + " DAY", listValue[1], record)
    PerformAction(CONFIG.driver.find_element(By.XPATH, listEnum[2]), fieldName + " YEAR", listValue[2], record)
    

def UseScreenObjects(MappedObjects, record):    
    #this function determines how interaction with the objects in the mapping table will occur 
    if CONFIG.verboseReporting == True:
        CONFIG.logger1.info (indent + "Current url is " + ConvertURL(CONFIG.driver.current_url))
    CheckValueCount(record)
    
    for c1 in range(0, len(record["fields"])):
        enum, value = ("",)*2
        fieldName = record["fields"][c1].strip().upper()
        expectedValue = record["values"][c1]       
        #---- changed for IIQ login in Production
        if record["application"] == "IIQ" and record["screen"] == "LOGIN" and CONFIG.env == "P-0":
            if fieldName == "USERNAME" and expectedValue == "spadmin":
                expectedValue == CONFIG.iiqUser[0]
            if fieldName == "PASSWORD" and base64.b64decode(bytes(expectedValue, "utf-8")).decode('ascii') == "spadmin": 
                expectedValue == CONFIG.iiqUser[1]
        if fieldName.upper() == "PASSWORD": 
            expectedValue = base64.b64decode(bytes(expectedValue, "utf-8")).decode('ascii')
            print(expectedValue)
        if expectedValue.upper() == "TODAY": expectedValue = str(datetime.date.today())
            
        if fieldName in MappedObjects.keys():
            if fieldName == "SELECT":
                enum = MappedObjects[fieldName]
                try: setTable = MappedObjects["NEWTAB"]
                except: setTable = ""
                SelectTableObject(enum, expectedValue, setTable, record)
                continue
            #Handling mapped objects that are dictionaries
            elif type(MappedObjects[fieldName]) is dict:
                if expectedValue.upper() in MappedObjects[fieldName].keys():
                    if type(MappedObjects[fieldName][expectedValue.upper()]) is str:
                        value = MappedObjects[fieldName][expectedValue.upper()]
                        if value.find("setTableAlias:") >= 0:
                            CONFIG.tableAlias = value.replace("setTableAlias:", "")
                            continue
                        #if value.find("tableActionAlias") > -1:
                        #    value = value.replace("tableActionAlias", CONFIG.tableActionAlias)
                        #print(expectedValue + ": " + MappedObjects[fieldName][expectedValue.upper()])
                        if value.find("link_text:") > -1: 
                            enum = FindObject("link_text", value, "")
                        else: 
                            if c1 == 0:
                                try:
                                    #("1")
                                    WaitForObject("XPATH", MappedObjects[fieldName], record, 60)
                                except: pass
                            enum = FindObject("xpath", value, "")
                    
                    if type(MappedObjects[fieldName][expectedValue.upper()]) is list:
                        for value in MappedObjects[fieldName][expectedValue.upper()]:                           
                            value = value.replace("expectedValue", expectedValue)
                            try:
                                if value.find("link_text:") > -1:
                                    enum = FindObject("link_text", value, expectedValue)
                                elif CONFIG.driver.find_element_by_xpath(value).is_displayed and CONFIG.driver.find_element_by_xpath(value).is_enabled:
                                    if c1 == 0:
                                        try:
                                            #("2")
                                            WaitForObject("XPATH", MappedObjects[fieldName], record, 60)
                                        except: pass
                                    enum = FindObject("xpath", value, "")
                            except: pass               
                    elif fieldName == "TABS":
                        if MappedObjects[fieldName][expectedValue.upper()].find(";"):
                            tabValues = MappedObjects[fieldName][expectedValue.upper()].split(";")
                            if len(tabValues) > 1:
                                CONFIG.activeTab = tabValues[1]

                            enum = CONFIG.driver.find_element_by_xpath(MappedObjects[fieldName][expectedValue.upper()]).click()
                            if CONFIG.verboseReporting == True:
                                CONFIG.logger1.info (indent + expectedValue.upper() + " tab selected")
                        else:
                            CONFIG.activeTab = "IDENTITY SEARCH"
                            enum = CONFIG.driver.find_element_by_xpath(MappedObjects[fieldName][expectedValue.upper()]).click()
                        #time.sleep(2)
                    elif not(enum):
                        enum = FindObject("xpath", MappedObjects[fieldName][expectedValue.upper()], "")
            elif type(MappedObjects[fieldName]) is list:
                for value in MappedObjects[fieldName]:
                    value = value.replace("expectedValue", expectedValue)
                    try:
                        if value.find("link_text:") > -1:
                            #print("link_check")
                            enum = FindObject("link_text", value, expectedValue)
                        elif CONFIG.driver.find_element_by_xpath(value).is_displayed and CONFIG.driver.find_element_by_xpath(value).is_enabled:    
                            if c1 == 0:
                                try:
                                    #print("3")
                                    WaitForObject("XPATH", MappedObjects[fieldName], record, 60)
                                except: pass
                            enum = FindObject("xpath", value, "")
                        else: time.sleep(1)
                    except: pass
            else:
                try:
                    if MappedObjects[fieldName].find("dynamic:") > -1:
                        enum = FindObject("xpath", MappedObjects[fieldName], expectedValue)
                        continue
                    elif MappedObjects[fieldName].find("link_text:") > -1:
                        enum = FindObject("link_text", MappedObjects[fieldName], "")
                    elif MappedObjects[fieldName].find("name:") > -1:
                        enum = FindObject("name", MappedObjects[fieldName], "")
                    elif MappedObjects[fieldName].find("id:") >= 0:
                        enum = FindObject("id", MappedObjects[fieldName], "")
                    elif MappedObjects[fieldName].find("splitDate:") >= 0:
                        #print("found split date")
                        SplitDate(MappedObjects[fieldName].replace("splitDate:", "").split("+,"), fieldName, expectedValue.replace("-", "/").split("/"), record)
                        continue
                    elif MappedObjects[fieldName].find("table:") >= 0:
                        CONFIG.tableAlias = ""
                        MappedObjects[fieldName] = MappedObjects[fieldName].replace("table:", "")
                        listValues = MappedObjects[fieldName].split(";")
                        #sets CONFIG.tableAlias 
                        result = SetTableArea(listValues[0], fieldName, expectedValue, record)
                        if CONFIG.tableAlias == "":
                            ReportErrors("Unable to locate table with expected value: " + fieldName + ": " + expectedValue, record)
                            return
                        if listValues[1] != "":
                            enum = CONFIG.driver.find_element_by_xpath(CONFIG.tableAlias + listValues[1])
                        else: continue
                    else:
                        if MappedObjects[fieldName].find("tableAlias") > -1:
                            MappedObjects[fieldName] = MappedObjects[fieldName].replace("tableAlias", CONFIG.tableAlias)
                        enum = CONFIG.driver.find_element_by_xpath(MappedObjects[fieldName])                    
                except: pass
            
            if enum == "NOT FOUND":
                if record["results"] == "F":
                    if fieldName == "ACTION": CONFIG.logger1.info (indent + "Screen object '" + expectedValue + "' was not found as expected.")
                    else: CONFIG.logger1.info (indent + "Screen object '" + fieldName + "' was not found as expected.")
                else:
                    if fieldName == "ACTION": ReportErrors("Unable to locate object '" + expectedValue + "'.  Requested Action cannot take place.", record)
                    else: ReportErrors("Unable to locate object '" + fieldName + "'", record)
                return
            
            try:
                if enum.is_displayed and enum.is_enabled:
                    result = PerformAction(enum, fieldName, expectedValue, record)
                else: ReportErrors("Unable to locate object with expected value: " + fieldName + ": " + expectedValue, record)
            except: pass
        else: ReportErrors("Unrecognized request: '" + fieldName + "'", record) 
        
        #if expectedValue == "FIND":
            
        #    time.sleep(8)
    return


def FindObject(process, value, replacement):  
    try:
        if process.upper() == "LINK_TEXT":
            enum = CONFIG.driver.find_element_by_link_text(value.replace("link_text:", "").replace("expectedValue", replacement))
        elif process.upper() == "NAME":
            enum = CONFIG.driver.find_element_by_name(value.replace("name:", ""))    
        elif process.upper() == "XPATH":
            enum = CONFIG.driver.find_element_by_xpath(value.replace("dynamic:", "").replace("expectedValue", replacement).replace("tableActionAlias", CONFIG.tableActionAlias))
        elif process.upper() == "ID":
            enum = CONFIG.driver.find_element_by_id(value.replace("id:", ""))        
        return enum
    except:
        return "NOT FOUND"
    
    

def SelectTableObject(object, expectedValue, setTable, record):
    #time.sleep(2)
    table = CONFIG.driver.find_elements_by_xpath(object)
    for enum in table:
        if enum.text == expectedValue:

            enum.click()
            #time.sleep(2)
            if CONFIG.verboseReporting == True:
                CONFIG.logger1.info (indent + "Table Value '%s' selected" % (expectedValue))
            CONFIG.activeTab = setTable
            return
    if enum.text != expectedValue:
        ReportErrors("Could not find requested value:" + expectedValue, record)
        
def SetTableArea(enumString, fieldName, expectedValue, record):
    action = ""
    if enumString.find("row:") >= 0:
        enumString = enumString.replace("row:", "")
        action = "rowOnly"
    listItem = enumString.split(",")
    if action == "":
        if len(listItem) == 4:
            if record["application"] == "MIDAS":
                result = MIDAS.FindTable(listItem[0], listItem[1], listItem[2], listItem[3])
            elif record["application"] == "XID":
                result = XID.FindTable(listItem[0], listItem[1], listItem[2], listItem[3])
        elif len(listItem) == 6:
            result = MIDAS.FindRow(MIDAS.FindTable(listItem[0], listItem[1], listItem[2], listItem[3]),listItem[4], expectedValue, listItem[5])    
    elif action == "rowOnly":
        if record["application"] == "MIDAS":
            result = MIDAS.FindRow(listItem[0], listItem[1], expectedValue, listItem[2]) 
        elif record["application"] == "XID":
            result = XID.FindRow(listItem[0], listItem[1], expectedValue, listItem[2]) 
        elif record["application"] == "GROUPERUI":
            result = GROUPERUI.FindRow(listItem[0], listItem[1], expectedValue, listItem[2]) 
    return result


def StatusCheck(record):
    time.sleep(2)
    #check for error messages
    try:
        enum = CONFIG.driver.find_element_by_xpath(".//*[@class='alert alert-danger']")
        if enum.is_displayed and enum.is_enabled: 
            errorMsg = enum.text
            #CONFIG.logger1.info(errorMsg)
            if enum.text != "":
                if record["results"] == "F": 
                    CONFIG.logger1.info(indent + "Expected Error message received: " + enum.text, record)
                else:
                    ReportErrors(enum.text, record)
    except: pass
    
    #check for success messages
    try:    
        enum = CONFIG.driver.find_element_by_xpath(".//*[@class='alert alert-success']")
        if enum.text != "":
            if record["results"] == "F": ReportErrors("Test step passed: " + enum.text, record)
            #else: CONFIG.logger1.info (indent + "Success message: " + enum.text)
    except: pass
    


def ReportErrors(msg, record):
    if record != "":
        #CONFIG.logger2.error(indent + "ERROR: " + msg)
        CONFIG.errorList.append([record["row"], record["description"], msg])


# This will print a summary of any errors found
def ErrorSummary():
    CONFIG.testSummary = "\n" + "ERROR SUMMARY\n"
    
    if len(CONFIG.errorList) == 1:
        CONFIG.testSummary = CONFIG.testSummary + indent + "There was 1 error encountered.\n"
    else:
        CONFIG.testSummary = CONFIG.testSummary + indent + "There were " + str(len(CONFIG.errorList)) + " errors encountered.\n"
    
    for error in CONFIG.errorList:
        CONFIG.testSummary = CONFIG.testSummary + indent + "Row: " + str(error[0]) + ", Description: " + error[1] + ":\n"
        CONFIG.testSummary = CONFIG.testSummary + indent * 2 + "Error: " + error[2] + "\n"

    CONFIG.logger1.info(CONFIG.testSummary)
    
def getCyberArkPassword(username):
    body, req, reqStatus = ("",)*4
    customHeaders = {"Accept": "application/json", "Content-type": "application/json"}
    baseURL = ""
    tokenBody = '{"username":"","password":"","useRadiusAuthentication":"false","connectionNumber":"1"}'
    getPassword = "curl -X GET '" + baseURL + "/PasswordVault/WebServices/PIMServices.svc/Accounts/" + username + "/Credentials' -H 'authorization: {{CyberArkLogonResult}}' -H 'content-type: application/json'"
    CONFIG.boolValue = ""
    customHeaders = {"Accept": "application/json", "Content-type": "application/json"}

        
        
    if request != "" and activeSSLCert == "":
        if request.startswith("https") and useAPICerts == True:
            activeSSLCert = True
            
            #CONFIG.logger1.info(indent + "Client Cert path: '%s'" % CONFIG.cert_file_path)
            #CONFIG.logger1.info(indent + "Keystore File path: '%s'" % CONFIG.key_file_path)
            CONFIG.logger1.info(indent + "Client/Key Combined File path: '%s'" % CONFIG.combined_path)
            #CONFIG.logger1.info(indent + "CA Cert File path: '%s'" % CONFIG.CA_file_path)
        else:
            activeSSLCert = False   
        print(request)    
        if type.upper() == "POST":
            if activeSSLCert:
                req = requests.post(request, data=body, headers=customHeaders, cert=CONFIG.combined_path, verify=CONFIG.CA_file_path, timeout=120)
            else:
                CONFIG.logger1.info(indent + "Using user name/password authentication")
                req = requests.post(request, data=json.dumps(body), auth=(userName, password), headers=customHeaders, timeout=120)
        if type.upper() == "DELETE":
            if activeSSLCert:
                req = requests.delete(request, data=body, headers=customHeaders, cert=CONFIG.combined_path, verify=CONFIG.CA_file_path, timeout=120)
            else:
                CONFIG.logger1.info(indent + "Using user name/password authentication")
                req = requests.delete(request, data=json.dumps(body), auth=(userName, password), headers=customHeaders, timeout=120)
        elif type.upper() == "PUT":
            if activeSSLCert or useAPICerts == True:
                req = requests.put(request, data=body, headers=customHeaders, cert=CONFIG.combined_path, verify=CONFIG.CA_file_path, timeout=120)
            else:
                req = requests.put(request, auth=(userName, password), data=body, headers=customHeaders, timeout=120)
        elif type.upper() == "GET": 
            if activeSSLCert:
                req = requests.get(request, data=body, headers=customHeaders, cert=CONFIG.combined_path, verify=CONFIG.CA_file_path, timeout=120)
            else:
                req = requests.get(request, auth=(userName, password), headers=customHeaders, timeout=120)
        if CONFIG.perfCounter <=1:
            if CONFIG.verboseReporting:
                CONFIG.logger1.info("%sAPI Header:%s%s" % (indent,indent,req.request.headers))
            if body == "":
                CONFIG.logger1.info("%sAPI Request:%s(%s)%s%s\n" % (indent,indent,type.upper(),indent,request))
            else:
                CONFIG.logger1.info("%sAPI Request:%s(%s)%s%s %s\n" % (indent,indent,type.upper(),indent,request, body))
    else:
        UTILS.ReportErrors("--- missing required parameters", record)
        return

    APIResponse = req.text
    while APIResponse == "":
        time.sleep(1)
        if req.status_code == 204:
            CONFIG.logger1.info("%sAPI Status Code:%s%s\n" %(indent, indent, str(req.status_code)))
            APIResponse = "No Content in Response Message"
            break
    reqStatus = req.status_code
    if (((str(reqStatus)[0] != "2") and str(reqStatus)[0] != "4") and (record["results"] != "F")) or (reqStatus == 500):
        UTILS.ReportErrors("API Request failed: HTTP Status code:%s\n " %(str(reqStatus)), record)
    elif (reqStatus == 201 or reqStatus == 200): 
        CONFIG.logger1.info("%sAPI Status Code:%s%s\n" %(indent, indent, str(reqStatus)))  
    
    if CONFIG.perfCounter <=1:
        CONFIG.logger1.info("%sAPI Response:%s%s" %(indent, indent, APIResponse))

    # if the request did not return a http status of 200 but was exptected to
    return
    