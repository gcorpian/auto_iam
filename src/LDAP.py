#---------- IMPORTS ----------
from ldap3 import Server, Connection, SIMPLE, SYNC, ASYNC, SUBTREE, BASE, LEVEL, ALL, ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES
import base64
import json
import datetime
from datetime import date, tzinfo 

#-------CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
CONFIG.attributeValues = ['sn', 'objectClass', 'harvardEduIDNumber']
recordDN, dn = ("",) *2

def MappingTable(record):
    #converts the screen value from the spreadsheet into a function that controls how interactions take place.
    available_mappings = {
        "SEARCH RECORD": LDAPOperations,
        "DELETE RECORD": LDAPOperations,
        "VERIFY RECORD": VerifyRecord,
        "CREATE RECORD": CreateRecord,
        "MODIFY RECORD": LDAPOperations
        } 
    MappedObjects = available_mappings.get(record["screen"], lambda x: None)(record)
    
    
def LDAPOperations(record):
    #defaults
    LDAPPassword, recordId = ("",) *2
    recordFilter = "(objectClass=*)"
    CONFIG.attributeValues = []
    
    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1]       
        if fieldName == "HUID": 
            if serverName == "AUTHLDAP": recordFilter = "(uid=" + expectedValue + ")"
            elif serverName == "HULDAP": recordFilter = "(harvardEduIDNumber=" + expectedValue + ")"
            elif serverName == "UNIFIED LDAP": recordFilter = "(harvardEduIDNumber=" + expectedValue + ")"
            elif serverName == "HLDAP": recordFilter = "(harvardEduIDNumber=" + expectedValue + ")"
            elif serverName == "FASLDAP": recordFilter = "(hascsHarvardEduLinkedIDNumber=" + expectedValue + ")"
            elif serverName == "FASMAIL": return
            else: recordFilter = "(harvardEduADHUID=" + expectedValue + ")"
        elif fieldName == "ADID" or fieldName == "NETID": 
            if serverName == "UNIVAD": recordFilter = "(cn=" + expectedValue + ")"
            elif serverName == "HLDAP": recordFilter = "(uid=" + expectedValue + ")"
            elif serverName == "HULDAP": recordFilter = "(harvardEduADID=" + expectedValue + ")"
            else: 
               UTILS.ReportErrors("ADID is an invalid search parameter for LDAP " + serverName, record)
               return 
        elif fieldName == "UID": 
            if serverName == "HULDAP": recordFilter = "(harvardEduADID=" + expectedValue + ")"
            else: recordFilter = "(uid=" + expectedValue + ")"
        elif fieldName == "CN": recordFilter = "(cn=" + expectedValue + ")"
        elif fieldName == "OU" or fieldName == "DC": recordOU = expectedValue
        elif fieldName == "FILTER": recordFilter = expectedValue
        elif fieldName == "HARVARDEDUIDLOGIN": 
            if serverName == "UNIFIED LDAP": 
                recordFilter = "(harvardEduIDLogin=" + expectedValue + ")"
                recordOU = "ou=xid-people,dc=harvard,dc=edu"
        elif fieldName == "SERVER NAME": 
            serverName = expectedValue
            if serverName == "HLDAP":
                server = CONFIG.HLDAPServer
                permissions = CONFIG.HLDAPPermissions
                recordOU = "dc=harvard,dc=edu"
                #recordFilter = 
            elif serverName == "HULDAP":
                server = CONFIG.HULDAPServer
                permissions = CONFIG.HULDAPPermissions
                recordOU = "ou=people,o=Harvard University Core,dc=huid,dc=harvard, dc=edu"
                #recordFilter = 
            
            elif serverName == "UNIFIED LDAP":
                server = CONFIG.UnifiedLDAPServer
                permissions = CONFIG.UnifiedLDAPPermissions
                recordOU = "ou=people,dc=harvard,dc=edu"
                
            elif serverName == "UNIVAD":
                server = CONFIG.UNIVADServer
                permissions = CONFIG.UNIVADPermissions
                if CONFIG.env == "P-3": recordOU = "dc=fuisidmdev.local"
                elif CONFIG.env == "P-2": recordOU = "dc=univad,dc=harvard,dc=edu"
                elif CONFIG.env == "P-1": recordOU = "ou=Schools,dc=huitdev,dc=harvard,dc=edu"
                elif CONFIG.env == "P-0": recordOU = "dc=university,dc=harvard,dc=edu"
            elif serverName == "FASAD":
                server = CONFIG.FASADServer
                permissions = CONFIG.FASADPermissions
                if CONFIG.env == "P-3": recordOU = "dc=testad,dc=fas,dc=harvard,DC=edu"
                elif CONFIG.env == "P-2": recordOU = "dc=fasad,dc=harvard,dc=edu"
                elif CONFIG.env == "P-1": recordOU = "ou=FASusers,dc=idm-test-ad,dc=fas,dc=harvard, dc=edu"
                elif CONFIG.env == "P-0": recordOU = "dc=ad,dc=fas,dc=harvard,DC=edu"
            elif serverName == "AUTHLDAP":
                server = CONFIG.AUTHLDAPServer
                permissions = CONFIG.AUTHLDAPPermissions
                recordOU = "dc=auth,dc=harvard,dc=edu"
            elif serverName == "FASLDAP":
                server = CONFIG.FASLDAPServer
                permissions = CONFIG.FASLDAPPermissions
                recordOU = "dc=fas, dc=harvard, dc=edu"
            elif serverName == "FASMAIL": 
                if CONFIG.env in ["P-0", "P-1"]:
                    server = CONFIG.FASMAILServer
                    permissions = CONFIG.FASMAILPermissions
                    if CONFIG.env == "P-1": recordOU = "dc=fasmailtest,dc=priv"
                    elif CONFIG.env == "P-0": recordOU = "dc=fasmail,dc=priv"
                else: return
            serverValue = server[0]
            portValue = server[1]
            sslValue = True
            userName = permissions[0]
            if LDAPPassword == "":
                LDAPPassword = base64.b64decode(bytes(permissions[1], "utf-8")).decode('ascii')
        elif fieldName == "ATTRIBUTE": 
            if expectedValue.upper() == "ALL": CONFIG.attributeValues.append(ALL_ATTRIBUTES)
            elif expectedValue.upper() == "*":CONFIG.attributeValues = [ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES]
            else: CONFIG.attributeValues.append(expectedValue)
        elif fieldName == "USER NAME": userName = expectedValue
        elif fieldName == "PASSWORD": LDAPPassword = base64.b64decode(bytes(expectedValue, "utf-8")).decode('ascii')
    
    if CONFIG.attributeValues == []: CONFIG.attributeValues.append(ALL_ATTRIBUTES)

    s = Server(serverValue, port=portValue, get_info=ALL, use_ssl=server[2])  # define an unsecure LDAP server, requesting info on DSE and schema
    c = Connection(s, auto_bind=True, client_strategy=SYNC, user=userName, password=LDAPPassword, authentication=SIMPLE, check_names=True, raise_exceptions=False)
    if c.result['description'] == "success": 
        if CONFIG.verboseReporting == True:
            CONFIG.logger1.info(indent + "Connected to %s Server: %s" %(serverName, str(s)))
            print(userName + "/" + LDAPPassword)
    else:
        UTILS.ReportErrors("Unable to connect to %s Server %s: %s" %(serverName, str(s), str(c.result['description']), record))
        #CONFIG.logger1.info(indent + "Unable to connect to LDAP Server " + str(s) + ": " + str(c.result['description']))
        return None
    if recordId == "":
        recordDN = recordOU
    else:
        recordDN = recordId + "," + recordOU
    
    if record["screen"].upper() == "DELETE RECORD":
        c.search(recordDN, recordFilter, search_scope=SUBTREE,  attributes=['dn'])
        if len(c.response) == 0:
            UTILS.ReportErrors("AD record %s not found in %s subtree %s" %(recordFilter, serverName, recordDN), record)
        else:
            for value in c.response:
                if str(value['dn']) != "":
                    CONFIG.logger1.info(indent + "Deleting LDAP record: " + str(value['dn']))
                    c.delete(str(value['dn']))

    elif record["screen"].upper() == "SEARCH RECORD":
        result = SearchRecord(c, recordDN, recordFilter, serverName, record)
        return result
    elif record["screen"].upper() == "MODIFY RECORD":
        c.search(recordDN, recordFilter, search_scope=SUBTREE,  attributes=CONFIG.attributeValues, get_operational_attributes=True)
        recordDN = c.response[0]['dn']
        result = ModifyRecord(c, recordDN, record)
    c.unbind()
    return CONFIG.attributeValues

# unfinished - may not be needed
def CreateRecord(record):
    # recordId, recordOU, recordInfo
    s = Server('ldap-master.dev.iam.harvard.edu', port = 6636, get_info = ALL, use_ssl=True)  # define an unsecure LDAP server, requesting info on DSE and schema
    c = Connection(s, auto_bind = True, client_strategy = SYNC, user='CN=Directory Manager', password='8aqEprEs4AThaHev', authentication=SIMPLE, check_names=True)
    
    recordDN = recordId + "," + recordOU
    recordAttrib = [(k, v) for (k, v) in recordInfo.items()]
    print("Adding record %s with ou=%s" % (recordDN, recordOU))
    ldapconn.add(recordDN, recordAttrib)
    c.unbind()


def ModifyRecord(c, recordDN, record):
    CONFIG.logger1.info(indent + "Modifying record %s" %(recordDN))
    query = {}
    counter = 0
    operation = "MODIFY_REPLACE"
    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1]
        if fieldName == "SERVER NAME" or fieldName == "DN":
            continue
        elif fieldName == "OPERATION":
            if expectedValue == "ADD": operation = "MODIFY_ADD"
            elif expectedValue == "DELETE": operation = "MODIFY_DELETE"
            elif expectedValue == "REPLACE": operation = "MODIFY_REPLACE"
            elif expectedValue == "INCREMENT": operation = "MODIFY_INCREMENT"
        elif fieldName == "UID":
            pass #do nothing
        else: 
            CONFIG.logger1.info((indent)*2 + operation + " " + fieldName + " with " + expectedValue)
            query.setdefault(fieldName, [operation, expectedValue])
        
    c.modify(recordDN, query)   


def VerifyRecord(record):
    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1]
        if fieldName in str(CONFIG.attributeValues.keys()).upper():
            if CONFIG.attributeValues[fieldName] == expectedValue: CONFIG.logger1.info(indent + fieldName.upper() + " matched expected value: '%s'" %(expectedValue))
            else: UTILS.ReportErrors(fieldName.upper() + " actual value ('%s') did not match expected value: '%s'" %(CONFIG.attributeValues[fieldName], expectedValue), record)
        else: UTILS.ReportErrors("Attribute %s was not found in the returned attribute list " %(fieldName.upper()), record)
        

def SearchRecord(c, recordDN, recordFilter, serverName, record):
    if CONFIG.attributeValues[0] == '*': getOps = True
    else: getOps = False
    c.search(recordDN, recordFilter, search_scope=SUBTREE,  attributes=CONFIG.attributeValues, get_operational_attributes=getOps)
    if len(c.response) == 0:
        UTILS.ReportErrors("AD record %s not found in %s subtree %s" %(recordFilter, serverName, recordDN), record)
        return None
    #elif len(c.response) == 1:
    #    c.response = c.response[0]
    else:
        if len(c.response) > 1 and CONFIG.verboseReporting == True:
            print("Multiple records returned:")
            CONFIG.logger1.info("\n" + indent + "Number of records returned: " + str(len(c.response)))
        for value in c.response:
            if 'attributes' in value.keys():
                CONFIG.attributeValues = value['attributes']
                if CONFIG.verboseReporting == True:
                    if str(value['dn']) != "":
                        recordDN = str(value['dn'])
                    CONFIG.logger1.info("\n" + indent + recordDN) # return unicode attributes
                    CONFIG.logger1.info(indent + "ATTRIBUTES:")  # return unicode attributes
                for key in sorted(value['attributes']):
                    if serverName == "FASAD" and key == "sAMAccountName":
                        CONFIG.FASUserName = value['attributes'][key]

                    value['attributes'][key] = str(value['attributes'][key]).replace("\n", " ").replace("'", "").replace("[", "").replace("]", "")
                    if CONFIG.verboseReporting == True:
                        CONFIG.logger1.info((indent)*2 + indent + "%s: %s" % (key, value['attributes'][key]))
    return c.response