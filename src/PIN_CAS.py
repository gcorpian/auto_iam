#---------- IMPORTS ----------
import base64
import poplib
import time
from datetime import datetime
import sys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException

#------- CUSTOM IMPORTS -------
import CONFIG
import UTILS
import KEYMGMT

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent


def MappingTable(record):
    available_mappings = {
        "PIN LOGIN": PINLogin,
        "CAS LOGIN": CASLogin, 
        "GET URL": GetURL,
        "RESULT PAGE": ResultPage
        } 
    MappedObjects = available_mappings.get(record["screen"], lambda x: None)(record)
    
    if record["screen"].upper() != "GET URL":
        UTILS.UseScreenObjects(MappedObjects, record)
        CONFIG.startTime = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
        UTILS.StatusCheck(record)
    

def GetURL(record):
    if CONFIG.driver == "":
        CONFIG.driver = UTILS.OpenBrowser(CONFIG.browser)

    CONFIG.driver.get(record["values"][0])
    UTILS.WaitForObject("XPATH", ".//*[@id='submitLogin']", record, 10)
    
def CASLogin(record):    
    
    return {"PASSWORD": ".//*[@id='password']",
            "LOGIN NAME": ".//*[@id='username']",
            "HUID": ".//*[@id='username']",
            "LOGIN ID": ".//*[@id='username']",
            "ERROR": ".//*[@id='casMsg']",
            "TABS":{"HUID": "//p[contains(.,'HUID')]",
                    "XID": "//p[contains(.,'XID')]",
                    "ECOMMONS": "//p[contains(.,'eCommons')]",
                    "FAS/CENTRAL": "//p[contains(.,'FAS/Central')]"},
            "ACTION":{"LOGIN": ".//*[@id='submitLogin']",
                       "FORGOT YOUR PASSWORD": "link_text:Forgot Your Password?>",
                       "FORGOT YOUR LOGIN NAME": "link_text:Forgot Your Login Name?>",
                       "GET YOUR HARVARD KEY": "link_text:Get Your HarvardKey >"}
                    }

def PINLogin(record):
    if CONFIG.env == "P-3": url = "https://pin-cas.dev.iam.harvard.edu/pin/authenticate?__authen_application=PIN_LOGIN_FUNCTIONAL_TEST_APP_"
    elif CONFIG.env == "P-2": url = "https://pin-cas.qa.iam.harvard.edu/pin/authenticate?__authen_application=PIN_LOGIN_FUNCTIONAL_TEST_APP_"
    elif CONFIG.env == "P-1": url = "https://stage.pin1.harvard.edu/cas/login?service=https%3A%2F%2Fstage.pin1.harvard.edu%2Fpin%2Fauthenticate%3F__authen_application%3DPIN_LOGIN_FUNCTIONAL_TEST_APP_1%26testAppName%3DPIN_Functional_Test_Application_1%26miscParameter%3DStateValue1"
    #elif CONFIG.env == "P-1": url = "https://stage.pin1.harvard.edu/pin-test/home"
    elif CONFIG.env == "P-0":url = "https://www.pin1.harvard.edu/cas/login?service=https%3A%2F%2Fwww.pin1.harvard.edu%2Fpin%2Fauthenticate%3F__authen_application%3DPIN_LOGIN_FUNCTIONAL_TEST_APP_1%26testAppName%3DPIN_Functional_Test_Application_1%26miscParameter%3DStateValue1"
    
    if len(record["fields"]) != len(record["values"]):
        UTILS.ReportErrors("Record count mismatch between Fields and Expected Values.", record)
        if record["results"] == "T":
            sys.exit()
        else:
            return
    
    if CONFIG.driver == "":
        CONFIG.driver = UTILS.OpenBrowser(CONFIG.browser)
    
    if CONFIG.driver.current_url != url:
        CONFIG.driver.get(url)
                
    MappedObjects = {"PASSWORD": ".//*[@id='password']",
                    "LOGIN NAME": ".//*[@id='username']",
                    "HUID": ".//*[@id='username']",
                    "LOGIN ID": ".//*[@id='username']",
                    "ERROR": ".//*[@id='casMsg']",
                    "TABS":{"HUID": "//p[contains(.,'HUID')]",
                            "XID": "//p[contains(.,'XID')]",
                            "ECOMMONS": "//p[contains(.,'eCommons')]",
                            "FAS/CENTRAL": "//p[contains(.,'FAS/Central')]"},
                    "ACTION":{"LOGIN": ".//*[@id='submitLogin']",
                               "FORGOT YOUR PASSWORD": "link_text:Forgot Your Password?>",
                               "FORGOT YOUR LOGIN NAME": "link_text:Forgot Your Login Name?>",
                               "GET YOUR HARVARD KEY": "link_text:Get Your HarvardKey >"}
                            }

    UTILS.UseScreenObjects(MappedObjects, record)
    
    try:
        enum = UTILS.WaitForObject("XPATH", ".//*[@id='casMsg']", record, 3)
        if enum == 0:
            enum = CONFIG.driver.find_element_by_xpath(".//*[@id='casMsg']")
            if record["results"] == "T": UTILS.ReportErrors(enum.text, record)
            else: CONFIG.logger1.info (indent + "EXPECTED ERROR: " + enum.text)
    except:
        pass

    
def ResultPage(record):
    UTILS.WaitForObject("XPATH", ".//*[@id='container']/a", record, 10)
    CONFIG.stopTime = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
    CONFIG.logger1.info (indent + "This page took " + str(CONFIG.stopTime - CONFIG.startTime) + " seconds to load")
    
    #CONFIG.driver.find_element_by_xpath(".//*[@id='container']/a").click()
    return {"PASSWORD": ".//*[@id='password']",
            "LOGIN NAME": ".//*[@id='username']",
            "HUID": ".//*[@id='username']",
            "LOGIN ID": ".//*[@id='username']",
            "ERROR": ".//*[@id='casMsg']",
            "ACTION":{"LOGOUT": ".//*[@id='container']/a"}
                    }

        