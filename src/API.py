# ---------- IMPORTS ----------
import base64
import requests
import json
# import pycurl
import time
from subprocess import Popen, PIPE, STDOUT

# -------CUSTOM IMPORTS -------
import CONFIG
import UTILS

# ---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
APIResponse = ""
reqStatus = ""


def MappingTable(record):
    # converts the screen value from the spreadsheet into a function that
    # controls how interactions take place.

    available_mappings = {
        "INPUT": InputAPI,
        "VERIFY": VerifyAPI
        }
    available_mappings.get(record["action"], lambda x: None)(record)


# This will check to see if a string or int value matches the expected value
def ParseSimple(actualValue, expectedValue):
    # print(str(actualValue) + ": " + str(expectedValue))
    if isinstance(actualValue, str):
        actualValue = actualValue.replace("\r", "").replace("\n", "")
        expectedValue = expectedValue.replace("\r", "").replace("\n", "")
    if str(actualValue).upper() == str(expectedValue).upper():
        return True
    else:
        # print(actualValue)
        return False


def ParseList(actualValue, expectedValue, fieldName):
    for x in range(len(actualValue)):
        # print("L ITEM--> " + str(x) + ": VALUE " + str(actualValue[x]) + "\n")
        # print(actualValue[x] + ": " + expectedValue)
        if str(actualValue[x]) == expectedValue:
            return True

        if not (isinstance(actualValue[x], str)):
            for listElement in actualValue[x]:
                # print(indent + "ELEMENT--> " + str(listElement) + ": " + _ 
                #str(actualValue[x][listElement]) + "\n")
                if actualValue[x][listElement] == expectedValue:
                    return True
                elif (isinstance(actualValue[x][listElement], str)) or (isinstance(actualValue[x][listElement], int)):
                    # print("L STRING ITEM--> " + listElement + ": " + str(actualValue[x][listElement]) + "\n")
                    if listElement == fieldName:
                        bool = ParseSimple(actualValue[x][listElement], expectedValue)
                        if bool:
                            return bool
                elif isinstance(actualValue[x][listElement], list):
                    # print("L LIST ITEM --> " +  listElement + ": " + str(actualValue[x][listElement]) + "\n")
                    bool = ParseList(actualValue[x][listElement], expectedValue, fieldName)
                    if bool:
                        return bool
                elif isinstance(actualValue[x][listElement], dict):
                    # print("L DICT ITEM --> " +  listElement + ": " + str(actualValue[x][listElement]) + "\n")
                    bool = ParseDict(actualValue[x][listElement], expectedValue, fieldName)
                    if bool:
                        return bool 
    return False


def ParseDict(actualValue, expectedValue, fieldName):
    for dictElement in actualValue:
        # print("D ITEM: " + dictElement + ": " + str(actualValue[dictElement]) + "\n")
        if actualValue[dictElement] == expectedValue:
            return True

        elif (isinstance(actualValue[dictElement], str)) or (isinstance(actualValue[dictElement], int)):
            # print("D STRING ITEM --> " + dictElement + ": " + str(actualValue[dictElement]) + "\n")
            if dictElement == fieldName:
                bool = ParseSimple(actualValue[dictElement], expectedValue)
                return bool

        elif isinstance(actualValue[dictElement], list):
            # print("D LIST ITEM --> " + dictElement + ": " + str(actualValue[dictElement]) + "\n")
            bool = ParseList(actualValue[dictElement], expectedValue, fieldName)
            if bool:
                return bool
        elif isinstance(actualValue[dictElement], dict):
            # print("D DICT ITEM --> " + dictElement + ": " + str(actualValue[dictElement]) + "\n")
            bool = ParseDict(actualValue[dictElement], expectedValue, fieldName)
            if bool:
                return bool

    return False, actualValue


def InputAPI(record):
    global APIResponse
    global reqStatus
    body, activeSSLCert, req, reqStatus = ("",)*4
    request = CONFIG.IdentityAPIURL
    useAPICerts = True
    CONFIG.boolValue = ""

    # verifyServerSSL = False
    # cert = (CONFIG.cert_file_path, CONFIG.key_file_path)

    userName, password, type = ("",)*3
    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1]

        if fieldName.upper() == "REQUEST": 
            request = expectedValue.replace(" ", "%20")
            if CONFIG.env == "P-0": request = request.replace("qa.", "")
            elif CONFIG.env == "P-1": request = request.replace("qa.", "stage.")
            elif CONFIG.env == "P-3": request = request.replace("qa.", "dev.")#.replace("/v2", "")            
        elif fieldName.upper() == "USER NAME": 
            useAPICerts = False
            userName = expectedValue
            if userName in CONFIG.userLists.keys():
                password = base64.b64decode(bytes(CONFIG.userLists[userName], "utf-8")).decode('ascii')
                print(password)
        elif fieldName.upper() == "PASSWORD": password = base64.b64decode(bytes(expectedValue, "utf-8")).decode('ascii')
        elif fieldName.upper() == "TYPE": type=expectedValue
        elif fieldName.upper() == "BODY" or fieldName.upper() == "MESSAGE": body=expectedValue
        elif fieldName.upper() == "HUID": request= request + "/huid:" + str(expectedValue)
        elif fieldName.upper() == "CERTS": useCert=expectedValue

    if record["screen"].upper() == "IDENTITY":
        # headers = {'Accept': 'application/json', 'content_type':'application/json'}
        customHeaders = {"Accept": "application/json", "Content-type": "application/json"}
    elif record["screen"].upper() == "GROUPER":
        useAPICerts = False
        customHeaders = {"Content-type": "text/x-json;charset=UTF-8"}
    else: customHeaders = {"Accept": "application/json", "Content-type": "application/json"}

    if request != "" and activeSSLCert == "":
        if request.startswith("https") and useAPICerts == True:
            activeSSLCert = True

            # CONFIG.logger1.info(indent + "Client Cert path: '%s'" % CONFIG.cert_file_path)
            # CONFIG.logger1.info(indent + "Keystore File path: '%s'" % CONFIG.key_file_path)
            CONFIG.logger1.info(indent + "Client/Key Combined File path: '%s'" % CONFIG.combined_path)
            # CONFIG.logger1.info(indent + "CA Cert File path: '%s'" % CONFIG.CA_file_path)
        else:
            activeSSLCert = False   
        if type.upper() == "POST":
            if activeSSLCert:
                req = requests.post(request, data=body, headers=customHeaders, cert=CONFIG.combined_path, verify=CONFIG.CA_file_path, timeout=120)
            else:
                CONFIG.logger1.info(indent + "Using user name/password authentication")
                req = requests.post(request, data=json.dumps(body), auth=(userName, password), headers=customHeaders, timeout=120)
        if type.upper() == "DELETE":
            if activeSSLCert:
                req = requests.delete(request, data=body, headers=customHeaders, cert=CONFIG.combined_path, verify=CONFIG.CA_file_path, timeout=120)
            else:
                CONFIG.logger1.info(indent + "Using user name/password authentication")
                req = requests.delete(request, data=json.dumps(body), auth=(userName, password), headers=customHeaders, timeout=120)
        elif type.upper() == "PUT":
            if activeSSLCert or useAPICerts == True:
                req = requests.put(request, data=body, headers=customHeaders, cert=CONFIG.combined_path, verify=CONFIG.CA_file_path, timeout=120)
            else:
                req = requests.put(request, auth=(userName, password), data=body, headers=customHeaders, timeout=120)
        elif type.upper() == "GET": 
            if activeSSLCert:
                req = requests.get(request, data=body, headers=customHeaders, cert=CONFIG.combined_path, verify=CONFIG.CA_file_path, timeout=120)
            else:
                req = requests.get(request, auth=(userName, password), headers=customHeaders, timeout=120)
        if CONFIG.perfCounter <=1:
            if CONFIG.verboseReporting:
                CONFIG.logger1.info("%sAPI Header:%s%s" % (indent,indent,req.request.headers))
            if body == "":
                CONFIG.logger1.info("%sAPI Request:%s(%s)%s%s\n" % (indent,indent,type.upper(),indent,request))
            else:
                CONFIG.logger1.info("%sAPI Request:%s(%s)%s%s %s\n" % (indent,indent,type.upper(),indent,request, body))
    else:
        UTILS.ReportErrors("--- missing required parameters", record)
        return

    APIResponse = req.text
    while APIResponse == "":
        time.sleep(1)
        if req.status_code == 204:
            CONFIG.logger1.info("%sAPI Status Code:%s%s\n" %(indent, indent, str(req.status_code)))
            APIResponse = "No Content in Response Message"
            break
    reqStatus = req.status_code
    if (((str(reqStatus)[0] != "2") and str(reqStatus)[0] != "4") and (record["results"] != "F")) or (reqStatus == 500):
        UTILS.ReportErrors("API Request failed: HTTP Status code:%s\n " %(str(reqStatus)), record)
    elif (reqStatus == 201 or reqStatus == 200): 
        CONFIG.logger1.info("%sAPI Status Code:%s%s\n" %(indent, indent, str(reqStatus)))  

    if CONFIG.perfCounter <=1:
        CONFIG.logger1.info("%sAPI Response:%s%s" %(indent, indent, APIResponse))

    # if the request did not return a http status of 200 but was exptected to
    return


def VerifyAPI(record):
    global APIResponse, reqStatus
    if reqStatus == 500:
        UTILS.ReportErrors("Test Case aborted due to error code 500", record)
        return

    if APIResponse == "":
        UTILS.ReportErrors("There was no response to parse", record)
        return
    try:
        Response = json.loads(APIResponse)
    except:
        UTILS.ReportErrors("Failed converting API response into JSON format", record)
        return

    for c1 in range(0, len(record["fields"])):
        bool = ""
        fieldName = record["fields"][c1].strip()
        expectedValue = record["values"][c1].strip()
        
        if expectedValue.upper() == "TODAY":
            expectedValue = str(datetime.date.today())

        if fieldName.upper() == "MATCHSCORE" and expectedValue.upper() == "EXACT":
            if APIResponse.find('"partialMatches":null'):
                CONFIG.logger1.info(indent + "'%s' value matched expected value: '%s'" % (fieldName, expectedValue))
            else:
                UTILS.ReportErrors("'%s' value did not match expected value: '%s'" % (fieldName, expectedValue), record)
            continue

        if fieldName.upper() == "RESPONSE":
            actualValue = Response.replace('}', ',')
            if actualValue == expectedValue:
                CONFIG.logger1.info(indent + "'%s' value matched Expected Value: '%s'" % (fieldName, expectedValue))
            elif expectedValue == "*":
                CONFIG.logger1.info(indent + "'%s' value (%s) -- No match on Expected Value (%s)" % (fieldName, actualValue, expectedValue))
            else:
                UTILS.ReportErrors("'%s' value (%s) did not match Expected Value: '%s'" % (fieldName, actualValue, expectedValue), record)

        else:
            for element in Response:
                if isinstance(element, dict):
                    bool = ParseDict(element, expectedValue, fieldName)
                    if bool:
                        break
                elif (isinstance(Response[element], str)) or (isinstance(Response[element], int)):
                    if element == fieldName:
                        bool = ParseSimple(Response[element], expectedValue)
                        break
                elif isinstance(Response[element], list):
                    bool = ParseList(Response[element], expectedValue, fieldName)
                elif isinstance(Response[element], dict):
                    bool = ParseDict(Response[element], expectedValue, fieldName    )
            if bool == True:
                CONFIG.logger1.info("%s'%s' value matched expected value: '%s'" % (indent, fieldName, expectedValue))
            elif bool == False:
                UTILS.ReportErrors("'%s' value did not match expected value: '%s'" % (fieldName, expectedValue), record)
            elif bool == "":
                UTILS.ReportErrors("'%s' was not found in the JSON response." % fieldName, record)