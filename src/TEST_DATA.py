#---------- ENVIRONMENT SETUP ----------
#env = "ANDROID"
#env = "IOS"
#env = "P-0"     #prod
#env = "P-1"     #stage
env = "P-2"     #qa
#env = "P-3"    #dev


#---------- SPREADSHEET SELECTION ---------- 
filename = "../Data/Auto_IAM_Tests.xlsm"


#---------- WORKSHEET SELECTION ----------
####  UNIT TESTS  ####
#worksheet = "Group_Counts"
#worksheet = "CLASPART"


####  SMOKE TESTS  ####
#worksheet = "Identity_API_Smoke
#worksheet = "Midas_Smoke"
#worksheet = "Midas_Search"
#worksheet = "GrouperAPI"
#worksheet = "GrouperUI"
#worksheet = "KeyMgmt"

####  REGRESSION TESTS  ####
worksheet = "Identity_API_Regression"
#worksheet = "Midas_Regression"
#worksheet = "XID"

####  ERRATA TESTS  ####
#worksheet = "Test"
#worksheet = "Grouper_XID"
#worksheet = "PIN-CAS"
#worksheet = "Authentication_Test"
#worksheet = "Match Scoring"
#worksheet = "API Common Name"
#worksheet = "API emails"
#worksheet = "IDMDATA-2545"
#worksheet = "Unclaim Key"
#worksheet = "UNIFIED LDAP"
#worksheet = "GrouperAPI_Performance"
#worksheet = "Connectors"
#worksheet = "Matching"
#worksheet = "ALUMNI_Test_API"
#worksheet = "Identity_API"
#worksheet = "PIN-CAS"
#worksheet = "ID_API_DEV"
#worksheet = "ID_API_QA"
#worksheet = "ID_API_STG"
#worksheet = "Email_API_QA"
#worksheet = "LoginName_API_QA"
#worksheet = "Email_API_Stage"
#worksheet = "Sailpoint_IIQ"
#worksheet = "Bulk_KeyMgmt"
#worksheet = "QA_Midas"
#worksheet = "Midas+API"
#worksheet = "Reset"
#worksheet = "Sailpoint_API_QA"



#---------- BROWSER SELECTION ----------
#browser, browserProfile = "FIREFOX", "../Profiles/Firefox"
browser, browserProfile = "CHROME", ""
#browser, browserProfile = "IE", ""
#browser, browserProfile = "PHANTOMJS", ""


#---------- SETUP VARIABLES  ----------
dbRecordResults = False
performanceMode = False
perfCounter = 0
totalTests = 25
verboseReporting = True
refreshIIQ = True
useAPICerts = True     # Set this to True if Certs are used in the API testing


#---------- CLIENT CERT SETUP ----------
#clientConfig is [client] [permissions]
# [client] = FAS_AURORA, HMS, IAM, QA, SIS
# [permissions] = IMPORT, FIND, FORCE, ALL
clientConfig = "QA ALL"

