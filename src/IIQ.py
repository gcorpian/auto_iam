#---------- IMPORTS ----------
import sys
import time
from selenium.webdriver.common.action_chains import ActionChains

#-------CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent


def MappingTable(record):
    #converts the screen value from the spreadsheet into a function that controls how interactions take place.
    
    available_mappings = {
        "ACCESS REQUESTS": AccessRequests,
        "ACTIVITY TARGET CATEGORIES": ActivityTargetCategories,
        "ADVANCED ANALYTICS": AdvancedAnalytics,
        "ANALYZE": Analyze,
        "APPLICATION RISK MODEL": ApplicationRiskModel,
        "APPLICATION RISK SCORES": ApplicationRiskScores,
        "APPLICATIONS": Applications,
        "BATCH REQUESTS": BatchRequests,
        "BUSINESS PROCESSES": BusinessProcesses,
        "CERTIFICATIONS": Certifications,
        "DASHBOARD": DashboardPage,
        "DEFINE": Define,
        "ENTITLEMENT CATALOG": EntitlementCatalog,
        "GROUPS": Groups,
        "IDENTITIES": Identities,
        "IDENTITY CORRELATION": IdentityCorrelation,
        "IDENTITY RISK MODEL": IdentityRiskModel,
        "IDENTITY RISK SCORES": IdentityRiskScores,
        "LIFECYCLE EVENTS": LifecycleEvents,
        "LOGIN": Login,
        "MANAGE": Manage,
        "MONITOR": Monitor,
        "MY ACCESS REVIEWS": MyAccessReviews,
        "POLICIES": Policies,
        "POLICY VIOLATIONS": PolicyViolations,
        "REFRESH IDENTITY": RefreshIdentity,
        "REPORTS": Reports,
        "ROLES": Roles,
        "SYSTEM SETUP": SystemSetup,
        "TASKS": Tasks,
        "VIEW IDENTITY": ViewIdentity,
        "WORK ITEMS": WorkItems,
        } 
    available_mappings.get(record["screen"], lambda x: None)(record)
    if record["action"] == "NAVIGATE":
        CONFIG.activeTab = ""
        

def AccessRequests(record):
      
    CONFIG.logger1.Info("ACCESS REQUESTS FOUND")
    if CONFIG.driver.current_url.find("analyze") == -1:
        CONFIG.logger1.info(indent + "Navigating to the Analyze page")
        CONFIG.driver.find_element_by_link_text("Analyze").click()  


def ActivityTargetCategories(record):
    #placeholder
    pass


def AdvancedAnalytics(record):
    if CONFIG.activeTab == "": CONFIG.activeTab = "IDENTITY SEARCH"
    
    if CONFIG.driver.current_url.find("analyzeTabs.jsf") == -1:  # if not on the Analytics page
        ActionChains(CONFIG.driver).move_to_element(CONFIG.driver.find_element_by_xpath("//*[contains(@href,'/analyze/index.jsf')]")).perform()
        CONFIG.driver.find_element_by_xpath("//*[contains(@class, 'x-menu-item-text') and contains(text(), 'Advanced Analytics')]").click()
        
        CONFIG.logger1.info(indent + "Url change: " + CONFIG.driver.current_url)

    result =UTILS.WaitForObject("XPATH", ".//button[@id='tab-1305-btnEl' and @class='x-tab-center']", record, 10)
    
    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1].upper()
        if fieldName == "ACTION":
            CONFIG.activeTab = "IDENTITY SEARCH"
            if expectedValue == "ADVANCED SEARCH": enum = CONFIG.driver.find_element_by_id("advancedIdentitySearchNavBtn-btnEl")
            if expectedValue == "IDENTITY SEARCH": enum = CONFIG.driver.find_element_by_id("identitySearchNavBtn-btnEl")
            if expectedValue == "RUN SEARCH": 
                enum = CONFIG.driver.find_element_by_id("preIdentitySearchBtn-btnEl")
            if expectedValue == "CLEAR SEARCH": 
                enum = CONFIG.driver.find_element_by_id("button-1300-btnEl")
                if enum:
                    enum.click()
                    enum = CONFIG.driver.find_element_by_id("identityClearBtn-btnEl")
        elif fieldName == "TABS":            
            CONFIG.activeTab = expectedValue
            time.sleep(2)
            UTILS.WaitForObject("ID", "tab-1305-btnEl", record, 10)
            if expectedValue == "IDENTITY SEARCH": enum = CONFIG.driver.find_element_by_xpath(".//button[@id='tab-1305-btnEl' and @class='x-tab-center']")
            elif expectedValue == "ACCESS REVIEW SEARCH": enum = CONFIG.driver.find_element_by_xpath("//*[@id='tab-1306-btnEl']")
            elif expectedValue == "ROLE SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1307-btnEl")
            elif expectedValue == "ACCOUNT GROUP SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1308-btnEl")
            elif expectedValue == "ACTIVITY SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1309-btnEl")
            elif expectedValue == "AUDIT SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1310-btnEl")
            elif expectedValue == "PROCESS METRICS SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1311-btnEl")
            elif expectedValue == "ACCESS REQUEST SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1312-btnEl")
        else:    
            available_tabs = {
                "IDENTITY SEARCH": AdvancedAnalyticsIdentitySearch,
                "ACCESS REVIEW SEARCH": AdvancedAnalyticsAccessReviewSearch,
                "ROLE SEARCH": None,
                "ACCOUNT GROUP SEARCH": None,
                "ACTIVITY SEARCH": None,
                "AUDIT SEARCH": None,
                "PROCESS METRICS SEARCH": None,
                "ACCESS REQUEST SEARCH": None
                } 

            available_tabs.get(CONFIG.activeTab, lambda x: None)(record)
            break

        if record["action"] == "VERIFY":
            if enum.text.upper() == expectedValue:
                CONFIG.logger1.info(indent + fieldName + " attribute matched expected value: " + expectedValue)
            else:
                UTILS.ReportErrors("%s attribute(%s) did not match expected value: %s" %(fieldName, enum.text, expectedValue), record)
        elif record["action"] == "INPUT":
            time.sleep(2)
            enum.click()
            
def AdvancedAnalyticsIdentitySearch(record):
    MappedObjects = {"LAST NAME":".//*[@id='identitySearchForm:identityLastName']']",
                     "FIRST NAME": ".//*[@id='identitySearchForm:identityFirstName']",
                     "USERNAME": ".//*[@id='identitySearchForm:userName']",
                     "DISPLAY NAME": ".//*[@id='identitySearchForm:displayName']",
                     "EMAIL": ".//*[@id='emailSuggestCmp-inputEl']",
                     "MANAGER": ".//*[@id='managerSuggestCmp-inputEl']",
                     "IS INACTIVE": ".//*[@id='identitySearchForm:inactive']",
                     "IS MANAGER": ".//*[@id='identitySearchForm:isManager']",
                     "APPLICATIONS": ".//*[@id='baseSuggest-1353-inputEl']",
                     "INSTANCE": ".//*[@id='identInstanceSuggestCmp-inputEl']",
                     "ASSIGNED ROLE": ".//*[@id='roleSuggest-2182-inputEl']",
                     "INCLUDE ASSIGNED ROLE HIERARCHY": ".//*[@id='identitySearchForm:identityAssignedRoleHierarchy']",
                     "FAS USER NAME": ".//*[@id='identityAttributefasUserNameValSuggestCmp-inputEl']",
                     "OFFICIAL NAME": ".//*[@id='identityAttributeofficialNameValSuggestCmp-inputEl']",
                     "UPN": ".//*[@id='identityAttributeUPNValSuggestCmp-inputEl']",
                     "SPAC SPONSOR HUID": ".//*[@id='identityAttributeSPACSponsorHUIDValSuggestCmp-inputEl']",
                     "SPAC REQUESTOR HUID": ".//*[@id='identityAttributeSPACRequestorHUIDValSuggestCmp-inputEl']",
                     "HUID": ".//*[@id='identityAttributeHUIDValSuggestCmp-inputEl']",
                     "UUID": ".//*[@id='identityAttributeUUIDValSuggestCmp-inputEl']",
                     "FASAD UPN": ".//*[@id='identityAttributefasUPNValSuggestCmp-inputEl']",
                     "ADID": ".//*[@id='identityAttributeADIDValSuggestCmp-inputEl']",
                     "DETECTED ROLE": ".//*[@id='roleSuggest-2162-inputEl']",
                     "INCLUDE ROLE HIERARCHY": ".//*[@id='identitySearchForm:identityRoleHierarchy']",
                     "WORKGROUP": ".//*[@id='identitySuggest-2202-inputEl']",
                     "ACTION": {"ADVANCED SEARCH": ".//*[@id='advancedIdentitySearchNavBtn-btnEl']",
                                "IDENTITY SEARCH": ".//*[@id='identitySearchNavBtn-btnEl']",
                                "RUN SEARCH": ".//*[@id='preIdentitySearchBtn-btnEl']",
                                "CLEAR SEARCH": [".//*[@id='button-1300-btnEl']", ".//*[@id='identityClearBtn-btnEl"]},
                     "SELECT": "//*[@class='x-grid-table x-grid-table-resizer']//tbody//tr//td/div",
                     "NEWTAB": "ATTRIBUTES"}
                     
    
    UTILS.UseScreenObjects(MappedObjects, record)
    
    
    
def AdvancedAnalyticsAccessReviewSearch(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}
                     
    
    UTILS.UseScreenObjects(MappedObjects, record)


    
def Analyze(record):
      
    if CONFIG.driver.current_url.find("analyze") == -1:
        enum = CONFIG.driver.find_element_by_link_text("Analyze").click()
        CONFIG.logger1.info (indent + "Url change: " + CONFIG.driver.current_url)
    
    MappedObjects = {
                     "ACTION": {
                                }}
                     
    
    UTILS.UseScreenObjects(MappedObjects, record)
        
        

def ApplicationRiskModel(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}
                     
    
    UTILS.UseScreenObjects(MappedObjects, record)
    
    

def ApplicationRiskScores(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}

    UTILS.UseScreenObjects(MappedObjects, record)
    

def Applications(record):
    
    if CONFIG.driver.current_url.find("applications") == -1: # if not on the Applications page
        ActionChains(CONFIG.driver).move_to_element(CONFIG.driver.find_element_by_xpath("//*[contains(@href,'/define/index.jsf')]")).perform()
        CONFIG.driver.find_element_by_xpath("//*[contains(@class, 'x-menu-item-text') and contains(text(), 'Application')]").click()
        
        CONFIG.logger1.info (indent + "Url change: " + CONFIG.driver.current_url)
        CONFIG.logger1.info(indent + "Current Location: Define > Applications Screen")
        time.sleep(3)

    table = CONFIG.driver.find_elements_by_xpath("//*[@class='x-grid-table x-grid-table-resizer']//tbody//tr//td/div")
    for c1 in range(0, len(table)):
        if table[c1].text == record["values"][0]:
            for c2 in range(0, len(record["fields"])):
                fieldName = record["fields"][c2].upper()
                expectedValue = record["values"][c2]
                if fieldName == "NAME":
                    c3 = c1  
                elif fieldName == "HOST":
                    c3 = c1+1           
                elif fieldName == "TYPE":
                    c3 = c1+2 
                elif fieldName == "MODIFIED":
                    c3 = c1+3

                if record["action"] == "VERIFY":
                    if table[c3].text == expectedValue:
                        CONFIG.logger1.info(indent + "'" + fieldName + "' attribute matched expected value: " + table[c3].text)
                    else:
                        UTILS.ReportErrors(fieldName + " attribute did not match expected value: " + expectedValue + ", actual value: " + table[c3].text, record)
                elif record["action"] == "INPUT":
                    table[c1].click()
                    return
        
            
def ApplicationConfiguration(record):
    
    if CONFIG.driver.current_url.find("applications") == -1:
        UTILS.ReportErrors("Application Configurations page did not load", record)
        return

    for attribute in record["fields"]:
        field_mappings = {"NAME": find_element_by_id("editForm:appName"),

        "OWNER": find_element_by_id("applicationPageOwner-inputEl"),
        "REVOKER": find_element_by_id("#applicationPageRevoker-inputEl"),
        "DESCRIPTION": find_element_by_xpath("//*[@id=appDescriptionHTMLCmp-iframeEl]//#document/body"),
        "APPLICATION TYPE": find_element_by_id("editForm:appType"),
        "PROXY APPLICATION": find_element_by_id("baseSuggest-1062-inputEl"),
        "AUTHORITATIVE APPLICATION": find_element_by_id(""),
        "CASE INSENSITIVE": find_element_by_id(""),
        "NATIVE CHANGE DETECTION": find_element_by_id("")
        }
        enum = field_mappings.get(record["screen"], lambda x: None)(record)


def BatchRequests(record):    
    if not CONFIG.driver.current_url.endswith("batchRequests.jsf"):
        CONFIG.driver.find_element_by_xpath("//select[@name='Manage']/option[text()='Batch Requests']").click()

    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1].upper()
        if fieldName == "OPTIONS":
            CONFIG.activeTab = "IDENTITY SEARCH"
            if expectedValue == "ADVANCED SEARCH": enum = CONFIG.driver.find_element_by_id("advancedIdentitySearchNavBtn-btnEl")
            if expectedValue == "IDENTITY SEARCH": enum = CONFIG.driver.find_element_by_id("identitySearchNavBtn-btnEl")
            if expectedValue == "RUN SEARCH": 
                enum = CONFIG.driver.find_element_by_id("preIdentitySearchBtn-btnEl")
            if expectedValue == "CLEAR SEARCH": 
                enum = CONFIG.driver.find_element_by_id("button-1300-btnEl")
                if enum:
                    enum.click()
                    enum = CONFIG.driver.find_element_by_id("identityClearBtn-btnEl")
        elif fieldName == "TABS":            
            CONFIG.activeTab = expectedValue
            if expectedValue == "IDENTITY SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1305-btnEl")
            elif expectedValue == "ACCESS REVIEW SEARCH": enum = CONFIG.driver.find_element_by_xpath("//*[@id='tab-1306-btnEl']")
            elif expectedValue == "ROLE SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1307-btnEl")
            elif expectedValue == "ACCOUNT GROUP SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1308-btnEl")
            elif expectedValue == "ACTIVITY SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1309-btnEl")
            elif expectedValue == "AUDIT SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1310-btnEl")
            elif expectedValue == "PROCESS METRICS SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1311-btnEl")
            elif expectedValue == "ACCESS REQUEST SEARCH": enum = CONFIG.driver.find_element_by_id("tab-1312-btnEl")
        else:    
            available_tabs = {
                "IDENTITY SEARCH": AdvancedAnalyticsIdentitySearch,
                "ACCESS REVIEW SEARCH": AdvancedAnalyticsAccessReviewSearch,
                "ROLE SEARCH": None,
                "ACCOUNT GROUP SEARCH": None,
                "ACTIVITY SEARCH": None,
                "AUDIT SEARCH": None,
                "PROCESS METRICS SEARCH": None,
                "ACCESS REQUEST SEARCH": None
                } 

            available_tabs.get(CONFIG.activeTab, lambda x: None)(record)
            break

        if record["action"] == "VERIFY":
            if enum.text.upper() == expectedValue:
                CONFIG.logger1.info(indent + fieldName + " attribute matched expected value: " + expectedValue)
            else:
                UTILS.ReportErrors("%s attribute(%s) did not match expected value: %s" %(fieldName, enum.text, expectedValue), record)
        elif record["action"] == "INPUT":
            time.sleep(2)
            enum.click()
    

def BusinessProcesses(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}

    UTILS.UseScreenObjects(MappedObjects, record)
    

def Certifications(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}
    
    UTILS.UseScreenObjects(MappedObjects, record)
    

def DashboardPage(record): 
    if CONFIG.driver.current_url.find("dashboard") == -1:
        enum = CONFIG.driver.find_element_by_link_text("Dashboard").click()
        CONFIG.logger1.info (indent + "Url change: " + CONFIG.driver.current_url)
        time.sleep(2)

    
def Define(record):  
    if CONFIG.driver.current_url.find("define") == -1:
        CONFIG.driver.find_element_by_link_text("Define").click()
        CONFIG.logger1.info (indent + "Url change: " + CONFIG.driver.current_url)
        CONFIG.logger1.info(indent + "Current Location: Define Screen")


def EntitlementCatalog(record):
    #placeholder
    MappedObjects = {
                   "ACTION": {
                                }}
                     
    
    UTILS.UseScreenObjects(MappedObjects, record)
    
    

def Groups(record):
    #placeholder
    MappedObjects = {
                   "ACTION": {
                                }}


    UTILS.UseScreenObjects(MappedObjects, record)
    
    
def Identities(record):
    UTILS.WaitForObject("XPATH", ".//*[@id='bodyDivFooter']/div/span[1]", record, 10)
    
    CONFIG.activeTab = ""
    if CONFIG.driver.current_url.find("identities") == -1: # if not on the Identities page
        ActionChains(CONFIG.driver).move_to_element(CONFIG.driver.find_element_by_xpath("//*[contains(@href,'/define/index.jsf')]")).perform()
        CONFIG.driver.find_element_by_xpath("//*[contains(@class, 'x-menu-item-text') and contains(text(), 'Identities')]").click()
        waitObject = UTILS.WaitForObject("XPATH", "//*[contains(@class, 'x-form-field x-form-empty-field x-form-text ') and contains(@name, 'searchfield-1029-inputEl')]", record, 10)
        CONFIG.logger1.info(indent + "Url change: " + CONFIG.driver.current_url)
        
        CONFIG.logger1.info(str(record["screen"]))
        if record["screen"] == "IDENTITIES":
            CONFIG.logger1.info(indent + "Current Location: Define > Identities Screen")
        time.sleep(3)
    
    c1, c3 = (0,)*2
    
    if record["fields"][c1].upper() == "SEARCH":
        CONFIG.logger1.info(record["values"][c1])
        
        if waitObject == -1: return
        else:
            CONFIG.driver.find_element_by_xpath(".//*[@id='searchfield-1029-inputEl']").send_keys(record["values"][c1])
            CONFIG.driver.find_element_by_xpath(".//*[@id='ext-gen1108']").click()

        
    else:
        table = CONFIG.driver.find_elements_by_xpath("//*[@class='x-grid-table x-grid-table-resizer']//tbody//tr//td/div")    

        for c1 in range(0, len(table)):
            if table[c1].text == record["values"][0]:
                for c2 in range(0, len(record["fields"])):
                    fieldName = record["fields"][c2]
                    bool = "False"
                    if fieldName == "USER NAME" and table[c1].text == record["values"][c2]:
                        c3 = c1
                        bool = "True"  
                    elif fieldName == "DISPLAY NAME FIRST" and table[c1+1].text == record["values"][c2]:
                        c3 = c1+1

                        bool = "True"            
                    elif fieldName == "DISPLAY NAME LAST"and table[c1+2].text == record["values"][c2]:
                        c3 = c1+2
                        bool = "True"   
                    elif fieldName == "MANAGER" and table[c1+3].text == record["values"][c2]:
                        c3 = c1+3
                        bool = "True"
                    if fieldName == "ASSIGNED ROLE SUMMARY" and table[c1+4].text == record["values"][c2]:
                        c3 = c1+4
                        bool = "True"    
                    elif fieldName == "DETECTED ROLE SUMMARY" and table[c1+5].text == record["values"][c2]:
                        c3 = c1+5

                        bool = "True"            
                    elif fieldName == "RISK SCORE"and table[c1+6].text == record["values"][c2]:
                        c3 = c1+6
                        bool = "True"   
                    elif fieldName == "LAST REFRESH" and table[c1+7].text == record["values"][c2]:
                        c3 = c1+7
                        bool = "True"

                    if record["action"] == "VERIFY":
                        if bool == "True":
                            CONFIG.logger1.info(indent + fieldName + " attribute matched expected value: " + table[c3].text)
                        else:
                            CONFIG.logger2.error(indent + fieldName + " attribute did not match expected value: " + record["values"][c2] + ", actual value: " + table[c3].text)
                    elif record["action"] == "INPUT":
                        table[c1].click()
                        if CONFIG.verboseReporting == True:
                            CONFIG.logger1.info(indent + record["fields"][c2] + ": " + record["values"][c2] + " selected")
                        return


def IdentityCorrelation(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}

    UTILS.UseScreenObjects(MappedObjects, record)
    

def IdentityRiskModel(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}

    UTILS.UseScreenObjects(MappedObjects, record)
    

def IdentityRiskScores(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}

    UTILS.UseScreenObjects(MappedObjects, record)
    

def LifecycleEvents(record):
    #placeholder
    MappedObjects = {
                     "ACTION": {
                                }}

    UTILS.UseScreenObjects(MappedObjects, record)
    

def Login(record):
    #user spadmin
    #pass admin
    
    if CONFIG.driver == "":
        UTILS.OpenBrowser(CONFIG.browser)
    
    if len(record["fields"]) != len(record["values"]):
        UTILS.ReportErrors("Record count mismatch between Fields and Expected Values.", record)
        if record["results"] == "T":
            sys.exit()
        else:
            return
    if CONFIG.driver.current_url != CONFIG.iiqURL:
        CONFIG.driver.get(CONFIG.iiqURL)
        
    UTILS.UseScreenObjects({"ACCOUNT ID": ".//*[@id='loginForm:accountId']",
                            "USERNAME": ".//*[@id='loginForm:accountId']",
                            "PASSWORD": ".//*[@id='loginForm:password']",
                            "ACTION": {"LOGIN": ".//*[@id='loginButtonDisplay']"}}, record)
    
    UTILS.WaitForObject("XPATH", ".//*[@id='bodyDivFooter']/div", record, 10)
    
    if not CONFIG.driver.current_url.endswith("dashboard.jsf"):
        UTILS.ReportErrors("Login unsuccessful.", record)

def MyAccessReviews(record):
    #placeholder
    pass


def Manage(record):
        
    if CONFIG.driver.current_url.find("manage") == -1:
        CONFIG.driver.find_element_by_link_text("Manage").click()
        CONFIG.logger1.info (indent + "Url change: " + CONFIG.driver.current_url)


def Monitor(record):
        
    if CONFIG.driver.current_url.find("monitor") == -1:
        CONFIG.driver.find_element_by_link_text("Monitor").click()
        CONFIG.logger1.info (indent + "Url change: " + CONFIG.driver.current_url)


def Policies(record):
    #placeholder
    pass
    

def PolicyViolations(record):
   #placeholder
    pass

    
def RefreshIdentity(record):
    if not CONFIG.driver.current_url.endswith("chooseIdentities.jsf"):
        enum = CONFIG.driver.find_element_by_link_text("Dashboard").click()
        time.sleep(3)
        enum = CONFIG.driver.find_element_by_link_text("Refresh Identity(s)").click()
        time.sleep(2)
    MappedObjects = {"SEARCH": ".//*[@id='AIDSF-inputEl']",
                     "SELECT": ".//*[@id='gridview-1050']/table/tbody/tr/td/div",
                     "ACTION": {"FIND": ".//*[@id='ext-gen1173']",
                                "ADVANCED SEARCH": ".//*[@id='button-1053-btnEl']",
                                "SUBMIT": ".//*[@id='editForm']/table/tbody/tr/td[2]/div/div/div[4]/input[1]",
                                "CANCEL": ".//*[@id='editForm']/table/tbody/tr/td[2]/div/div/div[4]/input[2]"}}

    UTILS.UseScreenObjects(MappedObjects, record)

        
def Reports(record):
    #placeholder
    pass


def Roles(record):
    #placeholder
    pass


def SystemSetup(record):
        
    if CONFIG.driver.current_url.find("systemSetup") == -1:
        enum = CONFIG.driver.find_element_by_link_text("System Setup").click()
        CONFIG.logger1.info (indent + "Url change: " + CONFIG.driver.current_url)
    

def Tasks(record):
    #placeholder
    pass
    
 
def ViewIdentity(record):
    if CONFIG.activeTab == "": CONFIG.activeTab = "ATTRIBUTES"
    if CONFIG.driver.current_url.find("identity.jsf") == -1: # if not on the Identity screen
        UTILS.ReportErrors("The View Identity page did not load properly", record)
        exit()
    
    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1].upper()
        if fieldName == "TABS":
            CONFIG.activeTab = expectedValue
            if expectedValue == "ATTRIBUTES": enum = CONFIG.driver.find_element_by_id("tab-1025-btnEl")
            elif expectedValue == "ENTITLEMENTS": enum = CONFIG.driver.find_element_by_id("tab-1026-btnEl")
            elif expectedValue == "APPLICATION ACCOUNTS": enum = CONFIG.driver.find_element_by_id("tab-1027-btnEl")
            elif expectedValue == "POLICY": enum = CONFIG.driver.find_element_by_id("tab-1028-btnEl")
            elif expectedValue == "HISTORY": enum = CONFIG.driver.find_element_by_id("tab-1029-btnEl")
            elif expectedValue == "RISK": enum = CONFIG.driver.find_element_by_id("tab-1030-btnEl")
            elif expectedValue == "ACTIVITY": enum = CONFIG.driver.find_element_by_id("tab-1031-btnEl")
            elif expectedValue == "USER RIGHTS": enum = CONFIG.driver.find_element_by_id("tab-1031-btnEl")
            elif expectedValue == "EVENTS": enum = CONFIG.driver.find_element_by_id("tab-1032-btnEl")
            if record["action"] == "VERIFY":
                if enum.text.upper() == expectedValue:
                    CONFIG.logger1.info(indent + fieldName + " attribute matched expected value: " + expectedValue)
                else:
                    UTILS.ReportErrors("%s attribute(%s) did not match expected value: %s" %(fieldName, enum.text, expectedValue), record)
            elif record["action"] == "INPUT":
                time.sleep(2)
                enum.click()
                CONFIG.logger1.info(indent + "Tab set to " + expectedValue.title())
    
        else:
            available_tabs = {
                "ATTRIBUTES": ViewIdentityAttributes,
                "ENTITLEMENTS": ViewIdentityEntitlements,
                "APPLICATION ACCOUNTS": ViewIdentityApplicationAccounts,
                "POLICY": ViewIdentityPolicy,
                "HISTORY": ViewIdentityHistory,
                "RISK": ViewIdentityRisk,
                "ACTIVITY": ViewIdentityActivity,
                "USER RIGHTS": ViewIdentityUser,
                "EVENTS": ViewIdentityEvents,
                } 
            available_tabs.get(CONFIG.activeTab, lambda x: None)(record)
            break
    return    


def ViewIdentityAttributes(record):
    CONFIG.activeTab = "ATTRIBUTES"
    
    MappedObjects = {"UUID": "//*[@class='spTable']//tbody//tr[1]//td[2]",
                     "EPPN": "//*[@class='spTable']//tbody//tr[2]//td[2]",
                     "HUID": "//*[@class='spTable']//tbody//tr[3]//td[2]",
                     "CARD REISSUE DIGIT": "//*[@class='spTable']//tbody//tr[4]//td[2]",
                     "ADID": "//*[@class='spTable']//tbody//tr[5]//td[2]",                    
                     "DISPLAY NAME": "//*[@class='spTable']//tbody//tr[8]//td[2]", 
                     "DISPLAY NAME PREFIX": "//*[@class='spTable']//tbody//tr[9]//td[2]",
                     "DISPLAY NAME FIRST": "//*[@class='spTable']//tbody//tr[6]//td[2]",
                     "DISPLAY NAME MIDDLE": "//*[@class='spTable']//tbody//tr[10]//td[2]",           
                     "DISPLAY NAME LAST": "//*[@class='spTable']//tbody//tr[7]//td[2]", 
                     "DISPLAY NAME SUFFIX": "//*[@class='spTable']//tbody//tr[11]//td[2]",
                     "COMMUNITIES": "//*[@class='spTable']//tbody//tr[12]//td[2]",
                     "COMMUNITY END DATES": "//*[@class='spTable']//tbody//tr[13]//td[2]",
                     "USERNAME": "//*[@class='spTable']//tbody//tr[14]//td[2]",
                     "UPN": "//*[@class='spTable']//tbody//tr[15]//td[2]",
                     "FASAD UPN": "//*[@class='spTable']//tbody//tr[16]//td[2]",
                     "FAS USER NAME": "//*[@class='spTable']//tbody//tr[17]//td[2]",
                     "LOGIN NAME": [".//tr[18]/td[2]/span", ".//tr[18]/td[2]/*[@type='text']"],
                     "CLAIM STATUS": [".//tr[19]/td[2]/span", ".//tr[19]/td[2]/*[@type='text']"],
                     "RECOVERY PRIMARY EMAIL": [".//tr[20]/td[2]/span", ".//tr[20]/td[2]/*[@type='text']"],
                     "RECOVERY ALTERNATE EMAIL": [".//tr[21]/td[2]/span", ".//tr[21]/td[2]/*[@type='text']"],
                     "RECOVERY PRIMARY PHONE": [".//tr[22]/td[2]/span", ".//tr[22]/td[2]/*[@type='text']"],
                     "RECOVERY ALTERNATE PHONE": [".//tr[23]/td[2]/span", ".//tr[23]/td[2]/*[@type='text']"],
                     "ONBOARDING EMAIL": "//*[@class='spTable']//tbody//tr[24]//td[2]",
                     "PERSON PRIVACY": "//*[@class='spTable']//tbody//tr[25]//td[2]",
                     "DOB MONTH": "//*[@class='spTable']//tbody//tr[26]//td[2]",
                     "DOB DAY": "//*[@class='spTable']//tbody//tr[27]//td[2]",
                     "DOB YEAR": "//*[@class='spTable']//tbody//tr[28]//td[2]",
                     "OFFICIAL NAME": "//*[@class='spTable']//tbody//tr[29]//td[2]",
                     "OFFICIAL NAME PREFIX": "//*[@class='spTable']//tbody//tr[30]//td[2]", 
                     "OFFICIAL NAME FIRST": "//*[@class='spTable']//tbody//tr[31]//td[2]",
                     "OFFICIAL NAME MIDDLE": "//*[@class='spTable']//tbody//tr[32]//td[2]",           
                     "OFFICIAL NAME LAST": "//*[@class='spTable']//tbody//tr[33]//td[2]", 
                     "OFFICIAL NAME SUFFIX": "//*[@class='spTable']//tbody//tr[34]//td[2]",
                     "OFFICIAL EMAIL ADDRESS": "//*[@class='spTable']//tbody//tr[35]//td[2]",
                     "BUILDING LOCATION": "//*[@class='spTable']//tbody//tr[36]//td[2]",
                     "BUILDING LOCATION DESC": "//*[@class='spTable']//tbody//tr[37]//td[2]",
                     "BUILDING LOCATION PRIVACY": "//*[@class='spTable']//tbody//tr[38]//td[2]",           
                     "PERSONA NON GRATA": "//*[@class='spTable']//tbody//tr[39]//td[2]",
                     "EMAIL ADDRESS": "//*[@class='spTable']//tbody//tr[40]//td[2]",
                     "EMAIL PRIVACY": "//*[@class='spTable']//tbody//tr[41]//td[2]",
                     "OFFICE PHONE NUMBER": "//*[@class='spTable']//tbody//tr[42]//td[2]",
                     "OFFICE PHONE EXTENSION": "//*[@class='spTable']//tbody//tr[43]//td[2]",
                     "OFFICE PHONE PRIVACY": "//*[@class='spTable']//tbody//tr[44]//td[2]",           
                     "MOBILE PHONE NUMBER": "//*[@class='spTable']//tbody//tr[45]//td[2]", 
                     "MOBILE PHONE PRIVACY": "//*[@class='spTable']//tbody//tr[46]//td[2]",     
                     "OFFICE FAX NUMBER": "//*[@class='spTable']//tbody//tr[47]//td[2]",
                     "OFFICE FAX PRIVACY": "//*[@class='spTable']//tbody//tr[48]//td[2]",
                     "ADDRESS PRIVACY VALUE": "//*[@class='spTable']//tbody//tr[49]//td[2]", 
                     "ADDRESS LINE 1": "//*[@class='spTable']//tbody//tr[50]//td[2]",
                     "ADDRESS LINE 2": "//*[@class='spTable']//tbody//tr[51]//td[2]",
                     "ADDRESS LINE 3": "//*[@class='spTable']//tbody//tr[52]//td[2]",           
                     "CITY": "//*[@class='spTable']//tbody//tr[53]//td[2]", 
                     "STATE": "//*[@class='spTable']//tbody//tr[54]//td[2]",
                     "ZIP CODE": "//*[@class='spTable']//tbody//tr[55]//td[2]",
                     "FERPA STATUS": "//*[@class='spTable']//tbody//tr[56]//td[2]", 
                     "FERPA PAST STUDENT": "//*[@class='spTable']//tbody//tr[57]//td[2]",
                     "POP UNIQUE ROLE ID": "//*[@class='spTable']//tbody//tr[58]//td[2]",     
                     "POP ROLE ID": "//*[@class='spTable']//tbody//tr[59]//td[2]",
                     "POP ROLE TYPE": "//*[@class='spTable']//tbody//tr[60]//td[2]",
                     "POP FACULTY CODE": "//*[@class='spTable']//tbody//tr[61]//td[2]", 
                     "POP FACULTY CODE DESC": "//*[@class='spTable']//tbody//tr[62]//td[2]",
                     "POP ROLE SOURCE": "//*[@class='spTable']//tbody//tr[63]//td[2]",
                     "POP ROLE TITLE": "//*[@class='spTable']//tbody//tr[64]//td[2]",                   
                     "POP ROLE START DATE": "//*[@class='spTable']//tbody//tr[65]//td[2]", 
                     "POP ROLE END DATE": "//*[@class='spTable']//tbody//tr[66]//td[2]",
                     "POP PRIVACY VALUE": "//*[@class='spTable']//tbody//tr[67]//td[2]",
                     "POP DEPARTMENT": "//*[@class='spTable']//tbody//tr[68]//td[2]", 
                     "POP DEPARTMENT DESC": "//*[@class='spTable']//tbody//tr[69]//td[2]",

                     "PROVINCE": "//*[@class='spTable']//tbody//tr[49]//td[2]", 

                     "ALUMNI PREFERRED EMAIL": "//*[@class='spTable']//tbody//tr[70]//td[2]",
                     "ADVANCE ID": "//*[@class='spTable']//tbody//tr[71]//td[2]",           
                     "NEED REFRESH": "//*[@class='spTable']//tbody//tr[72]//td[2]", 
                     "ACTION": {"CHANGE PASSWORD": "link_text:Change Password",
                                "CHANGE FORWARDING USER": "link_text:Change Forwarding User",   
                                "EDIT": ".//*[@id='editForm:attributeContentA4J']/table/tbody/tr[1]/td/span/span",
                                "SAVE": ".//*[@id='editForm:saveButton']",
                                "CANCEL": ".//*[@id='editForm:cancelButton']"}}

    UTILS.UseScreenObjects(MappedObjects, record)
      

def ViewIdentityEntitlements(record):
    CONFIG.logger1.info()


def ViewIdentityApplicationAccounts(record):
    CONFIG.logger1.info(indent+ "Current location: Define > Identities > Application Accounts tab")
    global counter0
    for c1 in range(0, len(record["fields"])):
        fieldName = record["fields"][c1].upper()
        expectedValue = record["values"][c1].upper()
        if fieldName == "APPLICATION":
            #time.sleep(2)
            enumList = CONFIG.driver.find_elements_by_xpath("//*[@id='appAcctTbl']/tbody/tr")
            
            CONFIG.counter0 = 1
            for enum in enumList:
                enumValue = enum.text.split(" ")                
                if enumValue[0].upper() == expectedValue:
                    enum = "//*[@id='appAcctTbl']/tbody/tr" + str(CONFIG.counter0) + "/td[2]/a"
                    enum = CONFIG.driver.find_element_by_xpath("//*[@id='appAcctTbl']/tbody/tr[" + str(CONFIG.counter0) + "]/td[2]/a")
                    break
                CONFIG.counter0 += 1  
        elif fieldName == "ACCOUNT NAME": 
            enum = CONFIG.driver.find_element_by_xpath("//*[@id='appAcctTbl']/tbody/tr[" + str(CONFIG.counter0) + "]/td[3]")
        elif fieldName == "STATUS": 
            enum = CONFIG.driver.find_element_by_xpath("//*[@id='appAcctTbl']/tbody/tr[" + str(CONFIG.counter0) + "]/td[5]")
        elif fieldName == "LAST REFRESH": 
            enum = CONFIG.driver.find_element_by_xpath("//*[@id='appAcctTbl']/tbody/tr[" + str(CONFIG.counter0) + "]/td[6]")
        elif fieldName == "ACTIONS":
            if expectedValue == "DELETE": enum = CONFIG.driver.find_element_by_id("editForm:linkDeleteButton")
            elif expectedValue == "MOVE ACCOUNT": enum = CONFIG.driver.find_element_by_xpath("//*[@id='appAcctTbl']//tbody//tr[6]/td/div/input[2]")  
            elif expectedValue == "SAVE": enum = CONFIG.driver.find_element_by_id("")  
            elif expectedValue == "CANCEL": enum = CONFIG.driver.find_element_by_id("")  
        else:
            enumList = CONFIG.driver.find_elements_by_xpath("//*[@id='appAcctTbl']/tbody/tr[" + str(CONFIG.counter0+1) + "]/td/div[2]/div[2]/table/tbody/tr")
            for CONFIG.counter1 in range(1,len(enumList)+1):
                enum = CONFIG.driver.find_element_by_xpath("//*[@id='appAcctTbl']/tbody/tr[" + str(CONFIG.counter0+1) + "]/td/div[2]/div[2]/table/tbody/tr[" + str(CONFIG.counter1) + "]/td[1]")
                if enum.text.upper() == fieldName:
                    enum = CONFIG.driver.find_element_by_xpath("//*[@id='appAcctTbl']/tbody/tr[" + str(CONFIG.counter0+1) + "]/td/div[2]/div[2]/table/tbody/tr[" + str(CONFIG.counter1) + "]/td[2]")
                    break
        
        if record["action"] == "VERIFY":
            actualValue = enum.text.replace("\n", ";").upper()
            if actualValue == expectedValue:
                CONFIG.logger1.info( "%s%s attribute matched expected value: %s" %(indent, fieldName, actualValue))
            else:
                UTILS.ReportErrors("'%s' attribute (%s) did not match expected value: %s" %(fieldName, actualValue, expectedValue), record)
            enum = ""
        elif record["action"] == "INPUT":
            time.sleep(2)
            enum.click()
            CONFIG.logger1.info(indent + "Selected %s: %s" %(fieldName, expectedValue))
    return    
    
    
def ViewIdentityPolicy(record):
    CONFIG.logger1.info()


def ViewIdentityHistory(record):
    CONFIG.logger1.info()


def ViewIdentityRisk(record):
    CONFIG.logger1.info()


def ViewIdentityActivity(record):
    CONFIG.logger1.info()


def ViewIdentityUser(record):
    CONFIG.logger1.info()


def ViewIdentityEvents(record):
    CONFIG.logger1.info()


def WorkItems(record):
    #placeholder
    pass