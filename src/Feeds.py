#---------- IMPORTS ----------
from datetime import datetime
from getpass import getuser
import os
import re
import sys
import time
import traceback
from subprocess import Popen, PIPE, STDOUT, check_output

#2013-02-13T02:57:01-05:00

#OPERATION
    #ADD
    #UPDATE
    #DELETE

#FEED TYPE
    #DCESTU
        #/u03/ftp/dce/idm/inbox
    #SISSTU
        #/u03/ftp/sis/idm/inbox
    #PS_EMP
        #/u03/ftp/psoft/idm/inbox
    #CFADEF
        #/u03/ftp/cfa/idm/inbox
    
    
#ATTRIBUTES
    
#ACTION

#HUID

FeedText = '<?xml version="1.0" encoding="ISO-8859-1" ?>\n<xml xmlns="http://xml.cadm.harvard.edu/HUPeopleData" xmlns:pd="http://xml.cadm.harvard.edu/HUPeopleData" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://xml.cadm.harvard.edu/HUPeopleData http://xml.cadm.harvard.edu/idm-xml/HUPeopleData_1.1.0.xsd">'
timeNow = str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))
#timeNow = str(datetime.now()).replace(" ", "T")
print(timeNow)


#* Address Category Code
AddressText = '<pd:address univId="requiredHUID" action="requiredAction"> \
<pd:addressCategory>requiredAddressCategory</pd:addressCategory> \
<pd:address1/> \
<pd:address2/> \
<pd:address3/> \
<pd:addressId>requiredAddressID</pd:addressId> \
<pd:addressMailRealm/> \
<pd:addressMailRealmDescription /> \
<pd:addressSource>requiredAddressSource</pd:addressSource> \
<pd:city>requiredCity</pd:city> \
<pd:addressISOCountryCode>requiredCountryCode</pd:addressISOCountryCode> \
<pd:effectiveDate>requiredDateTime</pd:effectiveDate> \
<pd:effectiveStatus>A</pd:effectiveStatus> \
<pd:location /> \
<pd:postal/> \
<pd:privacyValue /> \
<pd:state>MI</pd:state> \
<pd:updateBy>requiredFeedType</pd:updateBy> \
<pd:updateDate>requiredDateTime</pd:updateDate>  \
<pd:updateSource>requiredFeedType</pd:updateSource> \
</pd:address> '

#Address 1
    #<pd:address1 />
#Address 2
    #<pd:address2 />
#Address 3
    #<pd:address3 />
#* Address ID
    #requiredAddressID
#Address Mail Realm Code
    #<pd:addressMailRealm/>
#Address Source
    #requiredAddressSource
#City
    #requiredCity
#Country
    #requiredCountryCode
#* Effective Date
#Postal Code / ZIP
#State
#Update By
#Update Date
#Update Source
#* Effective Date
#* Listing Category
#* Listing ID
#Update By
#Update Date
#Update Source
#Contact Data Extension
#* Contact Data Subtype
#* Contact Data Type
#Contact Data
#* Effective Date
#* Listing Category
#* Listing ID
#Update By
#Update Date
#Update Source
#* Effective Date
#* Email Address Source
#Email Address Type
#* Email Domain Name
#* Email User Name
#Update By
#Update Date
#Update Source
#* Effective Date
#Name, First
#Name, Last
#Name, Middle
#Name Type
#Update By
#Update Date
#Update Source
#Birth Date
#* Effective Date
#Gender
#National ID (SSN)
#Update By
#Update Date
#Update Source
#* Ethnicity Code
#Board House
#Boarding Status
#Degree Program
#* Effective Date
#Graduation Date (expected or past)
#Last Attendance Date
#Residence House
#Role End Date
#* Role ID
#* Role Source
#Role Start Date
#* Role Type
#School Code
#Special Program Code
#Student Status
#Time Status
#Update By
#Update Date
#Update Source



