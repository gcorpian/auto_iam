#---------- IMPORTS ----------
import poplib
import time
import sys
import os

#------- CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
os.environ['ORACLE_HOME'] = "/opt/oracle/instantclient"
os.environ['DYLD_LIBRARY_PATH'] = "/opt/oracle/instantclient"


#---------- FUNCTIONS ----------
def MappingTable(record):
    #converts the screen value from the spreadsheet into a function that controls how interactions take place.
    available_mappings = {
        "XID HOME": xidHome,
        "MAIN MENU": mainMenu,
        "REGISTER NEW XID": Register,
        "EDIT XID": EditXID,
        "CREATE NEW XID": CreateNewXID,
        "LOOKUP XID ACCOUNT": LookupXIDAccount,
        "XID ACCOUNT SEARCH": XIDAccountSearch,
        "XID ACCOUNT SUMMARY": XIDAccountSummary,
        "VIEW MANAGED XID ACCOUNTS": ViewManagedXIDAccounts,
        "CREATE BATCH XID ACCOUNTS": BatchXIDACcountCreation,
        "SET PASSWORD": SetPassword,
        "CHANGE PASSWORD": ChangePassword,
        "XID SYSTEM USERS": XIDSystemUsers,
        "XID USER INFO": XIDUserInfo,
        "REPLACE XID MANAGER": ReplaceXIDManager,
        "PROCESS EMAIL": ProcessEmail
        } 

    MappedObjects = available_mappings.get(record["screen"].strip(), lambda x: None)(record)
    
    if record["screen"].strip().upper() != "PROCESS EMAIL" and record["values"][0].upper() != "ADMINISTRATOR":
        UTILS.UseScreenObjects(MappedObjects, record)

        try:
            enum = CONFIG.driver.find_element_by_class_name("errorLabel")
            if enum.is_displayed and enum.is_enabled and enum.text != "" and record["action"] == "INPUT":
                if record["results"] in ["N", "F"]:
                    CONFIG.logger1.info (indent + "Alertbox error '%s' appeared as expected." % (enum.text))
                else:
                    if enum.text.find("updated successfully") < 0 and enum.text.find("Congratulations") < 0 and enum.text.find("created successfully") < 0:
                        UTILS.ReportErrors(enum.text, record)
        except:
            pass
        

def xidHome(record):
    if CONFIG.driver == "":
        CONFIG.driver = UTILS.OpenBrowser(CONFIG.browser)
    if record["values"][0].upper() == "ADMINISTRATOR":
        CONFIG.driver.get(CONFIG.XIDAdminURL)
    else:    
        if CONFIG.driver.current_url != CONFIG.XIDURL:
            CONFIG.driver.get(CONFIG.XIDURL)

    return {
            "ACTION": {"EXISTING OR NEW": ".//tr[2]/td[1]/div/a/img",
                       "ACCOUNT MANAGER": ".//tr[3]/td[1]/div/a/img",
                       "ADMINISTRATOR": ".//tr[3]/td[1]/div/a/img"}}
                       
def mainMenu(record):
    return {
            "ACTION": {"REGISTER": ".//tr[2]/td[1]/div/a/img",
                       "EDIT ACCOUNT": ".//tr[3]/td[1]/div/a/img",
                       "CHANGE PASSWORD": ".//tr[4]/td[1]/div/a/img",
                       "CREATE NEW INDIVIDUAL XID ACCOUNTS": ".//tr[2]/td[1]/div/a/img",
                       "LOOKUP AN XID ACCOUNT": ".//tr[3]/td[1]/div/a/img",
                       "VIEW ALL YOUR MANAGED XID ACCOUNTS": ".//tr[4]/td[1]/div/a/img",
                       "CREATE A BATCH OF NEW XID ACCOUNTS": ".//tr[5]/td[1]/div/a/img",
                       "DOWNLOAD ALL YOUR MANAGED XID ACCOUNT DATA": ".//tr[6]/td[1]/div/a/img",
                       "CREATE AUTHORIZED USER": ".//tr[1]/td[1]/div/a/img",
                       "VIEW AUTHORIZED USERS": ".//tr[2]/td[1]/div/a/img",
                       "REPLACE XID MANAGER": ".//tr[3]/td[1]/div/a/img",
                       "XID HOME": "link_text:XID Home",
                       "LOGOUT": "link_text:Logout",
                       "ACTION": {"XID HOME": "link_text:XID Home",
                                  "LOGOUT": "link_text:Logout",}}}

def LookupXIDAccount(record):
    return {"LOGIN": ".//tr[1]/td[2]/input",
            "EMAIL": ".//tr[2]/td[2]/input",
            "XID": ".//tr[4]/td[2]/input",
            "ACTION": {"LOGIN SEARCH": ".//tr[3]/td[1]/div/input",
                       "EMAIL SEARCH": ".//table[2]/tbody/tr[2]/td[2]/div/input[1]",
                       "XID SEARCH": "",
                       "LOGOUT": "",
                       "RESET": "/table[2]/tbody/tr[2]/td[2]/div/input[2]",
                       "MAIN MENU": "link_text:Main Menu"}}


def BatchXIDACcountCreation(record):
    return {"TEXT": ".//table/tbody/tr[2]/td/textarea",
            "ACTION": {"BROWSE": ".//table/tbody/tr[5]/td/input",
                       "SUBMIT": ".//table/tbody/tr[2]/td[2]/div/input[1]",
                       "RESET": ".//table/tbody/tr[2]/td[2]/div/input[2]",
                       "LOGOUT": "link_text:Logout",
                       "MAIN MENU": "link_text:Main Menu"}}
    

def XIDAccountSummary(record):  
    UTILS.WaitForObject("XPATH", ".//form/table/tbody/tr[3]/td/div/table[2]/tbody/tr[2]/td/div/input", record, 20)
    
    return {"XID": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[1]/td[2]",
            "LOGIN ID": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[2]/td[2]",
            "EMAIL ADDRESS": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[3]/td[2]",
            "PREFIX": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[4]/td[2]",
            "FIRST NAME": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[5]/td[2]]",
            "MIDDLE NAME": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[6]/td[2]]",
            "LAST NAME": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[7]/td[2]",
            "SUFFIX": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[8]/td[2]",
            "EXPIRATION DATE": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[9]/td[2]",
            "CREATION DATE": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[10]/td[2]",
            "MANAGER EMAIL": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[11]/td[2]",
            "GROUP ID": ".//form/table/tbody/tr[3]/td/div/table[1]/tbody/tr[12]/td[2]",
            "ACTION": {"SEARCH AGAIN": ".//table[2]/tbody/tr[2]/td/div/input",
                       "LOGOUT": "link_text:Logout",
                       "MAIN MENU": "link_text:Main Menu"}}
                       
def Register(record):
    return {"EMAIL ADDRESS": ".//tr[1]/td[2]/input",

            "EMAIL ADDRESS CONFIRMATION": ".//tr[2]/td[2]/input",
            "LOGIN ID": ".//tr[4]/td[2]/input",
            "PREFIX": ".//tr[5]/td[2]/select",
            "FIRST NAME": ".//tr[6]/td[2]/input",
            "MIDDLE NAME": ".//tr[7]/td[2]/input",
            "LAST NAME": ".//tr[8]/td[2]/input",
            "SUFFIX": ".//tr[9]/td[2]/input",
            "ERRORS": ".//td[@class='errorLabel']",
            "ACTION": {"USE EMAIL AS LOGIN ID": ".//tr[3]/td[1]/div/input",
                       "REGISTER": ".//table[2]/tbody/tr[2]/td[2]/div/input[1]",
                       "RESET": "/table[2]/tbody/tr[2]/td[2]/div/input[2]",
                       "MAIN MENU": "link_text:Main Menu"}}
                
def EditXID(record):
    UTILS.WaitForObject("XPATH", ".//tr[3]/td/div/table[2]/tbody/tr[2]/td[2]/div/input[1]", record, 10)
    
    return {"EMAIL ADDRESS": ".//tr[1]/td[2]/input",
            "LOGIN ID": ".//tr[2]/td[2]/input",
            "PREFIX": ".//tr[3]/td[2]/input",
            "FIRST NAME": ".//tr[4]/td[2]/input",
            "MIDDLE NAME": ".//tr[5]/td[2]/input",
            "LAST NAME": ".//tr[6]/td[2]/input",
            "SUFFIX": ".//tr[7]/td[2]/input",
            "CREATION DATE": ".//tr[8]/td[2]",
            "ACTION": {"UPDATE": ".//tr[3]/td/div/table[2]/tbody/tr[2]/td[2]/div/input[1]",
                       "RESET": ".//tr[3]/td/div/table[2]/tbody/tr[2]/td[2]/div/input[2]",
                       "MAIN MENU": "link_text:Main Menu",
                       "LOGOUT": "link_text:Logout"}}


def SetPassword(record):
    return {"PERSONAL CHALLENGE": ".//table[1]/tbody/tr[3]/td/input",
            "RESPONSE": ".//table[1]/tbody/tr[5]/td/input",
            "CONFIRM RESPONSE": ".//table[1]/tbody/tr[7]/td/input",
            "NEW PASSWORD": ".//table/tbody/tr[1]/td[2]/input",
            "CONFIRM PASSWORD": ".//table/tbody/tr[2]/td[2]/input",
            "ERRORS": ".//td[@class='errorLabel']",
            "ACTION": {"SUBMIT": ".//tr[2]/td[2]/div/input[1]",
                       "RESET": ".//tr[2]/td[2]/div/input[2]",
                       "MAIN MENU": "link_text:Main Menu"}}  
                       
def ChangePassword(record):
    return {"LOGIN ID": ".//tr[2]/td[2]/input",
            "CHALLENGE": ".//tr[3]/td/div/table[1]/tbody/tr[2]/td",
            "RESPONSE": ".//tr[5]/td/input",
            "NEW PASSWORD": ".//tr[1]/td[2]/input",
            "CONFIRM PASSWORD": ".//tr[2]/td[2]/input",
            "ERRORS": ".//td[@class='errorLabel']",
            "MESSAGES": ".//td[@class='errorLabel']",
            "ACTION": {"SUBMIT": ".//*[@name='submit_btn']",
                       "MAIN MENU": "link_text:Main Menu"}}
                       
def CreateNewXID(record):
    return {"GROUP ID": "",
            "XID": "",
            "LOGIN": "",
            "NAME": "",
            "EMAIL ADDRESS": ".//table/tbody/tr[1]/td/input",
            "EMAIL ADDRESS CONFIRMATION": ".//table/tbody/tr[2]/td[2]/input",
            "USE EMAIL AS LOGIN ID": ".//table[1]/tbody/tr[3]/td[1]/div/input",
            "LOGIN ID": ".//table/tbody/tr[4]/td/input",
            "PREFIX": ".//table[1]/tbody/tr[5]/td[2]/select",
            "FIRST NAME": ".//table/tbody/tr[6]/td[2]/input",
            "MIDDLE NAME": ".//table/tbody/tr[7]/td[2]/input",
            "LAST NAME": ".//table/tbody/tr[8]/td[2]/input",
            "SUFFIX": ".//table/tbody/tr[9]/td[2]/input",
            "ERRORS": ".//td[@class='errorLabel']",
            "MESSAGES": ".//td[@class='errorLabel']",
            #"EXPIRATION DATE": ".//table/tbody/tr[7]/td[2]/input",
            #"GROUP ID": ".//table/tbody/tr[8]/td[2]/input",
            "ACTION": {"END SESSION": ".//tr[1]/td[2]/div/input",
                       "MANAGE ACCOUNT": ".//tr[2]/td[2]/div/input[1]",
                       "REGISTER": ".//tr[2]/td[2]/div/input[1]",
                       "RESET": ".//tr[2]/td[2]/div/input[2]",
                       "LOGOUT": "link_text:Logout"}}

def XIDAccountSearch(record):
   return {"LOGIN": ".//table/tbody/tr[1]/td[2]/input",
           "EMAIL": ".//table/tbody/tr[2]/td[2]/input",
           "XID": ".//table/tbody/tr[3]/td[2]/input",
           "MESSAGE": ".//td[@class='errorLabel']",
           "ACTION": {"LOGIN SEARCH": ".//tr[1]/td[3]/input",
                       "EMAIL SEARCH": ".//tr[2]/td[3]/input",
                       "XID SEARCH": ".//tr[3]/td[3]/input",
                       "RESET": ".//tr[2]/td/div/input"}}
                       
def ManagedXIDAccounts(record):
    return {"GROUP ID": "",
            "XID": "",
            "NAME": "",
            "LOGIN": "",
            "EMAIL": "",
            "ACTIVATED": "",
            "EDIT": "",
            "ACTION": {"XID HOME": "link_text:Main Menu",
                                  "LOGOUT": "link_text:Logout",}}

def ViewManagedXIDAccounts(record):
    pass
                                  

def ProcessEmail(record):
    time.sleep(30)

    # Set up the connection to the POP server 
    pop_conn = poplib.POP3_SSL('pop.gmail.com')
    pop_conn.user(CONFIG.mail_username)
    pop_conn.pass_(CONFIG.mail_password)

    poplist = pop_conn.list()
    # Iterate through the messages 
    for i in range(len(pop_conn.list()[1])): 
        message = ""
        for j in pop_conn.retr(i+1)[1]: 
            message += str(j).replace("b'", "").replace("'", "").replace("  ", "").replace("m_506676278511645089", "").replace("=3D", "").replace("<wbr>", "")
        #LOGIN NAME email
        if record["fields"][0].upper() == "XID":
            index = message.find("Your login id: ")
            if index > -1:
                CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
                message = message[index:]
                LoginName = message[message.find('Your login id: ') + len('Your login id: '):message.find('<br>')]
                SecretKey = message[message.find('secret key: ') + len('secret key: '):message.find('<br><br>')]
                URLLink = message[message.find('link: <a href=') + len('link: <a href='):message.find('>Activate XID')]
                
                if record["values"][0] == "":
                    CONFIG.logger1.info (indent + "Login Name is '" + LoginName + "'")
                    CONFIG.logger1.info (indent + "Secret Key is '" + SecretKey + "'")
                elif LoginName == record["values"][0]:
                    CONFIG.logger1.info (indent + "Login Name matched expected value: '" + LoginName + "'")    
                else:
                    UTILS.ReportErrors("Login Name did not match expected value.  Expected: '" + record["values"][0] + "    Actual: " + LoginName + "'", record)
                
                
                CONFIG.driver.get(URLLink)
                time.sleep(10)
                
                CONFIG.driver.find_element_by_xpath(".//table[1]/tbody/tr[5]/td/input").send_keys(SecretKey)
                #result = UTILS.PerformAction(enum, "Response", SecretKey, record)
                CONFIG.driver.find_element_by_xpath(".//tr[2]/td/div/input").click()

                print("Done checking e-mail account")
                break
        print(i)
        pop_conn.dele(i)
    pop_conn.quit()

def XIDUserInfo(record):
    #Create an Authorized User
    return {"USER ID": ".//tr[2]/td[2]/input",
            "EMAIL ADDRESS": ".//tr[3]/td[2]/input",
            "USER ROLE": ".//tr[4]/td[2]/select",
            "ACTION": {"CREATE USER":".//tr[2]/td[2]/div/input[1]",
                       "RESET FORM":".//tr[2]/td[2]/div/input[2]",
                       "REFRESH USER INFO": ".//tr[2]/td[2]/div/input[2]",
                       "XID HOME":"link_text:Main Menu",
                       "LOGOUT": "link_text:Logout"}}

def XIDSystemUsers(record):
    # create function to locate data in the table.
    
    return {"USER ID": ".//tr[2]/td[2]/input",
            "EMAIL ADDRESS": ".//tr[3]/td[2]/input",
            "ROLE": ".//tr[4]/td[2]/select",
            "ACTION": {"EDIT":".//tr[2]/td[2]/div/input[1]",
                       "MAIN MENU": "link_text:Main Menu",
                       "CREATE NEW USER": "link_text:Create New User",
                       "LOGOUT": "link_text:Logout"}}
                       
def ReplaceXIDManager(record):
    return {"USER ID": ".//tr[2]/td[2]/input",
            "EMAIL ADDRESS": ".//tr[3]/td[2]/input",
            "USER ROLE": ".//tr[4]/td[2]/select",
            "ACTION": {"CREATE USER": ".//tr[2]/td[2]/div/input[1]",
                       "RESET FORM": ".//tr[2]/td[2]/div/input[2]",
                       "XID HOME": "link_text:Main Menu",
                       "LOGOUT": "link_text:Logout"}}


def FindTable(tableAlias, searchPath, appendPath, searchString):
    # set initial table area location
    table = CONFIG.driver.find_elements_by_xpath(tableAlias)
    #parse through each row in the table
    for counter in range(1, len(table)+1, 1):
        #print(tableAlias + "[" + str(counter) + "]" + searchPath)
        try:
            #compare values to confirm a match
            if CONFIG.driver.find_element_by_xpath(tableAlias + "[" + str(counter) + "]" + searchPath).text.upper() == searchString.upper():
                counter += 1
                break
        except: pass
    #set the table alias for validation of other objects within that grouping.
    CONFIG.tableAlias = tableAlias + "[" + str(counter) + "]" + appendPath
    return CONFIG.tableAlias

def FindRow(tableAlias, appendPath, expectedValue, objectType):
    try:
        table = CONFIG.driver.find_elements_by_xpath(tableAlias)
        for counter in range(1, len(table)+1, 1):
                tempAlias = tableAlias + "[" + str(counter) + "]" + appendPath
                #print("tempAlias: ")
                #print(tempAlias)
                try:
                    if CONFIG.driver.find_element_by_xpath(tempAlias).text.upper() == expectedValue.upper(): 
                        CONFIG.tableAlias = tableAlias + "[" + str(counter) + "]/"   
                        print(CONFIG.tempAlias)
                        return CONFIG.tableAlias
                except: pass   
    except: pass


