#---------- IMPORTS ----------
import poplib
import time

#------- CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent


def MappingTable(record):
    #converts the screen value from the spreadsheet into a function that controls how interactions take place.
    
    available_mappings = {
        "HOME": Index,
        "KEY REGISTRATION": KeyRegistration,
        "FORGOT LOGIN NAME": ForgotLoginName,
        "RECOVER PASSWORD": ResetPassword,
        "RESET PASSWORD": ResetPassword,
        "MANAGE ACCOUNT": ManageAccount, 
        "PROCESS EMAIL": ProcessEmail,
        "UNCLAIM": Unclaim,
        "HARVARDKEY MENU": Menu
        #"FIND ALUMNI RECORD": FindAlumniRecord
        } 
    MappedObjects = available_mappings.get(record["screen"], lambda x: None)(record)

    if record["screen"].upper() != "PROCESS EMAIL" and record["screen"].upper() != "HARVARDKEY MENU":
        UTILS.UseScreenObjects(MappedObjects, record)

    UTILS.StatusCheck(record)
    
    
def KeyRegistration(record):
    time.sleep(3)
    return {"PASSWORD TYPE": {"PASSWORD": [".//*[@id='password']","//div[1]/div[1]/p"], "PASSPHRASE": "//div[1]/div[2]/p"},
            "HAA ID": ".//*[@id='haaId']",
            "LAST NAME": ".//*[@id='lastName']",
            "FIRST NAME": ".//*[@id='firstName']", 
            "MONTH":".//*[@id='monthOfBirth']",
            "DAY":".//*[@id='dayOfBirth']",
            "YEAR":".//*[@id='yearOfBirth']",
            "HUID": [".//*[@id='username']", ".//*[@id='huid']"],
            "PASSWORD":".//*[@id='password']",
            "YEAR OF GRADUATION": ".//*[@id='yearOfGraduation']",    
            "LOGIN NAME": [".//*[@id='loginName']", ".//*[@type='radio' and @value='expectedValue']"],
            "CONFIRM LOGIN NAME": [".//*[@id='loginNameConfirmed']"],
            "SELECTED LOGIN NAME": "dynamic:.//input[starts-with(@value,'expectedValue')]",
            "PRIMARY RECOVERY EMAIL": ".//*[@id='recoveryPrimaryEmail']",
            "SECONDARY RECOVERY EMAIL": ".//*[@id='recoverySecondaryEmail']",             "NEW PASSWORD": ".//*[@id='password']",
            "CONFIRM PASSWORD": ".//*[@id='confirmedPassword']",
            "NETID": ".//*[@id='adid']",
            "TABS": {"CURRENT PIN HOLDERS": ".//*[@id='selectUserTypeCurrentPinHoldersTabButton']/p",
                     "NO PIN": ".//*[@id='selectUserTypeNewOrNoPINTabButton']/p",
                     "NEW TO HARVARD": ".//*[@id='selectUserTypeNewOrNoPINTabButton']/p",
                     "NEW TO HARVARDKEY": ".//*[@id='selectUserTypeNewOrNoPINTabButton']/p",
                     "ALUMNI": ".//*[@id='selectUserTypeAlumniTabButton']"},
            "ACTION": {"CONTINUE": [".//*[@id='selectUserTypeContinueButton']", ".//input[@type='submit']"],
                       "QUIT REGISTRATION PROCESS": "",
                       "CANCEL": "link_text:Cancel",
                       "CLEAR": "//input[@type='reset']",
                       "LOGIN": ".//*[@id='submitLogin']",
                       "SUBMIT": "//input[@type='submit']",
                       "PRINT THIS PAGE": "html/body/div[2]/div/div[4]/a[1]",
                       "BACK TO HARVARDKEY HOME": [".//*[@id='backToHarvardKeyHome']", "link_text:Back to HarvardKey Home"]}}

       
# This is the index page allowing various account options
def Index(record):
    if CONFIG.driver == "":
        CONFIG.driver = UTILS.OpenBrowser(CONFIG.browser)
    
    if CONFIG.driver.current_url != CONFIG.harvardKeyURL:
        CONFIG.driver.get(CONFIG.harvardKeyURL)

        return {"CLAIM YOUR HARVARDKEY": "link_text:Claim Your HarvardKey >",
                "MANAGE YOUR TWO-STEP VERIFICATION": "link_text:Manage Your Two-step Verification >",
                "MANAGE YOUR ACCOUNT & SERVICES": "link_text:Manage Your Account & Services>",
                "RECOVER LOGIN NAME": "link_text:Recover Login Name >",
                "RECOVER/RESET PASSWORD": "link_text:Recover/Reset Password >"}            
                 

def ResetPassword(record):
    return {"PASSWORD TYPE": {"PASSWORD": "//div[1]/div[1]/p", "PASSPHRASE": "//div[1]/div[2]/p"},
            "LOGIN NAME": ".//*[@id='loginName']",
            "RECOVERY EMAIL": ".//*[@id='recoveryPrimaryEmail']",
            "NEW PASSWORD": ".//*[@id='password']",
            "CONFIRM PASSWORD": ".//*[@id='confirmedPassword']",
            "ACTION": {"CONTINUE": "//input[@type='submit']",
                      "CANCEL": "link_text:Cancel",
                      "SUBMIT": "//input[@type='submit']",
                      "BACK TO HARVARDKEY HOME": [".//*[@id='backToHarvardKeyHome']", "link_text:Back to HarvardKey Home"]}}
    

def ForgotLoginName(record):  
    return {"HUID": ".//*[@id='harvardId']",
            "HAA ID": ".//*[@id='harvardId']",
            "RECOVERY EMAIL": ".//*[@id='recoveryEmailAddress']",
            "ACTION": {"CONTINUE": "//input[@type='submit']",
                       "CANCEL": "link_text:Cancel"}}
        

def ProcessEmail(record):
    time.sleep(13)
    enum = CONFIG.driver.find_element_by_xpath(".//*[@class='alert alert-success']")
    if enum.is_displayed and enum.is_enabled:
        # Set up the connection to the POP server 
        pop_conn = poplib.POP3_SSL('pop.gmail.com')
        pop_conn.user(CONFIG.mail_username)
        pop_conn.pass_(CONFIG.mail_password)

        poplist = pop_conn.list()
        # Iterate through the messages 
        for i in range(len(pop_conn.list()[1])): 
            message = ""
            for j in pop_conn.retr(i+1)[1]: 
                message += str(j).replace("b'", "").replace("'", "").replace("  ", "").replace("m_506676278511645089", "").replace("=3D", "").replace("<wbr>", "").replace("=", "")
            #LOGIN NAME email
            if record["fields"][0].upper() == "LOGIN NAME":
                index = message.find("Here is the name we have on file for you:")
                if index > -1:
                    CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
                    message = message[index:]
                    LoginName = message[message.find('<span id="loginName">') + len('<span id="loginName">'):message.find('</span></p>')]
                    if record["values"][0] == "":
                        CONFIG.logger1.info (indent + "Login Name is '" + LoginName + "'")
                    elif LoginName == record["values"][0]:
                        CONFIG.logger1.info (indent + "Login Name matched expected value: '" + LoginName + "'")    
                    else:
                        UTILS.ReportErrors("Login Name did not match expected value.  Expected: '" + record["values"][0] + "    Actual: " + LoginName + "'", record)
                    pop_conn.dele(i+1)
            #REGISTRATION COMPLETE email
            elif record["fields"][0].upper() == "REGISTRATION COMPLETE":
                searchString = "Your HarvardKey has been activated and is now ready to use."
                index = message.find(searchString)
                if index > -1:
                    CONFIG.logger1.info(indent + record["fields"][0] + " email message received")
                    searchValues = ["Login Name", "Primary Recovery Email", "Secondary Recovery Email"]
                    message = message.replace(": <", ":<")
                    for value in searchValues:
                        message = message[message.find("<b>" + value + ":</b><span>") + len("<b>" + value + ":</b><span>"):]
                        CONFIG.logger1.info(indent + value.upper() + ":   " + message[:message.find("</span></p>")])
                    pop_conn.dele(i+1)
            #PASSWORD CHANGE email    
            elif record["fields"][0].upper() == "PASSWORD CHANGE":
                index = message.find("you recently changed the password for your HarvardKey account.")
                if index > -1:
                    CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
                pop_conn.dele(i+1)
            #PASSWORD RESET email
            elif record["fields"][0].upper() == "PASSWORD RESET":
                index = message.find("you recently reset the password for your HarvardKey account.")
                if index > -1:
                    CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
                pop_conn.dele(i+1)
            # RECOVERY EMAIL CHANGE email
            elif record["fields"][0].upper() == "RECOVERY EMAIL":
                index = message.find("you recently changed one or both of the recovery email addresses for your HarvardKey account.")
                if index > -1:
                    CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
                pop_conn.dele(i+1)
            # VERIFY RECOVERY EMAIL
            elif record["fields"][0].upper() == "VERIFY RECOVERY EMAIL":
                
                searchString = "Complete Your HarvardKey Recovery Email Verification"
                index = message.find(searchString)
                if index > -1:    
                    CONFIG.logger1.info (indent + record["fields"][0] + " message received")
                    url = message[message.find('"><span>https:')+ len('"><span>'):message.find('</span>')]
                    CONFIG.driver.get(url)
                pop_conn.dele(i+1)
            else:
                #REGISTRATION or PASSWORD email
                if record["fields"][0].upper() == "REGISTRATION" or record["fields"][0].upper() == "PASSWORD":
                    searchString = "Complete Your HarvardKey Account Registration"
                    index = message.find(searchString)
                    if index == -1:    
                        searchString = "Reset Your HarvardKey Password"
                        index = message.find(searchString)

                    if index > -1:
                        CONFIG.logger1.info (indent + record["fields"][0] + " email message received")
                        message = message[message.find(searchString) + len(searchString):]
                        #print("message: " + str(message))
                        passcode = message[message.find('"passcode">') + len('"passcode">'):message.find('</span></p>')]
                        #print("Passcode: " + str(passcode))
                        #searchString = "Enter your confirmation code in the following web page"
                        searchString = '"><span>'
                        message = message[message.find(searchString) + len(searchString):]
                        #print("message: " + str(message))
                        #url = message[message.find('"><span>https:')+ len('"><span>'):message.find('</span>')]
                        url = message[:message.find('</span>')]
                        #print("URL: " + str(url))

                        pop_conn.dele(i+1)
                    else: continue
                    CONFIG.driver.get(url)
                    enum = CONFIG.driver.find_element_by_name("passcodeToken")
                    result = UTILS.PerformAction(enum, "Confirmation Code", passcode, record)
                    CONFIG.driver.find_element_by_xpath("html/body/div[2]/div/form/div/input").click()

        pop_conn.quit()
    else:
        print("Failed to generate an 'email sent' condition")
        

def ManageAccount(record):
    return {"LOGIN NAME": [".//*[@id='username']", ".//*[@id='loginName']/span"],
            "HAA ID": ".//*[@id='username']",
            "PASSWORD": ".//*[@id='password']",
            "NETID": ".//*[@id='netId']/span",
            "ENROLLMENT STATUS": ".//*[@id='mfaEnrollmentStatus']/span",
            "ACTIVATION STATUS": ".//*[@id='mfaActivationStatus']/span",
            "CURRENT PASSWORD":".//input[@id='currentPassword']",
            "NEW PASSWORD": ".//*[@id='password']",
            "CONFIRM PASSWORD": ".//*[@id='confirmedPassword']",
            "PRIMARY RECOVERY EMAIL": ".//*[@id='recoveryPrimaryEmail']",
            "SECONDARY RECOVERY EMAIL": ".//*[@id='recoverySecondaryEmail']",
            "VERIFY PRIMARY RECOVERY EMAIL": ".//*[@id='verifyRecoveryPrimaryEmail']",
            "VERIFY SECONDARY RECOVERY EMAIL": ".//*[@id='verifyRecoverySecondaryEmail']",
            "EXISTING HARVARDKEY PASSWORD": ".//*[@id='password']",
            "ACTION": {"LOGIN": ".//*[@id='submitLogin']",
                       "CANCEL": "link_text:Cancel",
                       "SUBMIT": "//input[@type='submit']",
                       "NEW USER": "link_text:New user? Forgot your PIN / Password?",
                       "LOOK HERE FOR HOW TO LOGIN WITH HARVARDKEY": "",
                       "RECOVER/RESET YOUR HARVARDKEY PASSWORD": "link_text:Recover/Reset Your HarvardKey Password >",
                       "RECOVER YOUR HARVARDKEY LOGIN NAME": "link_text:Recover Your HarvardKey Login Name >",
                       "RECOVER/RESET PASSWORDS FOR OTHER LOGIN TYPES": "link_text:Recover/Reset Passwords for Other Login Types >",
                       "CLAIM A HARVARDKEY": "link_text:Claim a HarvardKey >",
                       "CLAIM YOUR HARVARDKEY": "link_text:Claim Your HarvardKey >",
                       "CHANGE PASSWORD": "link_text:Change Password >",
                       "SYNCHRONIZE PASSWORD": "link_text:Synchronize Password >",
                       "CHANGE RECOVERY INFORMATION": "link_text:Change Recovery Information >",
                       "MANAGE TWO-STEP VERIFICATION": "link_text:Manage Two-step Verification >",
                       "SET UP TWO-STEP VERIFICATION": "link_text:Set Up Two-step Verification >",
                       "BACK TO MANAGE ACCOUNT": ".//*[@id='backToManageAccount']",
                       "BACK TO HARVARDKEY HOME": ".//*[@id='backToHarvardKeyHome']",
                       "VERIFY PRIMARY RECOVERY EMAIL": ".//*[@id='verifyRecoveryPrimaryEmail']/a",
                       "VERIFY SECONDARY RECOVERY EMAIL": ".//*[@id='verifyRecoverySecondaryEmail']/a",
                       "ACTIVATE G.HARVARD ACCOUNT": "link_text:Activate g.harvard Account>",
                       },
            "LOGIN TYPE": {"HUID": "//input[@name='compositeAuthenticationSourceType' and @value='PIN']",
                           "FAS": "//input[@name='compositeAuthenticationSourceType' and @value='ADID']"},}
        
def pinSystem(record):
    return {"LOGIN ID": ".//*[@id='username']",
            "HAA ID": ".//*[@id='username']",
            "PIN/PASSWORD": ".//*[@id='password']",
            "LOGIN TYPE": {"HUID": ".//*[@id='compositeAuthenticationSourceType1']",
                           "HARVARD UNIVERSITY ID": ".//*[@id='compositeAuthenticationSourceType1']",
                           "FAS": "//input[@name='compositeAuthenticationSourceType' and @value='ADID']",
                           "CENTRAL USERNAME": ".//*[@id='compositeAuthenticationSourceType2']",
                           "ALUMNI.HARVARD LOGIN": ".//*[@id='alumi']"},
            "ERROR": "//h5[@class='alert-heading']",
            "ACTION": {"LOGIN": ".//*[@id='login']/fieldset[2]/input[1]",
                       "NEW USER": "link_text:	New user? Forgot your PIN / Password?"}}
                       

def Unclaim(record):
    # Delete LDAP record
    #LDAP.LDAPOperations({"screen"})
    # Reset IIQ
    
    # Delete data from Oracle
    
    pass

def Menu(record):
    # selenium doesn't recognize burgerWrap menus so we're emulating the functionality
    if record["fields"][0].upper() == "HOME":
        CONFIG.driver.close()
        CONFIG.driver = UTILS.OpenBrowser(CONFIG.browser)
        CONFIG.driver.get(CONFIG.harvardKeyURL)
    elif record["fields"][0].upper() == "CLOSE":    
        CONFIG.driver.close()
        CONFIG.driver = ""
    elif record["fields"][0].upper() == "FAQ":
        CONFIG.driver.get("http://reference.iam.harvard.edu/faq")
    elif record["fields"][0].upper() == "HELP":
        CONFIG.driver.get("http://reference.iam.harvard.edu/help")
    elif record["fields"][0].upper() == "LOGOUT":
        CONFIG.driver.get("https://pin-cas.qa.iam.harvard.edu/cas/logout")
