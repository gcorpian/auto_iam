import base64

#password = "4ut0m4t10n!"
#password = "Ekushe@2121"
#password = "secret123"
#password = '9c7nh9Zxt466qA5FmgQMGyyg'
#password = 'crF3ccXkP5YNckjy9BYAAx2M"'
#password = 'fTsnXF7hFn89NHtqsJfdJ7mX'
password = '8h6ffHPefSm3NpWyh65s'
#password = "Qa-test-poc1"
# Encode password
encoded_pw = base64.b64encode(bytes(password, "utf-8"))
print("Encoded Value:")
print(encoded_pw)
# Decode password
expectedValue = encoded_pw.decode('ascii')
print("\nString Formatted Value:")
print(expectedValue)

#expectedValue = 'dmxCOTA1NjhiYjhu'
decoded_pw = base64.b64decode(bytes(expectedValue, "utf-8"))
print("\nDecoded Value:")
print(decoded_pw.decode('ascii'))
